s.boot;

(
var window = Window("PitchShiftDelay", Rect(200,100,600,430)).front;
var pitchRatioSlider, pitchDispertionSlider, timeDispertionSlider, windowSizeSlider, gainSlider, delaySlider, cutoffSlider, panSlider, directSlider, modulFreqSlider, modulDepthSlider;
// number of independent feedback filters
var nums = 2;

var synth0 = { arg windowSize = 0.1, pitchRatio = 0.5, pitchDispertion = 0, timeDispertion = 0.5, fmFreq = 1, fmAmp = 0, delay = 1, gain = 0, cutoff = 4000, pan = 0;
	// delay is applied first so that all GUI actions are immediate
	var in = DelayN.ar(SoundIn.ar([0, 1]), 1, delay, gain);
	var out = PitchShift.ar( in, windowSize, SinOsc.kr(fmFreq, 0, fmAmp, pitchRatio), pitchDispertion, timeDispertion);
	var filtered_out = HPF.ar(LPF.ar(out,cutoff),20);
	Pan2.ar(filtered_out, pan);
}.play(s);

var synth1 = { arg windowSize = 0.1, pitchRatio = 0.5, pitchDispertion = 0, timeDispertion = 0.5, fmFreq = 1, fmAmp = 0, delay = 1, gain = 0, cutoff = 4000, pan = 0;
	// delay is applied first so that all GUI actions are immediate
	var in = DelayN.ar(HPF.ar(LPF.ar(SoundIn.ar([0, 1]), cutoff),20), 10, delay, gain);
	var out = PitchShift.ar( in, windowSize, SinOsc.kr(fmFreq, 0, fmAmp, pitchRatio), pitchDispertion, timeDispertion);
	Pan2.ar(out, pan);
}.play(s);

var direct = { arg gain = 0;
	SoundIn.ar([0, 1], gain);
}.play(s);

var ss = [synth0, synth1];

pitchRatioSlider = Array.fill(nums, { arg i; var xx = 20 + (i * 300); EZSlider(window, Rect(xx, 20, 250, 20), "Pitch Ratio", ControlSpec(0, 4, \lin, 0.01, 0.5), { ss[i].set(\pitchRatio, pitchRatioSlider[i].value); }, labelWidth:80) });

pitchDispertionSlider = Array.fill(nums, { arg i; var xx = 20 + (i * 300); EZSlider(window, Rect(xx, 60, 250, 20), "Pitch Dispersion", ControlSpec(0, 0.1, \lin, 0.001, 0), { ss[i].set(\pitchDispertion, pitchDispertionSlider[i].value)}, labelWidth:80) });

timeDispertionSlider = Array.fill(nums, { arg i; var xx = 20 + (i * 300); EZSlider(window, Rect(xx, 100, 250, 20), "Time Dispersion", ControlSpec(0, 1, \lin, 0.0001, 0.5, "sec"), { ss[i].set(\timeDispertion,timeDispertionSlider[i].value)}, labelWidth:80) });

modulFreqSlider = Array.fill(nums, { arg i; var xx = 20 + (i * 300); EZSlider(window, Rect(xx, 140, 250, 20), "Modulation Freq", ControlSpec(0, 10, \lin, 0.1, 1, "Hz"), { ss[i].set(\fmFreq, modulFreqSlider[i].value);  }, labelWidth:100) });

modulDepthSlider = Array.fill(nums, { arg i; var xx = 20 + (i * 300); EZSlider(window, Rect(xx, 180, 250, 20), "Modulation Depth", ControlSpec(0, 1, \lin, 0.1, 0), { ss[i].set(\fmAmp, modulDepthSlider[i].value);  }, labelWidth:100) });

windowSizeSlider = Array.fill(nums, { arg i; var xx = 20 + (i * 300); EZSlider(window, Rect(xx, 220, 250, 20), "Window Size", ControlSpec(0, 1, \lin, 0.01, 0.1, "sec"), { ss[i].set(\windowSize,windowSizeSlider[i].value);  }, labelWidth:80) });

delaySlider = Array.fill(nums, { arg i; var xx = 20 + (i * 300); EZSlider(window, Rect(xx, 260, 250, 20), "Delay", ControlSpec(0, 1 + (i*9), \lin, 0.1, 0.5 + (i*4.5), "sec"), { ss[i].set(\delay,delaySlider[i].value)}, labelWidth:80) });

gainSlider = Array.fill(nums, { arg i; var xx = 20 + (i * 300); EZSlider(window, Rect(xx, 300, 250, 20), "Gain", ControlSpec(0, 1, \lin, 0.05, 0), { ss[i].set(\gain,gainSlider[i].value)}, labelWidth:80) });

cutoffSlider = Array.fill(nums, { arg i; var xx = 20 + (i * 300); EZSlider(window, Rect(xx, 340, 250, 20), "LPF Cutofff", ControlSpec(0, 20000, \lin, 20, 4000, "Hz"), { ss[i].set(\cutoff,cutoffSlider[i].value)}, labelWidth:80) });

//mixSlider = EZSlider(window, Rect(20, 300, 250, 20), "Mix", ControlSpec(0, //1, \lin, 0.05, 1), { synth.set(\mix,mixSlider.value)}, labelWidth:80);

// panSlider = EZSlider(window, Rect(20, 300, 250, 20), " Pan ", ControlSpec(-1, 1, \lin, 0.1, 0), { synth.set(\pan,panSlider.value)}, labelWidth:80);

directSlider = EZSlider(window, Rect(20, 400, 250, 20), "Direct", ControlSpec(0, 1, \lin, 0.05, 0), { direct.set(\gain, directSlider.value); }, labelWidth:80);

)