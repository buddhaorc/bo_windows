#include "NotePlayer.h"
#include "Scale.h"
#include "NoteMaster.h"
#include "Panning.h"


int MidiOut::selected = 0;
int MidiOut::num = 0;
MidiOutEx* MidiOut::midis[MAX_MIDI];

NoteMaster master;
extern Panning panning;

NotePlayer::NotePlayer(SenderSet s,
           int id, int ch, int a, int dur, int no, int velo, float b, ofPoint p) :
    ss(s), melid(id), after(a), channel(ch), duration(dur), note(no), velocity(velo), bend(b), pt(p)
{
    t0 = ofGetElapsedTimeMillis();
    waiting = true;
    master.play(this);
}

NotePlayer::~NotePlayer()
{
    //dtor
}

//--------------------------------------------------------------
// TODO should be moved to util.cpp when we have it
bool askContinue() {
    cout << "Continue? (y/n) ";
    char answer;
    cin >> answer;
    return answer == 'y' || answer == 'Y';
}

//--------------------------------------------------------------
long NotePlayer::tick(long t)
{
    if (waiting)
    {
        if (t > t0 + after)
        {
            waiting = false;
            if (ss.osc) {
                sendNoteOsc(ss.osc, melid, channel, pt, note, velocity, duration);
            }

            if (ss.serial) {
                ss.serial->sendOn(note, velocity, duration); // the default tick is 50 msec
            }

            if (ss.synth) {
                ss.synth->sendOn(note, velocity);
            }

            if (ss.sampler) {
                ss.sampler->send(channel, note, velocity, duration, pt.x, pt.y); // the default tick is 50 msec
            }

            if (ss.mout) {
                if (Scale::enabled())
                {
                    ss.mout->sendPitchBend(channel,  bend * 2048 + 2048);
                }
                if (ss.mout->isPanAllowed(channel))
                {
                    if (::panning.getEnabled())
                    {
                        float pan = ofMap(::panning.getPan(pt.x, pt.y), -1, 1, 0, 1);
                        ss.mout->sendControlChange(channel, 10, ofClamp(pan * 127, 0, 127));
                        velocity += ::panning.getAttenuation(pt.x, pt.y);
                    }
                    else
                    {
                        ss.mout->sendControlChange(channel, 10, ofClamp(pt.x * 127, 0, 127));
                    }
                }
                ss.mout->sendNoteOn(channel, note, velocity);
            }
            t0 = t;
            return duration;
        }
        else
        {
            return t0 + after - t;
        }
    }
    else
    {
        if (t > t0 + duration)
        {
            stop();
            return -1;
        }
        else
        {
            return t0 + duration - t;
        }
    }
    cout << "bad tick" << endl;
    return 10000;
}

//--------------------------------------------------------------
void NotePlayer::stop()
{
    if (ss.serial) ss.serial->sendOff(note);
    if (ss.synth) ss.synth->sendOff(note);
    if (ss.mout) ss.mout->sendNoteOff(channel, note, velocity);
    if (ss.osc) stopNoteOsc(ss.osc, melid, channel);
}

//--------------------------------------------------------------
void MidiOutEx::sendProgramChange(int channel, int v)
{
    cout << " " << v;

    mtx.lock();
    ofxMidiOut::sendProgramChange(channel, v);
    mtx.unlock();
}

//--------------------------------------------------------------
void MidiOutEx::sendPitchBend(int channel, int v)
{
    mtx.lock();
    ofxMidiOut::sendPitchBend(channel, v);
    mtx.unlock();
}

//--------------------------------------------------------------
void MidiOutEx::sendNoteOn(int channel, int id, int value)
{
    mtx.lock();
    ofxMidiOut::sendNoteOn(channel, id, value);
    mtx.unlock();
}

//--------------------------------------------------------------
void MidiOutEx::sendNoteOff(int channel, int id, int value)
{
    mtx.lock();
    ofxMidiOut::sendNoteOff(channel, id, value);
    mtx.unlock();
}

//--------------------------------------------------------------
void MidiOutEx::sendControlChange(int channel, int id, int value)
{
    mtx.lock();
    ofxMidiOut::sendControlChange(channel, id, value);
    mtx.unlock();
}

//--------------------------------------------------------------
void MidiOut::sendProgramChange(int channel, int v)
{
    int contr = (channel >> 8) % MidiInstruments::m_InstrNum;
    midis[contr]->sendProgramChange(channel & 0xff, v);
}

//--------------------------------------------------------------
void MidiOut::sendPitchBend(int channel, int v)
{
    int contr = (channel >> 8) % MidiInstruments::m_InstrNum;
    midis[contr]->sendPitchBend(channel & 0xff, v);
}

//--------------------------------------------------------------
void MidiOut::sendNoteOn(int channel, int id, int value)
{
    if (f)
    {
        // TODO
    }
    if (MidiInstruments::m_InstrNum)
    {
        int contr = (channel >> 8) % MidiInstruments::m_InstrNum;
        midis[contr]->sendNoteOn(channel & 0xff, id, value);
    }
}

//--------------------------------------------------------------
void MidiOut::sendNoteOff(int channel, int id, int value)
{
    if (f)
    {
        // TODO
    }
    if (MidiInstruments::m_InstrNum)
    {
        int contr = (channel >> 8) % MidiInstruments::m_InstrNum;
        midis[contr]->sendNoteOff(channel & 0xff, id, value);
    }
}

//--------------------------------------------------------------
void MidiOut::recordOn()
{
    char fn[256];
    sprintf(fn, "%04i%02i%02i-%02i%02i%02i.mid",
            ofGetYear(), ofGetMonth(), ofGetDay(), ofGetHours(), ofGetMinutes(), ofGetSeconds());
    f = fopen(fn, "wb");
}

//--------------------------------------------------------------
void MidiOut::recordOff()
{
    if (f) fclose(f);
    f = 0;
}

//--------------------------------------------------------------
string MidiOut::getMidiName(int nm)
{
    if (nm < 0) nm = selected;
    if (nm < num) {
        return midis[nm]->getName();
    } else {
        return "";
    }
}

//--------------------------------------------------------------
void MidiOut::selectMidi(string nm)
{
    vector<string> mi = ofSplitString(nm, "=", true, true);
    if (!mi.size()) return;

    string name = mi[0];
    for (int i = 0; i < num; ++i)
    {
        if (midis[i]->getName() == name)
        {
            selected = i;
            return; // found the name
        }
    }

    if (mi.size() > 1) {
        selected = ofToInt(mi[1]);
    }
}

//--------------------------------------------------------------
MidiOut::~MidiOut()
{
    for (int i=0; i < MAX_MIDI; ++i)
    {
        delete midis[i];
    }
}

//--------------------------------------------------------------
void MidiOut::init(ofxIniSettings &ini)
{
    ofxMidiOut mo;

    // let's check the first start
    if (ini.get("midiout", string("none")) == "NOTDEFINED")
    {
        ini.outputFilename = "config.ini";
        int i, n;
        switch (mo.getNumPorts())
        {
        case 0:
            cout << "No MIDI devices found." << endl;
            if (askContinue())
            {
                ini.setString("midiout", "none");
                ini.keys["midiout"] = "none";
                return;
            }
            else exit(2);
        case 1:
            cout << "The MIDI Out device will be saved in " + ini.outputFilename + ": " <<  mo.getPortName(0) << endl;
            cout << "Press Enter to continue.";
            getchar();
            ini.setString("midiout", mo.getPortName(0));
            ini.keys["midiout"] = mo.getPortName(0);
            break;
        default:
            cout << "Found the following MIDI Out devices:" << endl;
            for (i = 0; i < mo.getNumPorts(); ++i)
            {
                cout << i << " " << mo.getPortName(i) << endl;
            }
            while (true)
            {
                cout << "Enter the number of the MIDI Out device to use, or -1 to exit application:" << endl;
                cin >> n;
                if (n < 0) exit(3);
                if (n < mo.getNumPorts())
                {
                    ini.setString("midiout", mo.getPortName(n));
                    ini.keys["midiout"] = mo.getPortName(n);
                    break;
                }
            }
        }
    }
    else
    {
        // just for information
        mo.listPorts();
    }

    num = 0;
    for (int no = 0; no < MAX_MIDI; ++no)
    {
        string suffix;
        if (no) suffix = ofToString(no);
        string midi = ini.get(string("midiout") + suffix, string("none"));
        if (midi == "none") break;
        bool panning = ini.get(string("midipan") + suffix, true);
        int np = -1;
        if (midi == "!Fluxa")
        {
            FluxaMidi *fluxa = new FluxaMidi(no);
            fluxa->init(ini);
            midis[no] = fluxa;
            np = no;
        }
        else
        {
            midis[no] = new MidiOutEx(no);
            midis[no]->panAllowed = panning;
            if (midi.at(0) == '#')
            {
                np = ofToInt(midi.substr(1));
                cout << midi << " : " << np << endl;
            }
            else
            {
                for (int i=0; i < mo.getNumPorts(); ++i) {
                    string nm = mo.getPortName(i);
                    int p = nm.find(midi);
                    if (p >= 0) {
        //                cout << "Using " << nm << endl;
                        np = i;
                    }
                }
            }
            if (np < 0)
            {
                cout << "Invalid MIDI configuration: " << midi << " not found" << endl;
                if (askContinue()) break; else exit(2);
            }
        }

        if (!midis[no]->openPort(np))
        {
            cout << "MIDI out open port failed " << no << endl;
            if (askContinue()) break; else exit(2);
        }
        cout << "MIDI out port opened " << no << " for " << midis[no]->getName() << endl;

        string instrs = ini.get("instruments" + suffix, string(""));
        MidiInstruments::assignChannels(no, *midis[no], instrs);
        noteControl[no] = ini.get("midiout" + suffix + "note", string(""));
        veloControl[no] = ini.get("midiout" + suffix + "velo", string(""));
        trackControl[no] = ini.get("midiout" + suffix + "track", string(""));
        envelControl[no] = ini.get("midiout" + suffix + "enve", string(""));
        num++;
    }

    MidiInstruments::m_Effect = ini.get("effect", 1);
}

//--------------------------------------------------------------
string MidiOut::sendCustomMidi(int key, ofxIniSettings &ini)
{
    ostringstream ostr;
    ostr << "midi";
    if (selected) ostr << selected;
    ostr << "ctl";
    char keychar = key + 'a' - 1; // 1 corresponds to a
    ostr << keychar;
    string s = ostr.str();
    string cmd = ini.get(s, string(""));
    if (!cmd.size())
    {
        return "ERROR: no command found for " + s;
    }
    vector<string> commands = ofSplitString(cmd, ";", true, true);
    for (int i=0; i < commands.size(); ++i)
    {
        vector<string> cmdParts = ofSplitString(commands[i], ",", true, true);
        if (cmdParts.size() < 2)
        {
            return "ERROR: invalid command " + cmd + " for " + s;
        }
        int cmdType = cmdParts[0].at(0);
        switch (cmdType)
        {
        case 'P':
            if (cmdParts.size() < 3)
            {
                return "ERROR: invalid program change command " + cmd + " for " + s;
            }
            sendProgramChange( (selected << 8) + ofToInt(cmdParts[1]), ofToInt(cmdParts[2]));
            break;
        case 'C':
            if (cmdParts.size() < 4)
            {
                return "ERROR: invalid control command " + cmd + " for " + s;
            }
            sendControlChange( (selected << 8) + ofToInt(cmdParts[1]), ofToInt(cmdParts[2]), ofToInt(cmdParts[3]));
            break;
        case 'S':
            return "ERROR: sysex commands not implemented";
        }
    }
    return s + " " + cmd;
}

//--------------------------------------------------------------
void MidiOut::stopAllNotes()
{
    string none = "REPEAT";
    for (int i=0; i<num; i++) {
        for (int c=0; c<16; c++) {
            midis[i]->allNotesOff(c);
        }
        ofSleepMillis(50);
        MidiInstruments::assignChannels(i, *midis[i], none);
    }
}

//--------------------------------------------------------------
void MidiOutEx::allNotesOff(int channel)
{
    mtx.lock();
    ofxMidiOut::sendControlChange(channel, 123, 0);
    mtx.unlock();
}

//--------------------------------------------------------------
void MidiOut::sendControls(int channel, int type, int value)
{
    if (channel <= 0 || MidiInstruments::m_InstrNum == 0) return;
    int c = (channel >> 8) % MidiInstruments::m_InstrNum;
    switch (type)
    {
    case CC_NOTE:
        sendControls(c, channel & 0xff, noteControl[c], value);
        break;
    case CC_VELO:
        sendControls(c, channel & 0xff, veloControl[c], value);
        break;
    case CC_TRCK:
        sendControls(c, channel & 0xff, trackControl[c], value);
        break;
    case CC_ENVE:
        sendControls(c, channel & 0xff, envelControl[c], value);
        break;
    }
}

//--------------------------------------------------------------
void MidiOut::sendBankSelect(int channel, int value)
{
    if (channel <= 0 || MidiInstruments::m_InstrNum == 0) return;
    int c = (channel >> 8) % MidiInstruments::m_InstrNum;
    int ch = channel && 0xff;
    MidiInstruments::assignBank(c, *midis[c], ch, value);
}

//--------------------------------------------------------------
void MidiOut::sendControls(int nm, int ch, string &s, int value)
{
    int sep = s.find(':');
    string cmds = (sep == string::npos) ? s : s.substr(0, sep);
    vector<string> ss = ofSplitString(cmds, ",", true, true);
    for (int i = 0; i < ss.size(); ++i)
    {
        string c = ss[i];
        int v;
        if (sscanf(c.c_str(), "%d", &v) == 1) {
            // cout << v << ", " << value << " to " << ch << " of " << midis[nm].getName() << endl;
            midis[nm]->sendControlChange(ch, v, value);
        } else {
            cout << "ignored " << c << endl;
        }
    }
}

//--------------------------------------------------------------
bool MidiOut::isPanAllowed(int channel)
{
    if (channel <= 0 || MidiInstruments::m_InstrNum == 0) return false;
    int c = (channel >> 8) % MidiInstruments::m_InstrNum;
    return midis[c]->panAllowed;
}

//--------------------------------------------------------------
void MidiOut::sendControlChange(int channel, int v1, int v2)
{
    if (channel <= 0 || MidiInstruments::m_InstrNum == 0) return;
    int c = (channel >> 8) % MidiInstruments::m_InstrNum;
    midis[c]->sendControlChange(channel & 0xff, v1, v2);
}

//--------------------------------------------------------------
string MidiOut::getControlName(int channel, int type)
{
    if (channel <= 0 || MidiInstruments::m_InstrNum == 0) return "";
    int c = (channel >> 8) % MidiInstruments::m_InstrNum;
    switch (type)
    {
        case CC_ENVE:
            return getControlName(envelControl[c]);
        case CC_NOTE:
            return getControlName(noteControl[c]);
        case CC_TRCK:
            return getControlName(trackControl[c]);
        case CC_VELO:
            return getControlName(veloControl[c]);
    }
    return "???";
}

//--------------------------------------------------------------
vector<int> MidiOut::getControls(int channel, int type)
{
    int c = (channel >> 8) % MidiInstruments::m_InstrNum;
    string cmd;
    switch (type)
    {
    case CC_NOTE:
        cmd = noteControl[c];
        break;
    case CC_VELO:
        cmd = veloControl[c];
        break;
    case CC_TRCK:
        cmd = trackControl[c];
        break;
    case CC_ENVE:
        cmd = envelControl[c];
        break;
    }

    vector<int> res;
    int sep = cmd.find(':');
    string cmds = (sep == string::npos) ? cmd : cmd.substr(0, sep);
    vector<string> ss = ofSplitString(cmds, ",", true, true);
    for (int i = 0; i < ss.size(); ++i)
    {
        int v;
        if (sscanf(ss[i].c_str(), "%d", &v) == 1) {
            res.push_back(v);
        }
    }

    return res;
}

//--------------------------------------------------------------
string MidiOut::getControlName(string cmd)
{
    int sep = cmd.find(':');
    return (sep == string::npos) ? cmd : cmd.substr(sep + 1);
}

//--------------------------------------------------------------
bool FluxaMidi::openPort(unsigned int port)
{
    if (serial.setup(comport, 19200))
    {
        mtx.lock();
        serial.writeByte('R');
        serial.flush();
        mtx.unlock();
        return true;
    }

    return false;
}

//--------------------------------------------------------------
void FluxaMidi::closePort()
{
    serial.close();
}

//--------------------------------------------------------------
string FluxaMidi::getName()
{
    return "FluxaSynth";
}
//--------------------------------------------------------------
void FluxaMidi::init(ofxIniSettings &ini)
{
	comport = ini.get("fluxa", string(""));
}

//--------------------------------------------------------------
void FluxaMidi::sendNoteOn(int channel, int id, int value)
{
    unsigned char cmd[] = {'n', channel, id, value};
    mtx.lock();
    serial.writeBytes(cmd, sizeof(cmd) / sizeof(cmd[0]));
    serial.flush();
    mtx.unlock();
}

//--------------------------------------------------------------
void FluxaMidi::sendNoteOff(int channel, int id, int value)
{
    unsigned char cmd[] = {'f', channel, id, value };
    mtx.lock();
    serial.writeBytes(cmd, sizeof(cmd) / sizeof(cmd[0]));
    serial.flush();
    mtx.unlock();
}

//--------------------------------------------------------------
void FluxaMidi::sendProgramChange(int ch, int v)
{
    cout << "." << v;
    if (v >= 0)
    {
        unsigned char cmd[] = {'p', ch, v };
        mtx.lock();
        serial.writeBytes(cmd, sizeof(cmd) / sizeof(cmd[0]));
        serial.flush();
        mtx.unlock();
    }
}

//--------------------------------------------------------------
void FluxaMidi::sendPitchBend(int ch, int v)
{
    unsigned char cmd []= {'b', ch, v };
    mtx.lock();
    serial.writeBytes(cmd, sizeof(cmd) / sizeof(cmd[0]));
    serial.flush();
    mtx.unlock();
}

//--------------------------------------------------------------
void FluxaMidi::sendControlChange(int channel, int v1, int v2)
{
    unsigned char cmd[] = {'c', channel, v1, v2 };
    mtx.lock();
    serial.writeBytes(cmd, sizeof(cmd) / sizeof(cmd[0]));
    serial.flush();
    mtx.unlock();
}

//--------------------------------------------------------------
void FluxaMidi::allNotesOff(int channel)
{
    mtx.lock();
    serial.writeByte('a');
    serial.writeByte(channel);
    serial.flush();
    mtx.unlock();
}

//--------------------------------------------------------------
OscSender::OscSender()
{

}

OscSender::~OscSender()
{
    for (list<ofxOscSender*>::iterator se = senders.begin(); se != senders.end(); se++)
    {
        // delete *se;
    }
}

void OscSender::init(ofxIniSettings &ini)
{
	string hosts = ini.get("oscHosts", string(""));
	vector<string> hh = ofSplitString(hosts, ";", true, true);
	for (int i = 0; i < hh.size(); ++i)
    {
        vector<string> hp = ofSplitString(hh[i], ":", true, true);
        if (hp.size() == 2)
        {
            int port = ofToInt(hp[1]);
            if (port)
            {
                ofxOscSender * s = new ofxOscSender();
                s->setup(hp[0], port);
                senders.push_back(s);
                cout << "added OSC sender " << hp[0] << ":" << hp[1] << endl;
            }
        }
    }
}

void OscSender::sendMessage(ofxOscMessage& message)
{
    for (list<ofxOscSender*>::iterator se = senders.begin(); se != senders.end(); se++)
    {
        (*se)->sendMessage(message);
    }
}

void NotePlayer::sendNoteOsc(OscSender *o, int melid, int channel, ofPoint &pt, int n, float vol, float dur)
{
    ofxOscMessage m;
    m.setAddress( "/bo/note/on" );
    m.addIntArg( melid );
    m.addIntArg( channel );
    m.addFloatArg( pt.x );
    m.addFloatArg( pt.y );
    m.addIntArg( n );
    m.addFloatArg( vol );
    m.addFloatArg( dur );
    o->sendMessage( m );
}

void NotePlayer::stopNoteOsc(OscSender *o, int melid, int channel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/note/off" );
    m.addIntArg( melid );
    m.addIntArg( channel );
    o->sendMessage( m );
}

