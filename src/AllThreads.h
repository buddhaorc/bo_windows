#ifndef ALLTHREADS_H
#define ALLTHREADS_H

#include <list>
#include "ofThread.h"

class AllThreads
{
    public:
        static void addThread(ofThread* t);
        static void removeThread(ofThread* t);
        static int getCount();
        static void stopAll();
        static void cleanUp();
        static void dump();

    private:
        static list<ofThread*> threads;
        static list<ofThread*> stopped;
        static ofMutex mutx;
};

#endif // ALLTHREADS_H
