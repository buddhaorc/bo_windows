#include "NoteMaster.h"
#include "NotePlayer.h"

using namespace std;

NoteMaster::NoteMaster()
{
    //ctor
    startThread(true, false);
}

NoteMaster::~NoteMaster()
{
    //dtor
}

void NoteMaster::stopAll()
{
    for (list<NotePlayer*>::iterator np = notes.begin(); np != notes.end(); np++)
    {
        (*np)->stop();
    }
    notes.clear();
    stopThread();
}

void NoteMaster::threadedFunction()
{
    while (isThreadRunning())
    {
        long closest = 10;
        long now = ofGetElapsedTimeMillis();
        lock();
        for (list<NotePlayer*>::iterator np = notes.begin(); np != notes.end(); )
        {
            NotePlayer *n = *np;
            long dt = n->tick(now);
            if (dt < 0)
            {
                // delete the note
                np = notes.erase(np);
                delete n;
            }
            else
            {
                if (dt < closest)
                {
                    closest = dt;
                }
                np++;
            }
        }
        unlock();
        ofSleepMillis(closest);
    }
}

void NoteMaster::play(NotePlayer *p)
{
    lock();
    notes.push_back(p);
    unlock();
}

int NoteMaster::getCount()
{
    return notes.size();
}
