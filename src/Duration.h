#ifndef DURATION_H
#define DURATION_H

class Note;
class Melody;
class ofxIniSettings;

class NoteDuration
{
    public:
        NoteDuration();
        virtual ~NoteDuration();

        void assignDurations(Melody *me);
        virtual void init(ofxIniSettings &ini);
    protected:
        virtual void setDuration(Note *no, Note *prev, float dscale);
        virtual float getDurationScale(Melody *me);
    private:
        float dscShort, dscMedium, dscLong;
        int cntShort, cntMedium, cntLong;
};

#endif // DURATION_H
