#ifndef REPLICATOR_H
#define REPLICATOR_H

#include <map>
#include "ofxOsc.h"

class ofxIniSettings;
class testApp;

// don't send to the nodes which are older than this
#define MAX_BO_AGE 60000

class OscTarget
{
public:
    OscTarget() { sender = new ofxOscSender(); lastSeen = 0; discovered = false; control = false; }
    ~OscTarget() { delete sender; }
    ofxOscSender *sender;
    long lastSeen;
    bool discovered;
    bool control; // true if boc; false if bo
};

class Replicator : public ofThread
{
    public:
        Replicator();
        virtual ~Replicator();

        void init(testApp *a, ofxIniSettings &ini);
        void invoke(); // handles all incoming messages
        void threadedFunction();

        // send messages to all remote hosts
        void key(int k);
        void key(int k, int mel);
        void select(int mel);
        void value(int k, float v, int mel);
        void svalue(int k, string s, int mel);
        void goTo(float x, float y);
        void midiControl(int control, int value);
        void midiNote(bool on, int ch, int pitch, int velo);
        void image(string img);
        void scale(string scl);
        void play(int mel);
        void pause(int mel);

        string myAddress() { return getThisIp() + ":" + ofToString(oscPort); }
        vector<string> getIfList();
        // the list of active nodes (all = false) only shows BO Controls, not BO
        vector<string> getSenders(bool all);

        void sendRouteHeader(const char *dest, const char *image, int wd, int ht, int melodyId, float cx, float cy, int col, int numpts);
        void sendRoutePoint(const char *dest, int melodyId, float x, float y);

        static string getThisIp();
        static string broadcast(string ip);
    protected:
    private:
        testApp *ta;
        int oscPort;
        int discoveryInterval;
        int maxAge;

        std::map<string, OscTarget*> senders;
        ofxOscSender discoverer;
        ofxOscReceiver receiver;

        void sendToAll(ofxOscMessage &m);
        void sendTo(const char*dest, ofxOscMessage &m);
        ofxOscSender* updateSenders(string addr, bool discovered, bool control);
};

#endif // REPLICATOR_H
