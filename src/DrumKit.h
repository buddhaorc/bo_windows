#ifndef DRUMKIT_H
#define DRUMKIT_H
#include <list>
#include <string>
#include "ofxMidi.h"

#define NUM_BEATS 128
// 81 - 35 + 1 = 57
#define MAX_DRUM_TYPES 57

#define DRUM_SZ 64
#define MAX_HIT 8

class Melody;
class Envelope;

class DrumInfo {

public:
    DrumInfo() : drumCnt(0), drumType(0), drumBit(false), ptr(0),
    maxHit(0), hit(0) {
        memset(vol, 0, DRUM_SZ * sizeof(vol[0]));
    }
    virtual ~DrumInfo() {}

    // v - volume - 0..127
    void sendDrum(Melody *m, int nmidi, float v, float dt, float dur);
    bool getDrumBit() { bool d = drumBit; drumBit = false; return d; }
    int getDrumCount() { return drumCnt; }
    void setDrumCount(int cnt) { drumCnt = cnt; }
    int getDrumType() { return drumType; }
    void setDrumType(int t) { drumType = t; }
    void changeMaxHit(int incr);

    ofPoint         centroid;
protected:
    virtual float getVolumeAdjustment();
    virtual bool getDrumHit();
private:
    bool            drumBit;
    int             drumCnt;
    int             drumType;
    float           vol[DRUM_SZ];
    int             ptr;
    int             maxHit;
    int             hit;
};

struct DrumEvent {
    std::list<Melody *> melodies;
    void remove(Melody *m);
};

class DrumKit
{
    public:
        DrumKit();
        virtual ~DrumKit();

        static void initTypes(std::string &dtypes, int *types, int &num);
        static void initDrums(std::string &dtypes);
        static void initMelos(std::string &dtypes);

        static int getDrumType(int idx);

        void clear();
        void incMelody(Melody *m);
        void decMelody(Melody *m);
        // needed when a melody is moved along the time axis
        void updateMelody(Melody *m);

        void assignDrum(Melody *m);
        void changeDrum(Melody *m);
        void plusDrum(Melody *m);
        void minusDrum(Melody *m);
        void tick(Envelope *env, float dt, float period);
        float getPosition() { return (float) m_ptr / NUM_BEATS; }

        static void mustHaveDrum(Melody *m);

        static int drumTypes[MAX_DRUM_TYPES];
        static int numDrumTypes;
        static int drumMelos[MAX_DRUM_TYPES];
        static int numDrumMelos;

    protected:
        virtual void drum(DrumEvent &de, Envelope * env, float period, float dt, float dur);
    private:
        int m_ptr;
        ofMutex mtx;
        DrumEvent timeline[NUM_BEATS];
        void remove(Melody *m);
        void insert(Melody *m);

};

#endif // DRUMKIT_H
