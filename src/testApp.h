#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"

// see http://in.somniac.me/2010/01/openframeworks-http-requests-xml-parser/
// http://www.addons.openframeworks.cc/files/

// http://www.companje.nl/doku.php/ofxinisettings
#include "ofxIniSettings.h"
#include "ofThread.h"
#include "ofxUI.h"

#include "Controller.h"
#include "TabMenu.h"
#include "Commander.h"
#include "Analyzer.h"
#include "Envelope.h"
#include "NoteLine.h"
#include "Duration.h"
#include "DrumKit.h"
#include "Replicator.h"
#include "SerialSender.h"
#include "Gui.h"
#include "NotePlayer.h"
#include "AudioSampler.h"
#include "WebController.h"
#include <map>
#include <list>

#define BPM 60
#define TICK_MS  200
#define ENV_OFFSET 60
#define MAX_DURATION 5

// keys for setValue()
#define VALUE_TICK  1
#define VALUE_BPM   2
#define VALUE_DRUM_COUNT    3
#define VALUE_DRUM_TYPE     4
#define VALUE_ECHO  5
#define VALUE_DURATION  6
#define VALUE_TRANSPOSE 7
#define VALUE_VOLUME    8
#define VALUE_QUALITY   9
#define VALUE_DEST  10
#define VALUE_CHORD 11
#define VALUE_MIDI  12
#define VALUE_ALPHA 13
#define VALUE_DISPLAY 14
#define VALUE_DYNAMIC 15
#define VALUE_MIDI_CH 16
#define VALUE_POSITION_X 17
#define VALUE_ARPEGGIO 18

#define DISP_MODE_NUMBERS 0
#define DISP_MODE_MINIM  1
#define DISP_MODE_GALLERY 2
#define DISP_MODE_FRAME  3
#define DISP_MODE_LAST  4

class testApp;
class MidiKeyMapper
{
    // b1 of a MIDI controller buttons
        int playBtn;
        int stopBtn;
        int instrKnob;
        testApp *app;
        std::map<int, string> ccmap;
    public:
        void init(ofxIniSettings &ini, testApp *ta);
        bool handle(ofxMidiMessage& e);
};

class InfoString {
    long validTo;
    string s;
    ofColor col;
public:
    InfoString() : validTo(0) { }
    void publish(string msg, ofColor c = ofColor::gray, int msec = 2000)
    {
        s = msg;
        col = c;
        validTo = ofGetElapsedTimeMillis() + msec;
    }
    ofColor getColor() { return col; }
    string getInfo() { return ofGetElapsedTimeMillis() < validTo ? s : ""; }
};

class testApp : public ofBaseApp, public ofxMidiListener {

        Gui gui;

        ofxIniSettings ini;

        int     t0; // last time the sound was updated
        // MIDI event generation
        int     tick, tick0;
        int     period;
        int     bpm;
        int     avar;
        int     pvar;
        float   tick2drum;
        int     cnt;
        int     lastClick; // last mouse click
        int     doubleClick; // threshold value
        ofPoint lastPoint;

        // Arduino LED period, mcs
        int     aperiod;

        MidiOut mout;
        ofxMidiIn   midiIn[MAX_MIDI];
        MidiControl periodCtr, tickCtr, arpeggioCtr;
        MidiKeyMapper midiBtns;
        OscSender osc;
        SerialSender serial;
        AudioSampler sampler;
        SimpleSynth synth;
        WebController webServer;

        Replicator replicator;
        vector<int> ignoredKeys;

        bool    help;
        bool    paused;
        bool    table;
        bool    reload;
        int     displayMode;
        long    escaping;
        int     alpha;
        bool    aspect;
        int     breathx, breathy; // screen animation
        ofTexture   screen;

        void updatePeriod(int cnt);
        void adjustVolume(Melody *m, int v, bool repl);
        void adjustTick(int v, bool repl);
        void adjustPeriod(int v, bool repl);
        float getIniFloat(const char *key, const char *def);
        void startOsc(const char*img);
        void stopOsc(const char*img);

        Analyzer az;
        Controller ctl;
        Envelope env;
        PatternSet ptrs;
        TabMenu tab;

        ofImage srcImg;
        NoteLine notes;

        // ADSR parameters
        int adsr;
        MidiControl ampCtr, attCtr, decCtr, susCtr, relCtr, fmCtr, sensCtr, dynamCtr;

        Poco::Mutex   mtx;
        Commander cmd;
        std::list<string> commandQueue;

        DrumKit     drumKit;

        char *m_appTitle;

        long exitAt; // set by startClosing
        void startClosing();

	public:
        testApp(const char*arg) : lastClick(0), doubleClick(500) { strcpy(imgFile, arg); }
		void setup();
        void exit();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);

        void newMidiMessage(ofxMidiMessage& eventArgs);
        bool newOscMessage(ofxOscMessage& eventArgs);
        void goTo(float x, float y);
        void select(int id);
        void loadImage(string fn, bool repl = true);
        void loadScale(string fn, bool repl = true);
        void loadStoredMelodies(string fn);
        void sendCurrentRouteTo(const char *dest);

        bool isAllowed(int key);
        // x, y in range [0..1]
        void mouseDoubleClick(float x, float y);
        ofRectangle getImageRect();

        static int getSynchLevel(long t, int period);
        static float getIniFloat(ofxIniSettings &settings, const char *key, const char *def);

        void saveBocConfig();
        void setValue(int key, float v, int mel = -1, bool repl = true);
        void setStrValue(int key, string s, int mel = -1, bool repl = true);
        float getValue(int key);
        string getStrValue(int key);
        void updateGui(bool playing) { gui.updateGui(playing); }
        void updateInstrumentList(Melody *me) { gui.updateInstruments(me); }
        vector<string> getInstrumentList(bool prefixed, Melody *me = 0);
        vector<string> getMidiList();
        int getTick() { return tick0; }
        int getBpm() { return bpm; }
        ofPoint getDimensions() { return ofPoint(srcImg.width, srcImg.height); }
        int getCurrentMelody() { return ctl.getCurrentMelody()->m_Id; }
        Melody * getMelody(int id) { return ctl.getMelody(id); }
        void dumpRoutes(ostream &os, const char *img) { ctl.dumpRoutes(os, img); }

        float getDrumOffset();

		char imgFile[256];
		InfoString info;

        friend class MidiKeyMapper;
        friend class Gui;
};

#endif
