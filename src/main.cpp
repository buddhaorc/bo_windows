#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"
#include "glut.h"

//========================================================================
int main(int argc, char **argv) {

    const char *imgf = "";
    if (argc > 1) {
        imgf = argv[1];
    }

    ofAppGlutWindow window;
    window.setGlutDisplayString("rgba double");
	ofSetupOpenGL(&window, 480, 360, OF_FULLSCREEN);			// <-------- setup the GL context
	glutSetCursor(GLUT_CURSOR_CROSSHAIR);

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp( new testApp(imgf));
}
