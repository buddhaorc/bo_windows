#ifndef GUI_H
#define GUI_H

#include "ofxIniSettings.h"
#include "ofxUI.h"
#include "Envelope.h"

class testApp;
class Replicator;
class Melody;

class Gui
{
    public:
        Gui();
        virtual ~Gui();

        void init(testApp *a, ofxIniSettings &ini);
        void showImageDir();
        void showMeloDir();
        void showGear(bool show) { gear->setVisible(show); }
        void toggleNetwork(Replicator &replicator);
        bool assignRoute(int mel, Replicator &replicator);

        void guiEvent(ofxUIEventArgs &e);
        void guiDirEvent(ofxUIEventArgs &e);
        void updateGui(bool pl = true);
        void updatePlaying();
        void updateInstruments(Melody *me);

        bool isHit(float x, float y) {
            return guiM->isHit(x, y) ||
            (guiI && guiI->isHit(x, y)) ||
            (guiP && guiP->isHit(x, y)) ||
            guiS->isHit(x, y) ||
            guiD->isHit(x, y) ||
            guiG->isHit(x, y) ||
            (guiLoadImg && guiLoadImg->isHit(x, y)) ||
            (guiLoadMel && guiLoadMel->isHit(x, y));
        }

        bool isVisible() {
            return guiM->isVisible();
        }
        void toggleVisible();

        // envelop sliders are public so they can be adjusted by MIDI events
        ofxUISlider *envelSliders[NSLIDERS];

    protected:
    private:
        // a little icon in the corner
        ofxUICanvas *gear;
        ofxUIImageToggle *gearBtn;

        ofxUICanvas *guiM, *guiS, *guiI, *guiD, *guiG, *guiP, *guiLoadImg, *guiLoadMel;
        ofxUILabel *melodyLabel;
        ofxUIToggle *play;
        ofxUIToggle *envelToggle;
        ofxUIToggle *veloToggle;
        ofxUIToggle *noteToggle;
        ofxUIToggle *trackToggle;
        ofxUIToggle *reverseToggle;
        ofxUIToggle *muteToggle;
        ofxUIToggle *soloToggle;
        ofxUIButton *reduceBtn;
        ofxUIButton *extremeBtn;
        ofxUIButton *mergeBtn;
        ofxUIButton *splitBtn;
        ofxUIButton *pauseBtn;
        ofxUIButton *rmPauseBtn;
        ofxUIButton *restoreBtn;
        ofxUIButton *assignBtn;

        // Sliders
        ofxUISlider *drumSlider;
        ofxUISlider *echoSlider;
        ofxUISlider *volumeSlider;
        ofxUISlider *dynamSlider;
        ofxUISlider *stacattoSlider;
        ofxUISlider *qualitySlider;

        ofxUIRadio *destList;
        ofxUIRadio *instrList;
        ofxUIRadio *chordList;
        ofxUIRadio *chordTypeList;
        ofxUIRadio *drumList;

        // global
        ofxUIToggle *pause;
        ofxUISlider *tickSlider;
        ofxUISlider *bpmSlider;
        ofxUISlider *alphaSlider;
        ofxUISlider *arpeggioSlider;
        ofxUIRadio *midiList;
        ofxUIRadio *playingList;
        ofxUIRadio *drummingList;
        ofxUIButton *boConfigBtn;
        ofxUIButton *imageDirBtn;
        ofxUIButton *meloDirBtn;
        ofxUIButton *quitBtn;
        ofxUIToggle *windowToggle;

        // directory lists
        ofxUIRadio *imageFiles;
        ofxUIRadio *melodyFiles;

        // network info
        ofxUISuperCanvas *guiN;
        // a dialog to assign a route to a remote BO client
        ofxUISuperCanvas *guiAssign;

        testApp *ta;
        int wd;

        bool dirty;

        void activateRadioById(ofxUIRadio *r, int id);
        ofxUIRadio *addFilesRadio(ofxUICanvas *gui, string dir, string caption, string ext, string filter = "");
};

#endif // GUI_H
