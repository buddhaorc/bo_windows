#include "AllThreads.h"

list<ofThread*> AllThreads::threads;
list<ofThread*> AllThreads::stopped;
ofMutex AllThreads::mutx;

void AllThreads::addThread(ofThread* t)
{
    mutx.lock();
    threads.push_back(t);
    mutx.unlock();
}

void AllThreads::removeThread(ofThread* t)
{
    mutx.lock();
    threads.remove(t);
    stopped.push_back(t);
    mutx.unlock();
}

int AllThreads::getCount()
{
    mutx.lock();
    int n = threads.size();
    mutx.unlock();
    return n;
}

void AllThreads::stopAll()
{
    mutx.lock();
    for(list<ofThread*>::iterator itr = threads.begin(); itr != threads.end(); ++itr)
    {
        ofThread * t = *itr;
        t->stopThread();
    }
    mutx.unlock();
}

void AllThreads::cleanUp()
{
    mutx.lock();
    for(list<ofThread*>::iterator itr = stopped.begin(); itr != stopped.end(); ++itr)
    {
        ofThread *t = *itr;
        delete t;
    }
    stopped.clear();
    mutx.unlock();
}

void AllThreads::dump()
{
    mutx.lock();
    for(list<ofThread*>::iterator itr = threads.begin(); itr != threads.end(); ++itr)
    {
        ofThread *t = *itr;
        cout << t->getThreadName() << " ";
    }
    mutx.unlock();
}
