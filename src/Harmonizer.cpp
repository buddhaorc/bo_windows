#include "ofxIniSettings.h"
#include "Harmonizer.h"
#include "Melody.h"

#define HIGH_PROB 80
#define LOW_PROB  20
//Harmonizer::Harmonizer() : m_ctype(MAJOR)
//{
//    //ctor
//}
//
//Harmonizer::~Harmonizer()
//{
//    //dtor
//}

void Harmonizer::init(ofxIniSettings &ini)
{
    int low = ini.get("harmonizer.low", LOW_PROB);
    int high = ini.get("harmonizer.high", HIGH_PROB);

    const string defaultProbabilities = "10,20,30,40,50,60,70,80,90,100,90,80";
    for (int i=0; i < NOTE_NUM; ++i)
    {
        m_noteHist[i] = 0;
        m_noteProbs[i] = low;
        m_intrProbs[i] = low;
    }

    string ctype = ini.get("harmonizer.chords", string("major"));
    if (ctype == "major")
    {
        m_ctype = MAJOR;
        m_noteProbs[0] = m_noteProbs[4] = m_noteProbs[7] = high;
    }
    else if (ctype == "minor")
    {
        m_ctype = MINOR;
        m_noteProbs[0] = m_noteProbs[3] = m_noteProbs[7] = high;
    }
    else if (ctype == "augmented")
    {
        m_ctype = AUGMENTED;
        m_noteProbs[0] = m_noteProbs[4] = m_noteProbs[8] = high;
    }
    else if (ctype == "diminished")
    {
        m_ctype = DIMINISHED;
        m_noteProbs[0] = m_noteProbs[3] = m_noteProbs[6] = high;
    }
    else if (ctype == "custom")
    {
        m_ctype = CUSTOM;
        string probs = ini.get("harmonizer.probabilities", defaultProbabilities);
        vector<string> pp = ofSplitString(probs, ", ", true, true);
        for (int i = 0; i < pp.size(); i++)
        {
            m_noteProbs[i] = ofToFloat(pp[i]);
            cout << " " << m_noteProbs[i];
        }
    }
    cout << endl;

    string intervals = ini.get("harmonizer.intervals", defaultProbabilities);
    vector<string> ipp = ofSplitString(intervals, ", ", true, true);
    for (int i = 0; i < ipp.size(); i++)
    {
        m_intrProbs[i] = ofToFloat(ipp[i]);
        cout << " " << m_intrProbs[i];
    }
    cout << endl;
}

void Harmonizer::addHistogram(Melody *mel)
{
    list<Note*>::iterator nit = mel->melody.begin();
    for ( ; nit != mel->melody.end(); nit++)
    {
        m_noteHist[(*nit)->note0 % NOTE_NUM]++;
    }
}

void Harmonizer::processHistogram()
{
    int mx = 0;
    int imx = -1;
    for (int i=0; i<NOTE_NUM; ++i)
    {
        if (m_noteHist[i] > mx)
        {
            mx = m_noteHist[i];
            imx = i;
        }
    }
    m_harmBase = imx;
//    cout << endl << "Base: " << Note::noteString(m_harmBase) << endl;
}

void Harmonizer::processMelody(Melody *mel, bool interv)
{
    std::list<Note*>::iterator noit = mel->melody.end();
    Note *prev = *(--noit); // previous for the first is the last note
    for (noit = mel->melody.begin(); noit != mel->melody.end(); noit++)
    {
        Note *current = *noit;
        int n1 = current->note0;
        int n0 = prev->note0;
        // the interval
        int nn = n1 - n0;
        int n = n1 % NOTE_NUM;
        while (nn < 0) { nn += NOTE_NUM; }
        nn %= NOTE_NUM;

        cout << Note::noteString(current->note0);

        if (interv)
        {
            // keep changing interval until it is good
            while (ofRandom(0, 100) > m_intrProbs[nn])
            {
                cout << "(" << nn << ")";
                nn = ofRandom(0, NOTE_NUM);
            }
            n = (n0 + nn + NOTE_NUM) % NOTE_NUM;
        }
        else
        {
            // find the new note based on the good interval
    //        int n = (n0 + nn - m_harmBase + NOTE_NUM) % NOTE_NUM;
            // keep changing the note until it is good
            while (ofRandom(0, 100) > m_noteProbs[n])
            {
                cout << "(" << Note::noteString(n) << ")";
                n = ofRandom(0, NOTE_NUM);
            }
        }

        // find the correct octave for the updated note
        int oct = n1 / NOTE_NUM * NOTE_NUM;
        int updated = oct + n;
        // how far has the note been moved by correction?
        int diff = updated - current->note0;
        if (oct && (diff > NOTE_NUM / 2))
        {
            updated -= NOTE_NUM;
        }
        else if (diff < -NOTE_NUM / 2)
        {
            updated += NOTE_NUM;
        }

        cout << "->" << Note::noteString(updated) << " ";
        current->note0 = updated;
        prev = current;
    }
    cout << endl;
}

int Harmonizer::harmonize(int note, int prev, float relTime)
{

}

void Harmonizer::dumpHistogram()
{
    cout << "HISTO BASE " << m_harmBase << "; ";
    for (int i=0; i<NOTE_NUM; ++i)
    {
        cout << m_noteHist[i] << " ";
    }
    cout << endl;
}

// the melody histogram
int Harmonizer::m_noteHist[NOTE_NUM];
// computed from the duration histogram
int Harmonizer::m_harmBase;
// probabilities for notes
int Harmonizer::m_noteProbs[NOTE_NUM];
// probabilities for intervals
int Harmonizer::m_intrProbs[NOTE_NUM];

Harmonizer::ChordType Harmonizer::m_ctype;
