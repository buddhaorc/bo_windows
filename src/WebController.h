#ifndef WEBCONTROLLER_H
#define WEBCONTROLLER_H
#include "ofxIniSettings.h"
#include "Poco/Net/HTTPServer.h"

class testApp;

class CtrlDescriptor
{
    public:
        enum CtlType {
            NoCtl,
            MidiCtl,
            ValueCtl,
            KeyCtl
        };

        CtrlDescriptor() { ctlType = NoCtl; }
        CtlType setType(string v, string &valStr);
        virtual string make_java_script_value() = 0;
        virtual void update(string v) = 0;

        void sendNewMidi(int val);

        CtlType ctlType;

        string title;
        int channel, cc, key, val;
};

class SliderCtrlDescriptor : public CtrlDescriptor
{
    public:
        SliderCtrlDescriptor(string v);
        virtual string make_java_script_value();
        virtual void update(string v);

        float value, minval, maxval;
};

class ToggleCtrlDescriptor : public CtrlDescriptor
{
    public:
        ToggleCtrlDescriptor(string v);
        virtual string make_java_script_value();
        virtual void update(string v);

        bool value;
};

class ListCtrlDescriptor : public CtrlDescriptor
{
    public:
        ListCtrlDescriptor(string v);
        virtual string make_java_script_value();
        virtual void update(string v);

        int value;
        vector<string> items;
};

class PicCtrlDescriptor : public CtrlDescriptor
{
    public:
        PicCtrlDescriptor(string v);
        virtual string make_java_script_value();
        virtual void update(string v) {} // not used
};

class PageDescriptor
{
    public:
        PageDescriptor(string t, string bf);
        virtual void update(string t, string v);
        void update_page_values(string q);

        string make_html_list_element(string key);
        string make_html_slider_values();
        virtual string make_html_init() { return ""; }

        testApp *app;
        string title;
        string button_format;
        vector<CtrlDescriptor*> controls;
};

class PicPageDescriptor : public PageDescriptor
{
    public:
        PicPageDescriptor(string t, string bf, string def);
        virtual void update(string t, string v);
        virtual string make_html_init();
};

class WebController
{
    public:
        WebController();
        virtual ~WebController();
        void init(testApp *app, ofxIniSettings &ini);
        void stop();

        static char *read_file(string fn, int &sz);
        static map<string, PageDescriptor*> pages;
        static string index_page, page_template, wwwdir;
        static testApp *theApp;
    protected:
    private:
        Poco::Net::HTTPServer *server;
};

#endif // WEBCONTROLLER_H
