#include "ofMain.h"
#include "Controller.h"
#include "DrumKit.h"
#include "testApp.h"
#include "Scale.h"

Controller::Controller() : m_Music(0), soloMelody(0), m_Img(0), m_Ptrns(0),
    mn(0), mx(127), qua(0), groupRGB(false)
{
    //ctor
}

Controller::~Controller()
{
    //dtor
}

void Controller::setImage(ofImage *img, testApp *a) {
    m_Img = img;
    m_App = a;
    int w = img->getWidth();
    int h = img->getHeight();
    cout << "Controller::setImage " << w << " x " << h << endl;
    m_CvImg.allocate(w, h);
    ofPixels &px = img->getPixelsRef();
    cout << "pix bpchannel " << px.getBitsPerChannel() << " bpp " << px.getBytesPerPixel() << endl;
    m_CvImg.setFromPixels(img->getPixels(), w, h);
}

void Controller::init(ofxIniSettings &ini)
{
    quaCtl.init(ini, "quality", &qua);
    //groupRGB = ini.get("group", 1) != 0;
    meloKnob = ini.get("meloKnob", 15);
    m_ND.init(ini);
    string stored = ini.get("stored", string(""));
    if (stored != "")
    {
        m_restore = stored;
    }

    m_autoplay = ini.get("autoplay", false);

    Melody::echoDelay = ini.get("echoDelay", 2000);
    Melody::echoVelocity = ini.get("echoVelocity", -10);
    Melody::echoPitch = ini.get("echoPitch", 0);

    Melody::multiPause = ini.get("multipause", true);
    Melody::pauseDuration = ini.get("pauseduration", 50);

    Melody::historyOn = ini.get("history", true);
}


const char* Controller::info(float dur)
{
    static char buf[256];
    Melody *m = soloMelody ? soloMelody : *currentMelody;
    int n = snprintf(buf, sizeof(buf) - 1, "%s [%s] %s",
            m->info(dur),
            Note::noteString(m->current()->note),
            groupRGB ? "[Group]" : "");
    buf[n] = 0;
    return buf;
}

void Controller::control(int key)
{
    Melody *me = *currentMelody;
    string dest = me ? me->getDestName() : "";
    std::list<Melody*>::iterator meitr;
    float relx, rely;
    ofRectangle r = m_App->getImageRect();

    switch (key)
    {
    case 127: // delete
        if (!me->m_HasChild)
        {
            if (me == soloMelody) soloMelody = 0;
            me->stop(me->current());
            meitr = currentMelody;
            control(359); // next melody
            m_Music->erase(meitr);
        }
        break;
    case 362: // home
        currentMelody = m_Music->begin();
        break;
    case 363: // end
        currentMelody = m_Music->end();
        currentMelody--;
        break;
    case 364: // insert
        (*currentMelody)->inserted = !((*currentMelody)->getPlayingState() & MINST);
        break;
    case 359: // down
        if (!soloMelody && (++currentMelody == m_Music->end())) currentMelody = m_Music->begin();
        if (soloMelody)
        {
            m_App->info.publish("solo " + ofToString(soloMelody->m_Id), ofColor::yellow);
        }
        (*currentMelody)->display = 10;
        break;
    case 357: //up
        if (!soloMelody)
        {
            if (currentMelody != m_Music->begin()) currentMelody--;
            else { currentMelody = m_Music->end(); currentMelody--; }
        }
        else
        {
            m_App->info.publish("solo " + ofToString(soloMelody->m_Id), ofColor::yellow);
        }
        (*currentMelody)->display = 10;
        break;
//    case 360: // PgUp
//        if (!soloMelody) pageUp();
//        (*currentMelody)->display = 10;
//        break;
//    case 361: // PgDn
//        if (!soloMelody) pageDn();
//        (*currentMelody)->display = 10;
//        break;
    case 356: // Left
        if (!soloMelody) prevPlaying(MPLAY | MDRUM);
        else
        {
            m_App->info.publish("solo " + ofToString(soloMelody->m_Id), ofColor::yellow);
        }
        (*currentMelody)->display = 10;
        break;
    case 358: // Right
        if (!soloMelody) nextPlaying(MPLAY | MDRUM);
        else
        {
            m_App->info.publish("solo " + ofToString(soloMelody->m_Id), ofColor::yellow);
        }
        (*currentMelody)->display = 10;
        break;
    case 'd':
        (*currentMelody)->display = 20;
        break;
    case 'o':
        if (soloMelody) soloMelody = 0;
        else
        {
            soloMelody = *currentMelody;
            stopAll();
        }
        break;
    case 'g':
        // groupRGB = !groupRGB;
        break;
    case 'n':
        relx = ofMap(m_App->mouseX, r.getMinX(), r.getMaxX(), 0, 1, true);
        rely = ofMap(m_App->mouseY, r.getMinY(), r.getMaxY(), 0, 1, true);

        if (soloMelody)
        {
            list<Melody*>::iterator n = nearestMelody(relx, rely);
            if ((*n)->getPlayingState() && MPLAY)
            {
                soloMelody = *(currentMelody = n);
            }
            else
            {
                m_App->info.publish("solo " + ofToString(soloMelody->m_Id), ofColor::yellow);
            }
        }
        else
        {
            currentMelody = nearestMelody(relx, rely);
        }
        (*currentMelody)->display = 10;
        break;
    case 'M':
        m_App->info.publish( "exported to " + (*currentMelody)->writeMidi(), ofColor::white);
        break;
    default:
        if (!controlGroup(key) && !(*currentMelody)->control(key))
        {
            cout << "controller " << key << endl;
        }
    }
    if (me != *currentMelody || (*currentMelody)->getDestName() != dest)
    {
        m_App->updateInstrumentList(*currentMelody);
    }
}
//--------------------------------------------------------------
list<Melody*>::iterator Controller::nearestMelody(float x, float y)
{
    list<Melody*>::iterator best = currentMelody;
    float mx =  (float) x * m_Img->getWidth();
    float my =  (float) y * m_Img->getHeight();
    float b = 1e38;
    for (list<Melody*>::iterator itr = m_Music->begin(); itr != m_Music->end(); ++itr) {
        ofPoint p = (*itr)->getCenter();
        float d = pow(p.x - mx, 2) + pow(p.y - my, 2);
        if (d < b) {
            b = d;
            best = itr;
        }
    }
    return best;
}
//--------------------------------------------------------------
bool Controller::controlGroup(int key)
{
    Melody *mlds[MAX_GROUP_SZ];
    int cnt = 1;
    getGroup(mlds, cnt);
    for (int idx = 0; idx < cnt; ++idx)
    {
        if (!control(mlds[idx], key)) return false;
        mlds[idx]->display = 10;
    }
    return true;
}

//--------------------------------------------------------------
void Controller::stopAll()
{
    list<Melody*>::iterator itr = m_Music->begin();
    for ( ; itr != m_Music->end(); ++itr)
    {
        (*itr)->stop();
    }
}

//--------------------------------------------------------------
void Controller::getGroup(Melody **mlds, int &cnt)
{
    if (groupRGB)
    {
        ofRectangle &r = (*currentMelody)->boundingRect;
        list<Melody*>::iterator itr = m_Music->begin();
        for (cnt = 0; itr != m_Music->end(); ++itr)
        {
            Melody *m = *itr;
            if (belong(m->boundingRect, r))
            {
                mlds[cnt] = m;
                if (++cnt == MAX_GROUP_SZ) break;
            }
        }
    }
    else
    {
        mlds[0] = *currentMelody;
        cnt = 1;
    }
}

//--------------------------------------------------------------
Melody *Controller::getMelody(int id)
{
    for (list<Melody*>::iterator itr = m_Music->begin(); itr != m_Music->end(); ++itr) {
        Melody * m = (*itr);
        if (m->m_Id == id) return m;
    }
    return 0;
}

//--------------------------------------------------------------
void Controller::setCurrentMelody(int id)
{
    for (list<Melody*>::iterator itr = m_Music->begin(); itr != m_Music->end(); ++itr) {
        Melody * m = (*itr);
        if (m->m_Id == id)
        {
            if (soloMelody && ((*itr)->getPlayingState() & MPLAY == 0))
            {
                m_App->info.publish("solo " + ofToString(soloMelody->m_Id), ofColor::yellow);
                m->display = 15;
                return;
            }
            currentMelody = itr;
            m->display = 10;
            return;
        }
    }
    cout << "not selected melody " + id << endl;
    m_App->info.publish("not selected melody " + ofToString(id) + ": not found", ofColor::yellow);
}

//--------------------------------------------------------------
static char *safeChars = "HJVKLN()fuvij:;";

//--------------------------------------------------------------
bool Controller::control(Melody *m, int key)
{
    if (key == 'p') return false; // we don't process it here
    // melodies are paused and restarted unless the key is in the safe list
    bool paused = m->paused  || strchr(safeChars, key) || (key == 268) || (key == 267); // F11, F12
    if (!paused) m->control('p');
    bool res = true;
    switch (key)
    {
    case 'm':
        m->merge();
        break;
    case 'x':
        m->extreme();
        break;
    case 's':
        m->split();
        break;
    case 'r':
        m->reduce();
        break;
    case 'y':
    case 'Y':
        nextPattern(m, key == 'Y');
        break;
    case 'z':
        m->addPauses();
        break;
    case 'Z':
        m->removePauses();
        break;
    default:
        res = false;
    }
    if (!paused) m->control('p');
    if (res)
    {
        m->storeKey(key);
    }

    return res;
}

//--------------------------------------------------------------
void Controller::mouse(float x, float y)
{
    Melody *m = *currentMelody;
    m->stop(m->current());
    m->goTo(x * m_Img->width, y * m_Img->height);
}

//--------------------------------------------------------------
void Controller::mouseDbl(float x, float y)
{
    list<Melody*>::iterator n = nearestMelody(x, y);

    // only allow switching to playing melodies
    if (soloMelody && !((*n)->getPlayingState() & MPLAY)) {
        (*currentMelody)->display = 15;
        m_App->info.publish("solo " + ofToString(soloMelody->m_Id), ofColor::yellow);
        return;
    }

    if (soloMelody)
    {
        soloMelody = *n;
    }

    (*(currentMelody = n))->display = 10;
}

//--------------------------------------------------------------
void Controller::nextPattern(Melody *m, bool init)
{
    if (!init && (++m->ptrnIdx >= m_Ptrns->getNumber())) {
        m->ptrnIdx = 0;
    }
    m->stop();
    ofPoint &p = m->current()->pt;
    int x = p.x;
    int y = p.y;
    m->generate(m_Img->width, m_Img->height, init ? 0 : m_Ptrns->getPtrn(m->ptrnIdx), &m_ND);
    m->goTo(x, y);
}

//--------------------------------------------------------------
void Controller::nextPlaying(int mode)
{
    searchDn(-1,  mode);
}

//--------------------------------------------------------------
void Controller::prevPlaying(int mode)
{
    searchUp(-1,  mode);
}

//--------------------------------------------------------------
bool Controller::belong(ofRectangle &r1, ofRectangle &r2)
{
//    cout << "belong " << r1.x << " " << r1.y << " and " << r2.x << " " << r2.y << endl;
    ofRectangle r = m_CvImg.getIntersectionROI(r1, r2);
    float s1 = r1.height * r1.width;
    float s2 = r2.height * r2.width;
    float s = r.height * r.width;
    if (!s || !s1 || !s2) return false;
//    cout << "areas s=" << s << " s1=" << s1 << " s2=" << s2 << endl;
    float a = s1 / s2;
    float b = s / s1;
    return (a > 0.9) && (a < 1.1) && (b > 0.9) && (b < 1.1);
}
//--------------------------------------------------------------
void Controller::searchUp(int id, int mode)
{
    list<Melody*>::iterator itr = currentMelody;
    ofRectangle &r = (*currentMelody)->boundingRect;
//    while (itr-- != m_Music->begin()) {
//        if ((*itr)->m_Id == id || (playing && (!(*itr)->paused))) {
    while (itr-- != m_Music->begin()) {
        int st = (*itr)->getPlayingState();
//        if (belong((*itr)->boundingRect, r) || (mode & st)) {
        if (mode & st) {
            currentMelody = itr;
            return;
        }
    }
    for(itr = m_Music->end(); ;) {
//        if ((*(--itr))->m_Id == id || (playing && !(*itr)->paused)) {
        --itr;
        int st = (*itr)->getPlayingState();
//        if (belong((*itr)->boundingRect, r) || (mode & st)) {
        if (mode & st) {
            currentMelody = itr;
            return;
        }
        if (itr == m_Music->begin()) {
            break;
        }
    }
}

//--------------------------------------------------------------
void Controller::searchDn(int id, int mode)
{
    list<Melody*>::iterator itr = currentMelody;
    ofRectangle &r = (*currentMelody)->boundingRect;
    while (++itr != m_Music->end()) {
        int st = (*itr)->getPlayingState();
//        if (belong((*itr)->boundingRect, r) || (mode & st)) {
        if (mode & st) {
            currentMelody = itr;
            return;
        }
    }
    for(itr = m_Music->begin(); itr != m_Music->end(); ++itr) {
        int st = (*itr)->getPlayingState();
//        if (belong((*itr)->boundingRect, r) || (mode & st)) {
        if (mode & st) {
            currentMelody = itr;
            return;
        }
    }
}

//--------------------------------------------------------------
void Controller::pageUp()
{
    int id = (*currentMelody)->m_Id;
    searchUp(id, false);
}

//--------------------------------------------------------------
void Controller::pageDn()
{
    int id = (*currentMelody)->m_Id;
    searchDn(id, false);
}
//--------------------------------------------------------------
bool Controller::handle(ofxMidiMessage& e, int ch, bool envEdit) {
    if (ch < 0)
    {
        if (envEdit && quaCtl.handle(e))
        {
            Melody *mlds[MAX_GROUP_SZ];
            int cnt = 1;
            getGroup(mlds, cnt);
            for (int idx = 0; idx < cnt; ++idx)
            {
                mlds[idx]->quality = qua;
            }
            return true;
        }
        else if (e.control == meloKnob) {
            if (!soloMelody)
            {
                int n = m_Music->size();
                int m = e.value * (n - 1) / 127;
                std::list<Melody*>::iterator c = m_Music->begin();
    //            cout << "b2=" << e.byteTwo << " m=" << m << " of " << n << endl;
                for (int i=0; i < m; ++i, ++c) { /* empty */ }
                (*(currentMelody = c))->display = 5;
            }
            else
            {
                m_App->info.publish("solo " + ofToString(soloMelody->m_Id), ofColor::yellow);
                (*currentMelody)->display = 15;
            }
            return true;
        }

        else
            return false;
    }
//    int val = e.byteTwo;
//    float res = float(val) * (mx - mn) / 127 + mn;
//    cout << "Melody Controller " << ch << " "<< res << endl;
//    if (v) *v = res;
//    else if (fv) *fv = res;
//    else return false;

    return false;
}

//--------------------------------------------------------------
ostream &Controller::write(ostream &os)
{
    os << "{Music=";
    for (std::list<Melody*>::iterator m = m_Music->begin(); m != m_Music->end(); ++m)
    {
        (*m)->write(os);
    }
    os << "}";
}

//--------------------------------------------------------------
void Controller::read(istream &os)
{

}

//--------------------------------------------------------------
void Controller::dumpRoutes(const char *name, const char *img)
{
    ofstream of(name);
    dumpRoutes(of, img);
    of.close();
}

void Controller::dumpRoutes(ostream &of, const char *img) {
    of << m_Img->width << "," << m_Img->height << " | " << (img ? img : "") << endl;

    for (std::list<Melody*>::iterator m = m_Music->begin(); m != m_Music->end(); ++m)
    {
        Melody *me = *m;
        if (me->m_Ch0 != -1 || me->isDrumming()) {
            ofColor col = m_Img->getColor(me->getCenter().x, me->getCenter().y);
            of << me->m_Id << "|" << me->getCenter().x << "," << me->getCenter().y << "|" << col.getHex() << "|" << me->points.size() << "|";
            for (std::list<ofPoint*>::iterator p = me->points.begin(); p != me->points.end(); ++p)
            {
                ofPoint *po = *p;
                of << "(" << po->x << ","  << po->y << ")";
            }

            of << endl;
        }
    }
    of.flush();
}

//--------------------------------------------------------------
void Controller::dumpMelodies(const char *name)
{
    ofstream of((string(name) + "-" + ofGetTimestampString("%Y%m%d-%H%M%S") + ".txt").c_str());
    if (Scale::enabled())
    {
        of << "!scale=" << Scale::getFileName() << endl;
    }

    if (m_App->imgFile && m_App->imgFile[0])
    {
        of << "!image=" << m_App->imgFile << endl;
    }

    of <<"!bpm=" << m_App->getBpm() << endl;
    of <<"!tick=" << m_App->getTick() << endl;

    for (std::list<Melody*>::iterator m = m_Music->begin(); m != m_Music->end(); ++m)
    {
        Melody *me = *m;
        if (me->isDrumming() || (me->m_Ch0 != -1)) {
            dumpMelody(me, of);
        }
    }
    of.close();
}

//--------------------------------------------------------------
void Controller::dumpMelody(Melody *me, ostream &of)
{
    if (me->m_ParentId >= 0)
    {
        of << me->m_Id << "," << me->m_ParentId;
    }
    else
    {
        of << me->m_Id;
    }
    of << "=" << me->keystory;
    of << '?';
    if (me->portamento) of << "port=on;";
    if (me->vibrato) of << "vibr=on;";
    of << "vol=" << me->getVolume() << ";";
    of << "ch=" << me->m_Ch0 << ";";
    if (me->isDrumming()) {
        of << "drtype=" << me->m_pDrum->getDrumType() << ";drcnt=" << me->m_pDrum->getDrumCount() << ";";
    }
    if (me->getEcho()) {
        of << "echo=" << me->getEcho() << ";";
    }
    of << "dd=" << me->getDynamic() << ";";
    of << "quality=" << me->quality << ";";
    of << "chord=" << me->getChordName() << ";";
    of << "dest=" << me->getDestName() << ";";
    of << "offset=" << me->getCenter().y << ";";
    of << "positionx=" << me->getCenter().x << ";";
    of << "legato=" << me->getLegato() << ";";
    of << "playing=" << me->getPlayingState() << ";";
    of << "cc=" << me->ccFlags << ";";
    of << endl;
}

//--------------------------------------------------------------
string Controller::restoreMelodies(const char *fn)
{
    if (fn) m_restore = fn;
    if (m_restore.length() == 0)
    {
        return "";
    }
    cout << "restoring melodies from " << m_restore << endl;

    ifstream stf(m_restore.c_str());
    if (!stf) return "";

    bool header = true;
    string key;
    string ignored = "UpY";
    std::map<int, Melody*> mels;
    std::list<Melody*>::iterator m = m_Music->begin();
    while(getline(stf,key)) { //read
        int sep = key.find('=', 0);
        if (sep == string::npos) continue;

        if (header) {
            if (key.at(0) == '!') {
                string cmd = key.substr(1, sep - 1);
                string val = key.substr(sep + 1);
                if (cmd == "scale") {
                    m_App->loadScale(val, true);
                } else if (cmd == "image") {
                    m_App->loadImage(val, true);
                    m = m_Music->begin();
                } else if (cmd == "bpm") {
                    m_App->setValue(VALUE_BPM, ofToInt(val));
                } else if (cmd == "tick") {
                    m_App->setValue(VALUE_TICK, ofToInt(val));
                } else {
                    cout << "Unknown " << cmd << " : " << val << endl;
                }
                continue;

            } else {
                header = false;
            }
        }
cout << key << endl;

        int id, parent = -1;
        string idStr = key.substr(0, sep);
        int idcnt = sscanf(idStr.c_str(), "%d,%d", &id, &parent);
        if (!idcnt) continue;

        if (idcnt == 2 && parent != -1)
        {
            Melody *par = mels[parent];
            if (!par)
            {
                cout << "not found parent " << parent << endl;
            }
            else
            {
                // generate melody 'id' from the parent
                Melody *m = par->copy(id, 4);
                m_Music->push_back(m);
                m->generate(par->m_ImgWidth, par->m_ImgHeight, 0, getND());
            }
        }

        for (; m != m_Music->end(); ++m)
        {
            if ((*m)->m_Id == id)
            {
                if ((*m)->m_ParentId < 0)
                {
                    // it has no parent
                    mels.insert(std::pair<int,Melody*>(id, *m));
                }

                m_App->select(id);
                m_App->keyPressed('Y');

                int sep2 = key.find('|', 0);
                bool newformat = false;
                if (sep2 == string::npos)
                {
                    sep2 = key.find('?', 0);
                    newformat = sep2 != string::npos;
                }

                string mkey = key.substr(sep+1, sep2 != string::npos ? sep2 - sep - 1 : -1);
                for (int i=0; i<mkey.size(); ++i)
                {
                    if (ignored.find(mkey[i]) == string::npos)
                    {
                        char k = mkey[i];
                        if (k == 'z' && i < mkey.size() && mkey[i + 1] == '[')
                        {
                            int closing = mkey.find(']', i + 2);
                            if (closing != string::npos)
                            {
                                string idxStr = mkey.substr(i + 2, closing - i - 2);
                                int idx = ofToInt(idxStr);
                                i = closing;
                                (*m)->setCurrentIdx(idx);
                            }
                        }
                        m_App->keyPressed(k);
                    }
                }

                Melody *me = (*m);

                if (newformat)
                {
                    vector<string> pars = ofSplitString(key.substr(sep2 + 1, -1), ";", true, true);
                    for (int i=0; i < pars.size(); ++i)
                    {
                        vector<string> kv = ofSplitString(pars[i], "=", true, true);
                        if (kv.size() != 2)
                        {
                            cout << "ignored " << pars[i] << endl;
                        }
                        else
                        {
                            string k = kv[0];
                            string v = kv[1];
                            if (k == "ch") {
                                me->m_Ch0 = ofToInt(v);
                            } else if (k == "vol") {
                                m_App->setValue(VALUE_VOLUME, ofToInt(v), me->m_Id);
                            } else if (k == "port") {
                                if (v == "on") m_App->keyPressed(';');
                            } else if (k == "vibr") {
                                if (v == "on") m_App->keyPressed(':');
                            } else if (k == "drcnt") {
                                m_App->setValue(VALUE_DRUM_COUNT, ofToInt(v), me->m_Id);
                            } else if (k == "drtype") {
                                m_App->setValue(VALUE_DRUM_TYPE, ofToInt(v), me->m_Id);
                            } else if (k == "echo") {
                                m_App->setValue(VALUE_ECHO, ofToInt(v), me->m_Id);
                            } else if (k == "dd") {
                                m_App->setValue(VALUE_DYNAMIC, ofToFloat(v), me->m_Id);
                            } else if (k == "dest") {
                                m_App->setStrValue(VALUE_DEST, v, me->m_Id);
                            } else if (k == "chord") {
                                m_App->setStrValue(VALUE_CHORD, v, me->m_Id);
                            } else if (k == "offset") {
                                m_App->setValue(VALUE_TRANSPOSE, ofToInt(v), me->m_Id);
                            } else if (k == "positionx") {
                                m_App->setValue(VALUE_POSITION_X, ofToInt(v), me->m_Id);
                            } else if (k == "quality") {
                                m_App->setValue(VALUE_QUALITY, ofToInt(v), me->m_Id);
                            } else if (k == "legato") {
                                m_App->setValue(VALUE_DURATION, ofToFloat(v), me->m_Id);
                            } else if (k == "cc") {
                                me->ccFlags = ofToInt(v);
                            } else if (k == "playing") {
                                int p = ofToInt(v);
                                if (p & MPLAY) m_App->keyPressed('p');
                            } else {
                                cout << "ignored " << k << " : " << v << endl;
                            }
                        }
                    }
                }
                else if (sep2 != string::npos)
                {
                    int vol=0, instr=-1, drType = 0, drCnt = 0;
                    string pars = key.substr(sep2 + 1, -1);
                    int n = sscanf(pars.c_str(), "%i|%i|%i|%i", &vol, &instr, &drType, &drCnt);
                    if (n)
                    {
                        me->m_Ch0 = instr;
                        me->setVolume(vol);
                        if (n > 2)
                        {
                            for (int i=0; i<drType; i++)
                            {
                                m_App->keyPressed('.');
                            }
                            for (int i=0; i<drCnt; i++)
                            {
                                m_App->keyPressed(360); // pgUp
                            }
                        }
                        else if (m_autoplay) // drum melodies are not started automatically
                        {
                            m_App->keyPressed('p');
                        }
                    }
                }

                // done with this melody, read next line
                break;
            }
        }
    }

    m_App->select(0);
    stf.close();
    return m_restore;
}
