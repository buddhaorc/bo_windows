#ifndef SCALE_H
#define SCALE_H

#include "ofMain.h"

// FIXME
#ifndef NOTE_NUM
#define NOTE_NUM 12
#endif

class Scale
{
    static int m_NumNotes;
    static double *m_Ratios;
    static string m_ScaleFile;

public:

    static bool loadScale(const char *fname);
    static void mapFreqTo(float freq, int &midiNote, float &bend);
    static void midiNoteToScaled(int note, int &midiNote, float &bend);
    static bool enabled() { return m_NumNotes != 0; }
    static int getNumNotes() { return m_NumNotes ? m_NumNotes : NOTE_NUM; }
    static bool hasTwelveNotes() { return getNumNotes() == NOTE_NUM; }
    static string getFileName() { return m_ScaleFile; }
};


#endif // SCALE_H
