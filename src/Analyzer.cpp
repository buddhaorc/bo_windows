#include <assert.h>
#include <strstream>

#include "ofxIniSettings.h"

#include "Analyzer.h"

int Analyzer::transposes[MAX_GROUP_SZ];
int Analyzer::lowest = 0;
int Analyzer::highest = 127;
// durations
int Analyzer::maxnote = 5000;
int Analyzer::maxpause = 8000;

std::vector<NoteSplitter*> Analyzer::splitters;

ofxCvContourFinder Analyzer::cf;

Analyzer::Analyzer()
{
    //ctor
}

Analyzer::~Analyzer()
{
    //dtor
    list<Melody*>::iterator mit = m_Music.begin();
    for ( ; mit != m_Music.end(); mit++)
    {
        Melody *m = *mit;
        delete m;
    }

}

void Analyzer::init(ofxIniSettings &ini)
{
    string trans = ini.get(string("transpose"), string("-4,0,4"));
    istrstream ss(trans.c_str());
    char sep;
    int n, idx = 0;
    while((idx < MAX_GROUP_SZ) && (ss >> n))
    {
        transposes[idx++] = n;
        ss >> sep;
    }
    memset(transposes, 0, sizeof(transposes));
    lowest = ini.get("lowest", 0);
    highest = ini.get("highest", 127);
    maxnote = ini.get("maxNoteDuration", 5000);
    maxpause = ini.get("maxPauseDuration", 8000);

    minShapeWd = ini.get("minShapeWidth", 5);
    minShapeHt = ini.get("minShapeHeight", 5);

    string splits = ini.get("splits", string("4,7:1.0"));
    // 0,4,7:0.8;0,1,2,3,4:0.2
    vector<string> sps = ofSplitString(splits, ";", true, true);
    for (int i=0; i < sps.size(); i++)
    {
        NoteSplitter *ns = new NoteSplitter(sps[i]);
        if (ns->getWeight())
        {
            splitters.push_back(ns);
        }
        else
        {
            delete ns;
        }
    }

    if (!splitters.size())
    {
        splitters.push_back(new NoteSplitter("0,0,4"));
    }
}

NoteSplitter::NoteSplitter(string s)
{
    vector<string> ss = ofSplitString(s, ":", true, true);
    if (ss.size() != 2)
    {
        cout << "invalid splitter string " << s << endl;
        weight = 0;
        return;
    }
    //0,4,7:0.8
    vector<string> sss = ofSplitString(ss[0], ",", true, true);
    for (int i = 0; i < sss.size(); i++)
    {
        notes.push_back(ofToInt(sss[i]));
    }
    weight = sss.size() ? ofToFloat(ss[1]) : 0;

}

std::list<int> &Analyzer::getNoteSplit()
{
    while(true)
    {
        int idx = ofRandom(splitters.size());
        NoteSplitter *ns = splitters[idx];
        if (ns->getWeight() > ofRandom(1.0))
        {
            return ns->getSplit();
        }
    }
}

int Analyzer::analyze(ofImage &src)
{
    int total = 0;
    for (list<Melody*>::iterator it = m_Music.begin(); it != m_Music.end(); it++)
    {
        Melody *m = *it;
        delete m;
    }

    m_Music.clear();
    wd = src.width;
    ht = src.height;

    ofxCvColorImage	colorImg;
    colorImg.allocate(wd, ht);
    colorImg.setFromPixels(src.getPixels(), wd, ht);

    ofxCvGrayscaleImage 	grayImage;
    grayImage.allocate(wd, ht);

    int id = 0;
    for (int p = 0; p < NUM_PLANES; ++p)
    {
        grayImage = colorImg;
        grayImage.contrastStretch();
        grayImage.threshold(255 / (NUM_PLANES + 1) * (p + 1), false);
        cf.findContours(grayImage, 10, wd * ht  / 2, MAX_LOOPS, true);
        for (int j=0; j<cf.blobs.size(); j++)
        {
            if (cf.blobs[j].nPts > 3 && (cf.blobs[j].boundingRect.width > minShapeWd || cf.blobs[j].boundingRect.height > minShapeHt))
            {
                ofPoint c = cf.blobs[j].centroid;
                if (addMelody(j, id++, transposes[p], src.getColor(c.x, c.y))) {
                    total++;
                }
            }
        }
    }

    m_NextId = id;
    cf.blobs.clear();

    return total;
}

Melody *Analyzer::addMelody(int blobId, int id, int trans, ofColor col)
{
    ofxCvBlob &blob = cf.blobs[blobId];
    Melody *m = new Melody(trans, id);
    for( int i = 0; i < blob.nPts ; i++ ) {
        m->addPoint(new ofPoint(blob.pts[i]));
    }

    m->boundingRect = blob.boundingRect;
    m->setCenter(blob.centroid, col);
    if (m->maxDist)
    {
        m_Music.push_back(m);
        return m;
    }
    else
    {
        delete m;
        return 0;
    }
}

void Analyzer::generate(NoteDuration *nd)
{
    list<Melody*>::iterator it = m_Music.begin();
    for (; it != m_Music.end(); it++)
    {
        Melody *m = *it;
        m->generate(wd, ht, 0, nd);
    }
}

// pattern-generated melodies are not affected
void Analyzer::addPauses()
{
    // remove all pauses
    list<Melody*>::iterator it = m_Music.begin();
    for (; it != m_Music.end(); it++)
    {
        Melody *m = *it;
        if ((m->getPlayingState() & MPLAY) == 0 || m->m_Ptrn) continue;
        m->removePauses();
    }
    // find the longest melody
    int longest = 0;
    for (it = m_Music.begin(); it != m_Music.end(); it++)
    {
        Melody *m = *it;
        if ((m->getPlayingState() & MPLAY) == 0) continue;
        int d = m->getDuration();
        if ( d > longest ) longest = d;
    }

    for (it = m_Music.begin(); it != m_Music.end(); it++)
    {
        Melody *m = *it;
        if ((m->getPlayingState() & MPLAY) == 0 || m->m_Ptrn) continue;
        m->addPauses(longest);
        m->storeKey('U');
    }
}
