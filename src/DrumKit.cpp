#include "DrumKit.h"
#include "Melody.h"
#include "Envelope.h"

using namespace std;

int DrumKit::drumTypes[MAX_DRUM_TYPES];
int DrumKit::numDrumTypes = 0;
int DrumKit::drumMelos[MAX_DRUM_TYPES];
int DrumKit::numDrumMelos = 0;

int DrumKit::getDrumType(int idx)
{
    return numDrumTypes ? drumTypes[idx % numDrumTypes] : 0;
}

DrumKit::DrumKit()
{
    //ctor
    m_ptr = 0;
}

DrumKit::~DrumKit()
{
    //dtor
}

void DrumKit::initDrums(std::string &dtypes)
{
    initTypes(dtypes, drumTypes, numDrumTypes);
}

void DrumKit::initMelos(std::string &dtypes)
{
    initTypes(dtypes, drumMelos, numDrumMelos);
}

void DrumKit::initTypes(string &dtypes, int *types, int &num)
{
    num = 0;
    char *tmp = new char[dtypes.length() + 1];
    strcpy(tmp, dtypes.c_str());

    // assign drum types
    for (char * t = strtok(tmp,", "); t; t = strtok(0, ", "))
    {
        int dt = t ? atoi(t) : 1;
        types[num++] = dt;
        cout << dt << " ";
    }
    delete [] tmp;
    cout << "[" << num << "] " << dtypes << endl;
}

void DrumKit::drum(DrumEvent &de, Envelope * env, float period, float dt, float dur)
{
    list<Melody*>::iterator mit = de.melodies.begin();
    for ( ; mit != de.melodies.end(); mit++)
    {
        Melody *m = *mit;
        int ch = m->getCh();
        int nmidi = ch < 0 ? 0 : ch >> 8;
        float v = env->getValue(period, m->getBeat()) * 127;
        // TODO how to access colour?
        m->m_pDrum->sendDrum(m, nmidi, v + m->getVolume(), dt, dur);
    }
}

void DrumKit::tick(Envelope *env, float dt, float period)
{
    float relt = env->getRelativeTime(period);
    int ptr = relt * NUM_BEATS;
    while (ptr != m_ptr)
    {
        if (++m_ptr == NUM_BEATS) m_ptr = 0;
        mtx.lock();
        drum(timeline[m_ptr], env, period, dt, period * 0.1);
        mtx.unlock();
    }
}

void DrumInfo::changeMaxHit(int incr)
{
    // initial value is assigned when -/+ is used for the first time
    if (!maxHit) maxHit = MAX_HIT / 2;

    maxHit += incr;
    if (maxHit < 1) maxHit = 1;
    if (maxHit >= MAX_HIT) maxHit = MAX_HIT;
    cout << maxHit << " of " << MAX_HIT << endl;
}

bool DrumInfo::getDrumHit()
{
    bool yes = !maxHit || (hit++ < maxHit);
    hit %= MAX_HIT;
    return yes;
}

void DrumKit::plusDrum(Melody *m)
{
    mustHaveDrum(m);
    m->m_pDrum->changeMaxHit(1);
}

void DrumKit::minusDrum(Melody *m)
{
    mustHaveDrum(m);
    m->m_pDrum->changeMaxHit(-1);
}

void DrumKit::incMelody(Melody *m)
{
    mustHaveDrum(m);
    int cnt = m->m_pDrum->getDrumCount();
    // remove existing drums of this melody
    if (cnt) remove(m);
    m->m_pDrum->setDrumCount( ++cnt );
    insert(m);
}

void DrumKit::assignDrum(Melody *m)
{
    mustHaveDrum(m);
    // remove existing drums of this melody
    remove(m);
    if (m->m_pDrum->getDrumCount())
    {
        insert(m);
    }
}

void DrumKit::decMelody(Melody *m)
{
    if (!m->m_pDrum) return;

    int cnt = m->m_pDrum->getDrumCount();
    if (cnt)
    {
        remove(m);
        m->m_pDrum->setDrumCount( --cnt );
        if (cnt) insert(m);
    }
}

void DrumKit::updateMelody(Melody *m)
{
    if (!m->m_pDrum) return;

    int cnt = m->m_pDrum->getDrumCount();
    if (cnt)
    {
        remove(m);
        insert(m);
    }
}

void DrumKit::mustHaveDrum(Melody *m)
{
    if (!m->m_pDrum)
    {
        m->m_pDrum = new DrumInfo();
        m->m_pDrum->centroid.x = (float)m->getCenter().x / m->getImgWidth();
        m->m_pDrum->centroid.y = (float)m->getCenter().y / m->getImgHeight();
    }
}

void DrumKit::changeDrum(Melody *m)
{
    mustHaveDrum(m);
    int t = m->m_pDrum->getDrumType();
    if (++t >= numDrumTypes)
    {
        t = 0;
    }
    m->m_pDrum->setDrumType(t);
}

void DrumKit::remove(Melody *m)
{
    for (int i=0; i<NUM_BEATS; i++)
    {
        timeline[i].remove(m);
    }
}

void DrumKit::insert(Melody *m)
{
    int idx = m->getCenter().x / m->getImgWidth() * NUM_BEATS;
    int cnt = m->m_pDrum->getDrumCount();
    if (cnt)
    {
        if ((m->getCh() % 16) != 10)
        {
            cnt = 1; // for regular instruments, the note duration will be increased
        }
        int gap = NUM_BEATS / cnt;
        if (!gap) gap = 1;
//        if (gap > 5) gap = 5;
        for (int i=0; i<cnt; i++)
        {
            int j = (i * gap + idx) % NUM_BEATS;
            timeline[j].melodies.push_back(m);
        }
    }
}

void DrumEvent::remove(Melody *m)
{
    melodies.remove(m);
}

float DrumInfo::getVolumeAdjustment()
{
    ++ptr;
    if (ptr >= drumCnt || ptr >= DRUM_SZ) {
        ptr = 0;
    }
    if ((ofRandom(100) > 90) || (vol[ptr] == 0)) {
        vol[ptr] = ofRandom(5);
    }
    return vol[ptr];
}

void DrumInfo::sendDrum(Melody *m, int nmidi, float v, float dt, float dur)
{
    if (!getDrumHit()) return;

    v -= getVolumeAdjustment();

    int dtype = getDrumType();
    dtype = DrumKit::drumTypes[dtype % DrumKit::numDrumTypes];

//    cout << "sendDrum " << dtype << " v=" << v << endl;
    if (v > 127) v = 127;
    if (v < 0) v = 0;

    if ((m->getCh() % 16) != 10)
    {
        dur *= m->m_pDrum->getDrumCount();
    }
    m->sendDrum(nmidi, dtype, v, dt, dur);

    drumBit = true;
}

void DrumKit::clear()
{
    mtx.lock();
    for (int i = 0; i < NUM_BEATS; i++)
    {
        timeline[i].melodies.clear();
    }
    mtx.unlock();
}
