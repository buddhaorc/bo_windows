#ifndef MELODY_H
#define MELODY_H

#include "ofMain.h"
#include <list>
#include <string>
#include "ofxMidi.h"
#include "ofxOsc.h"
#include "ofxIniSettings.h"
#include "SerialSender.h"
#include "Conditions.h"
#include "NotePlayer.h"

// Max number of rhythm patterns
#define MAXPTRN 50
// The shortest note 1/32
#define SHORTEST 32

// Melodies might be grouped in the controller.
// The expected size of a group is 3, for R,G, and B components
#define MAX_GROUP_SZ 10

class NoteDuration;
class DrumInfo;

struct PtrnItem
{
    int vol;
    int dur;
    int cnt;
};

class Ptrn
{
        std::list<PtrnItem*> ptrn;
        std::list<PtrnItem*>::iterator now;
        int   rndVol;
        int   rndDur;
        char  title[100];
    public:
        Ptrn(string p);
        virtual ~Ptrn();
        PtrnItem *getNext();
        char *getTitle() { return title; };
};

class ofxIniSettings;

class PatternSet
{
        Ptrn *ptrns[MAXPTRN];
        int m_Cnt;
    public:
        PatternSet();
        void init(ofxIniSettings &ini);
        virtual ~PatternSet();

        int getNumber() { return m_Cnt; }
        Ptrn *getPtrn(int idx) { return ptrns[idx]; }
};

struct Note
{
    int note0;
    int note;
    int velo;
    int synlev;
    int dur;
    ofPoint pt;
    int playing;

    Note(ofPoint p, int n, int v, int syn, int d);
    void write(ostream &os);
    bool read(istream &os);
    float distance(ofPoint p);

    static char *noteString(int n);
};

#define MPLAY 1
#define MDRUM 2
#define MINST 4

class Melody
{
    public:
        Melody(int trans, int id) : duration(0), paused(true),
            full(false), drums(false),
            effect(false), vibrato(false), portamento(false),
            edge(false), inserted(false),
            quality(50), m_Midi(0), m_Ch0(-1), m_Bank(0), display(0), accord(0),
            ptrnIdx(0), m_Ptrn(0), reverse(false), m_Osc(0), m_Serial(0), m_pDrum(0),
            volume(0), m_Dest(MIDI), m_legato(1.0f), ccFlags(0),
            m_Id(id), m_ParentId(-1), m_HasChild(false), m_echo(0), m_dynamic(0.5), m_Beat(0) {}
        virtual ~Melody();

        void setMoutOscSerial(MidiOut *mout, OscSender *osc, SerialSender *ss, AudioSampler *sam, SimpleSynth *synth)
        {
            m_Midi = mout; m_Osc = osc; m_Serial = ss; m_Sampler = sam; m_Synth = synth;
        }
        void addPoint(ofPoint *p) { points.push_back(p); }
        void setCenter(ofPoint &c, ofColor col);
        ofPoint getCenter() { return centroid; }

        void moveTo(ofPoint &newpos);
        void moveBack() { moveTo(centroid0); }
        void scale(float sx, float sy);

        // return false if a note has not been played.
        // Another Melody can be invoked.
        bool playNext(int ch, float vol, int tick, int sync);
        void sendDrum(int nmidi, int dtype, float v, float dt, float dur);

        void sendControl(int ch, int tp, int val);

        // true if moved to the next note
        bool advance(int &n, int &v, int &d, int synch);
        void midiThrough(int ch, MidiStatus cmd, int b1, int b2);

        Note *current() { return *playing; }
        int getCurrentIdx();
        void setCurrentIdx(int idx);
        // peek next note of the melody
        Note *getNextNote();

        void draw(float ox, float oy, float sx, float sy, bool line, bool fill, ofColor c);
        void write(ostream &os);
        bool read(istream &os);
        int getDuration() { return duration; }
        string getCCLabel(int kind);

        // sets the current note to the position
        // closest to the given point
        void goTo(float x, float y);

        // ADSR parameters
        void sendADSR(int par, int val);
        void sendADSR(int amp, int a, int d, int s, int r, int e);

        // drumming
        DrumInfo *m_pDrum;
        bool isDrumming();
        int getPlayingState();
        bool getCCflag(int fl) { return (ccFlags & fl) != 0; }

        // volume adjustment
        int getVolume() { return volume; }
        void setVolume(int v) { volume = v; }

        int getBeat() { return m_Beat; }

        void setDynamic(float d);
        float getDynamic() { return m_dynamic; }

        int getImgWidth() { return m_ImgWidth; }
        int getImgHeight() { return m_ImgHeight; }
        ofColor getColour() { return m_Colour; }

        void generate(int w, int h, Ptrn *ptrn, NoteDuration *nd);
        Melody *copy(int id, int trans);

        bool control(int k);
        // applies rulez to make the note sound better
        int harmonize(int n);
        int dominant(int n);
        void limitDurations();

        const char* info(float dur);
        const char* drumInfo();
        const char* instrInfo();
        int getCh() { return m_Ch0; }
        void setCh(int c) { m_Ch0 = c; }
        bool isDestOsc() { return m_Dest == OSC || m_Dest == BOTH; }
        static string getDestName(int i);
        string getDestName();
        void setDest(string nm);

        static string getChordName(int i);
        string getChordName();
        void setChord(string nm);

        int getEcho() { return m_echo; }
        void setEcho(int e) { m_echo = e; }

        float getLegato() { return m_legato; }
        void setLegato(float e) { m_legato = e; }

        bool getReverse() { return reverse; }
        void setReverse(bool r) { reverse = r; }

        void storeKey(int k);
        static bool historyOn;

        const std::list<ofPoint*> &getPoints() { return points; }

        int m_Id;
        int m_ParentId;
        bool m_HasChild;

        // public because we want to show it on screen
        int quality; // probability of using "pure" notes of [0,4,7] set
        int display;

        enum GenType { ALL_POINTS, VERT_LINES, GRAVITY };
        static GenType gtype;

        static int echoDelay;
        static int echoVelocity;
        static int echoPitch;

        static bool multiPause;
        static int pauseDuration;

        static float arpeggioDelay;

    protected:
        std::list<ofPoint*> points;
        std::list<Note*>::iterator playing;
        std::list<Note*> melody;
        // last played notes
        std::list<int> played;
        void addPlayed(int n);

        void generateAllPoints(int w, int h);
        void generateVert(int w, int h);
        void gravityDurations();
        void split();
        void merge();
        void extreme();
        void reduce();
        void removePauses();
        void addPauses();
        void addPauses(int longest);
        void clear();
        void countDurations();
        // events are sent when the melody starts or stops
        void startTrackOsc();
        void stopTrackOsc();

        void stop(Note *n = 0);
        string writeMidi();

        void toggleCCflag(int fl);
        void ccFlagsInfo(char *buf);
    private:
        int m_Ch0; // the assigned midi channel
        int m_Bank;// selected MIDI bank
        int m_Beat; // off-beat
        bool edge; // the route touches edge of the image
        bool paused;
        bool inserted; // in the short list
        bool full; // max volume
        bool drums; // plays in midi channel 10
        bool effect; // MidiInstruments::m_Effect
        bool vibrato;
        bool portamento;
        int accord; // 0 - off, 1 - up, 2 - down, 3 - updow, 4 - random, 5 - history
        int duration; // total duration of all notes
        bool reverse;
        int volume; // adjustment -127..+127
        float m_legato;
        string keystory;
        int ccFlags;
        static string storedKeys;

        ofRectangle     boundingRect;
        ofPoint         centroid0;
        ofPoint         centroid;
        ofColor         m_Colour;
        float           maxDist;
        float           minDist;
        int             m_ImgWidth;
        int             m_ImgHeight;

        enum { MIDI, AUDIO, SYNTH, OSC, BOTH, SERIAL } m_Dest;

        SenderSet ss;
        SenderSet *getSenderSet();

        int m_echo; // number of echoes
        float m_dynamic;

        int  ptrnIdx;
        Ptrn *m_Ptrn;

        MidiOut *m_Midi;
        OscSender *m_Osc;
        SerialSender *m_Serial;
        AudioSampler *m_Sampler;
        SimpleSynth *m_Synth;

    friend class Analyzer;
    friend class Controller;
    friend class Harmonizer;
    friend class NoteLine;
    friend class NoteDuration;
};

struct Param
{
    std::string key;
    std::string val;
};

class Evolver
{
    public:
        Evolver(std::list<Param>);
        void evolve(Melody &src, Melody &dst);
};

#define MAXTOKEN 100

class MeloParser
{
    public:
        // possible parsing results:
        static const int OK;
        static const int EOM;
        static const int ERR;
        static const int CLOSE;
        static int nextKey(istream &is, char *buf);
        static int nextValue(istream &is, char *buf);
        static int nextInt(istream &is, const char *key, int &val);
        static int nextFloat(istream &is, const char *key, float &val);
        static int nextBool(istream &is, const char *key, bool &val);
};
#endif // MELODY_H
