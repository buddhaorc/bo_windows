#include "SerialSender.h"
#include "AllThreads.h"
#include "Conditions.h"

SerialSender::SerialSender() : m_byte(0), active(false)
{
    memset(m_bits, 0, sizeof(m_bits));
}

SerialSender::~SerialSender()
{
    serial.close();
}

void SerialSender::init(ofxIniSettings &ini)
{
	int baud = ini.get("rate", 57600);
	string comport = ini.get("com", string(""));
	if (comport != "") {
        //active = serial.setup(("\\\\.\\" + comport).c_str(), baud);
        active = serial.setup(comport.c_str(), baud);
        if (!active) {
            serial.enumerateDevices();
        }
	}
    string sb = ini.get("serialBits", string(""));
    int idx = 0;
    while (!sb.empty())
    {
        string c;
        int sep = sb.find(',');
        if (sep != string::npos) {
            c = sb.substr(0, sep);
            sb = sb.substr(c.length() + 1);
        } else {
            c = sb;
            sb = "";
        }
        int v;
        if (sscanf(c.c_str(), "%d", &v) == 1) {
            m_bits[idx++] = v;
        } else {
            cout << "ignored " << c << endl;
        }
    }

    hasBits = idx > 0;

    if (active)
    {
        AllThreads::addThread(this);
        startThread(true, false);
        cout << "active " << comport << " has bits " << hasBits << endl;
    }
}

void SerialSender::threadedFunction()
{
    while (isThreadRunning())
    {
        if (serial.available())
        {
            Conditions::handle(serial.readByte());
        }
        ofSleepMillis(50);
    }
    AllThreads::removeThread(this);
}

void SerialSender::sendOn(int n, int d, int v)
{
    unsigned char b = m_bits[n % NOTE_NUM];
    m_byte |= b;
    if (active && hasBits) {
        serial.writeByte(m_byte);
        cout << "ON: " << status() << endl;
    }
}

void SerialSender::sendOff(int n)
{
    unsigned char b = ~m_bits[n % NOTE_NUM];
    m_byte &= b;
    if (active && hasBits) {
        serial.writeByte(m_byte);
    }
//    cout << "OFF: " << status() << endl;
}

// ch = 0..3, val = 0..63
void SerialSender::sendControl(int ch, int val)
{
    unsigned char bt = ((ch & 3) << 6) | (val & 0x3f);
    if (active && !hasBits) {
    //    serial.writeByte(bt);
    } else {
        cout << ofToBinary((int) bt) << " ";
    }
}

string SerialSender::status()
{
    return ofToHex(m_byte);
}
