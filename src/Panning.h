#ifndef PANNING_H
#define PANNING_H

#include <ofMain.h>
#include "ofxIniSettings.h"

class OscSender;
class Panning
{
    public:
        Panning();
        virtual ~Panning();
        void init(ofxIniSettings &ini, OscSender *o);

        void setTargets(float cx, float cy, float cz)  { tx = cx; ty = cy; tz = cz; resetCurrent(); }
        void setTargets(float cx, float cy) { tx = cx; ty = cy; resetCurrent(); }
        void setTargetX(float cx) { tx = cx; resetCurrent(); }
        void setTargetY(float cy) { ty = cy; resetCurrent(); }
        void setTargetZ(float cz) { tz = cz; resetCurrent(); }
        void resetCurrent() { ox = x; oy = y; oz = z; started = ofGetElapsedTimeMillis(); }

        void getOffsets(float &cx, float &cy) { cx = x; cy = y; }
        float getOffsetZ() { return z; }
        void setOffsets(float cx, float cy, float cz);
        void setOffsets(float cx, float cy);
        void setOffsetZ(float cz);

        float getScale() { return scl; }
        void setScale(float s) { scl = s; }
        bool getEnabled() { return enabled; }
        void setEnabled(bool e) { enabled = e; }
        void update();
        void draw(ofRectangle &rect, ofImage &img);
        bool handle(int key);

        // pan within -1..1
        float getPan(float x, float y);
        // 1/r^2
        float getAttenuation(float x, float y);
        // with given speed of sound
        float getDelay(float x, float y);

        // send state to osc
        void sendOscState();

    protected:
    private:
        // x, y positions are within 0..1 of width and height
        // z position
        // current position
        float x, y, z;
        // target position
        float tx, ty, tz;
        // origin position
        float ox, oy, oz;

        // only used for display
        float scl;
        // speed of sound
        float speed;

        long transition; // transition time, msec
        long started; // target set at this time

        bool enabled;
        bool camera;

        ofVideoGrabber vidGrabber;
        ofImage buf;

        OscSender *osc;
};

#endif // PANNING_H
