#ifndef ENVELOPE_H
#define ENVELOPE_H

#include "ofxMidi.h"
#include "ofxIniSettings.h"

// nano kontrol sliders to control the envelope
#define NSLIDERS 8

class Envelope
{
    public:
        Envelope();
        virtual ~Envelope();
        void init(ofxIniSettings &ini);
        float getValue(long period, int beat);
        bool handle(ofxMidiMessage& e);
        void setSliderValue(int ch, int v);
        int getSliderValue(int ch);
        int getSlider(int ch);
        void draw(float x, float y, float w, float h, int qua);
        void draw(float x, float y, float w, float h);
        void reset() { t0 = ofGetElapsedTimeMillis(); }
        float getRelativeTime(int period);
        bool edit;
    protected:
    private:
        unsigned long t0;
        float minVol;
        float maxVol;
        ofMutex mtx;
        float getVolumeInternal(float reltime);

        void setRange(float mn, float mx) { minVol = mn / 127.0f; maxVol = mx / 127.0f; }
        void setSliders(const char *chstr);
        void setEnvelope(const char *chstr);

        int sliders[NSLIDERS + 1];
        int channels[NSLIDERS];
};

#endif // ENVELOPE_H
