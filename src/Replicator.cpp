#include "Replicator.h"
#include "testApp.h"
#include "AllThreads.h"
#include "Poco/Net/NetworkInterface.h"
#include "Poco/Net/DNS.h"

using Poco::Net::IPAddress;
using Poco::Net::NetworkInterface;
using Poco::Net::HostEntry;
using Poco::Net::DNS;

static string myIp;

Replicator::Replicator()
{
    //ctor
}

Replicator::~Replicator()
{
    //dtor
}

void Replicator::init(testApp *a, ofxIniSettings &ini)
{
    ta = a;

	oscPort = ini.get("oscListen", 0);
    if (oscPort) {
        cout << "Listening OSC at " << oscPort << endl;
        receiver.setup(oscPort);
    }

    if (ini.get("discovery", false))
    {
        vector<string> ips;
        NetworkInterface::NetworkInterfaceList ifcs = NetworkInterface::list();
        for (int i=0; i<ifcs.size(); ++i)
        {
            NetworkInterface ni = ifcs[i];
            IPAddress ip = ni.address();
            cout << ip.toString() << " " << ni.displayName() << endl;
            ips.push_back(ip.toString());
        }

        string repstr = ini.get("remote", string(""));
        vector<string> reps = ofSplitString(repstr, ";", true, true);
        for (int i=0; i<reps.size(); ++i) {
            string dest = reps[i];
            OscTarget *target = new OscTarget();
            int sep = dest.find(':');
            if (sep != string::npos)
            {
                string hostname = dest.substr(0, sep);
                string p = dest.substr(sep + 1);
                int port;
                if (sscanf(p.c_str(), "%d", &port) == 1)
                {
                    if (ofContains(ips, hostname) && port == oscPort)
                    {
                        cout << "excluded " << hostname << ":" << port << endl;
                        continue;
                    }
                    target->sender->setup(hostname, port);
                    senders[hostname + ":" + ofToString(port)] = target;
                }
                else
                {
                    cout << "failed sscanf " << p << endl;
                }
            }
            else
            {
                if (ofContains(ips, dest))
                {
                    cout << "excluded " << dest << endl;
                    continue;
                }
                target->sender->setup(dest, oscPort);
                senders[dest + ":" + ofToString(oscPort)] = target;
            }
        }

        discoveryInterval = ini.get("discoveryInterval", 10000);
        discoverer.setup(broadcast(getThisIp()), oscPort);
        AllThreads::addThread(this);
        startThread(true, false);
    }
    else
    {
        discoveryInterval = INT_MAX;
    }

    maxAge = ini.get("maxAge", MAX_BO_AGE);
    // we cannot be too strict
    if (maxAge < discoveryInterval * 2) maxAge = discoveryInterval * 2;
}

void Replicator::key(int k)
{
    ofxOscMessage m;
    m.setAddress( "/bo/key" );
    m.addIntArg( k );
    sendToAll(m);
}

void Replicator::key(int k, int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/key" );
    m.addIntArg( k );
    m.addIntArg( mel );
    sendToAll(m);
}

void Replicator::play(int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/melody/play" );
    m.addIntArg( mel );
    sendToAll( m );
}

void Replicator::pause(int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/melody/pause" );
    m.addIntArg( mel );
    sendToAll( m );
}

void Replicator::select(int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/select" );
    m.addIntArg( mel );
    sendToAll(m);
}

void Replicator::value(int k, float v, int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/value" );
    m.addIntArg( k );
    m.addIntArg( mel );
    m.addFloatArg( v );
    sendToAll(m);
//    cout << "value " << k << " " << v << " mel " << mel << endl;
}

void Replicator::svalue(int k, string s, int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/svalue" );
    m.addIntArg( k );
    m.addIntArg( mel );
    m.addStringArg( s );
    sendToAll(m);
}

void Replicator::goTo(float x, float y)
{
    ofxOscMessage m;
    m.setAddress( "/bo/goto" );
    m.addFloatArg( x );
    m.addFloatArg( y );
    sendToAll(m);
}

void Replicator::midiControl(int control, int value)
{
    ofxOscMessage m;
    m.setAddress( "/bo/midi/control" );
    m.addIntArg( control );
    m.addIntArg( value );
    sendToAll(m);
}

void Replicator::midiNote(bool on, int ch, int pitch, int velo)
{
    ofxOscMessage m;
    m.setAddress( "/bo/midi/note" );
    m.addIntArg( on ? 1 : 0 );
    m.addIntArg( ch );
    m.addIntArg( pitch );
    m.addIntArg( velo );
    sendToAll(m);
}

void Replicator::image(string img)
{
    ofxOscMessage m;
    m.setAddress( "/bo/image" );
    m.addStringArg( img );
    sendToAll(m);
}

void Replicator::scale(string scl)
{
    ofxOscMessage m;
    m.setAddress( "/bo/scale" );
    m.addStringArg( scl );
    sendToAll(m);
}

void Replicator::sendToAll(ofxOscMessage &m)
{
    long oldest = ofGetElapsedTimeMillis() - maxAge;
    lock();
    for (map<string, OscTarget*>::iterator sit = senders.begin(); sit != senders.end(); sit++)
    {
        if (sit->second->lastSeen > oldest)
        {
            ofxOscSender *s = sit->second->sender;
            s->sendMessage( m );
        }
    }
    unlock();
}

// handles all incoming messages
void Replicator::invoke()
{
    string myAddr = myAddress();
    // check for waiting messages
    while(receiver.hasWaitingMessages()){
        ofxOscMessage m;
        receiver.getNextMessage(&m);
        // cout << m.getAddress() << endl;
        if (m.getAddress() == "/bo/discovery")
        {
            // this is a discovery requst
            string from = m.getArgAsString(0);
            if (from != myAddr)
            {
                ta->info.publish("discovery from " + from, ofColor::fromHex(0x222222));
                ofxOscMessage resp;
                resp.setAddress("/bo/bo");
                resp.addStringArg(myAddr);
                // send response
                ofxOscSender *sender = updateSenders(from, false, false);
                sender->sendMessage(resp);
            }
        }
        else if (m.getAddress() == "/bo/bo")
        {
            // this is a discovery response
            ta->info.publish("bo " + m.getArgAsString(0), ofColor::fromHex(0x444444));
            updateSenders(m.getArgAsString(0), true, false);
        }
        else if (m.getAddress() == "/bo/boc")
        {
            // this is a discovery response
            ta->info.publish("control " + m.getArgAsString(0), ofColor::fromHex(0x444444));
            updateSenders(m.getArgAsString(0), true, true);
        }
        else
        {
            ta->newOscMessage(m);
        }
    }
}

ofxOscSender* Replicator::updateSenders(string addr, bool discovered, bool control)
{
    ofxOscSender* snd = 0;
    lock();
    map<string, OscTarget*>::iterator it = senders.find(addr);
    OscTarget *ot = 0;
    if (it == senders.end())
    {
        // not found
        ot = new OscTarget();
        ot->lastSeen = ofGetElapsedTimeMillis();
        vector<string> aa = ofSplitString(addr, ":", true, true);
        if (aa.size() == 2)
        {
            ot->sender->setup(aa[0], ofToInt(aa[1]));
        }
        else
        {
            ot->sender->setup(addr, oscPort);
        }
        senders[addr] = ot;
        snd = ot->sender;
    }
    else
    {
        ot = it->second;
        ot->lastSeen = ofGetElapsedTimeMillis();
        snd = ot->sender;
    }

    if (discovered)
    {
        ot->discovered = true;
        ot->control = control;
    }

    unlock();
    return snd;
}

string Replicator::getThisIp()
{
    HostEntry he = DNS::thisHost();
    IPAddress ad = DNS::resolveOne(he.name());
    return ad.toString();
}

string Replicator::broadcast(string ip)
{
    size_t dot = ip.rfind('.');
    if (dot == string::npos) {
        cout << "possible broadcast problem: " << ip << endl;
        return ip;
    }
    string s = ip.substr(0, dot + 1) + "255";
//    cout << "broadcast " << s << endl;
    return s;
}

void Replicator::threadedFunction() {
    while (isThreadRunning())
    {
        ofxOscMessage m;
        m.setAddress("/bo/discovery");
        m.addStringArg(myAddress());
        discoverer.sendMessage(m);
        for (int w = discoveryInterval; w > 0 && isThreadRunning(); )
        {
            int wt = w > 500 ? 500 : w;
            w -= wt;
            ofSleepMillis(wt);
        }
    }
    AllThreads::removeThread(this);
}

vector<string> Replicator::getIfList()
{
    vector<string> ips;
    NetworkInterface::NetworkInterfaceList ifcs = NetworkInterface::list();
    for (int i=0; i<ifcs.size(); ++i)
    {
        NetworkInterface ni = ifcs[i];
        IPAddress ip = ni.address();
        ips.push_back(ip.toString() + " " + ni.displayName());
    }
    return ips;
}

vector<string> Replicator::getSenders(bool all)
{
    long now = ofGetElapsedTimeMillis();
    long oldest = now - maxAge;
    if (oldest < 1) oldest = 1;

    vector<string> ss;
    vector<string> olds;
    bool discovered = false;
    lock();
    for (map<string, OscTarget*>::iterator sit = senders.begin(); sit != senders.end(); sit++)
    {
        OscTarget * t = sit->second;
        long age = (now - t->lastSeen) / 1000;
        if (t->discovered) discovered = true;
        string state = t->discovered ? (t->control ? "BO Control" : "BO") : "?";
        // the list of active nodes only shows BO Controls, not BO
        if (t->lastSeen > oldest && (all || t->control))
        {
            ostringstream out;
            out << sit->first << " " << state << " " << age << " sec";
            ss.push_back( out.str() );
        }
        else
        {
            ostringstream out;
            out << sit->first << " ";
            if (t->lastSeen)
                out << state << " " << age << " sec";
            else
                out << "never seen";
            olds.push_back( out.str() );
        }
    }
    unlock();
    if (all && olds.size())
    {
        ss.push_back("--- INACTIVE ---");
        ss.insert(ss.end(), olds.begin(), olds.end());
    }
    if (!discovered && ss.size())
    {
        ta->info.publish("No responses from other nodes - check firewall", ofColor::yellow, 4000);
    }
    return ss;
}

void Replicator::sendRouteHeader(const char *dest, const char *image, int wd, int ht, int melodyId, float cx, float cy, int col, int numpts)
{
    ofxOscMessage m;
    m.setAddress("/bo/route/header");
    m.addStringArg(image);
    m.addIntArg(wd);
    m.addIntArg(ht);
    m.addIntArg(melodyId);
    m.addFloatArg(cx);
    m.addFloatArg(cy);
    m.addIntArg(col);
    m.addIntArg(numpts);
    sendTo(dest, m);
}

void Replicator::sendRoutePoint(const char *dest, int melodyId, float x, float y)
{
    ofxOscMessage m;
    m.setAddress("/bo/route/point");
    m.addIntArg(melodyId);
    m.addFloatArg(x);
    m.addFloatArg(y);
    sendTo(dest, m);
}

void Replicator::sendTo(const char*dest, ofxOscMessage &m)
{
    lock();
    if (senders.find(dest) == senders.end())
    {
        ta->info.publish(string("not found: ") + dest, ofColor::yellow);
    }
    else
    {
        ofxOscSender *s = senders[dest]->sender;
        s->sendMessage(m);
        ta->info.publish("sent " + m.getAddress() + dest);
    }
    unlock();
}
