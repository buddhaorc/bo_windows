#ifndef AUDIOSAMPLER_H
#define AUDIOSAMPLER_H

#include "ofMain.h"
#include "ofxIniSettings.h"

struct SampleSet
{
    string dirname;
    bool init(string dir);
    vector<ofSoundPlayer*> samples;
    vector<int> durations;
    ~SampleSet();
    enum {NONE, DURATION, PITCH} speedctl;
};

class AudioSampler
{
    public:
        AudioSampler();
        virtual ~AudioSampler();
        void init(ofxIniSettings &ini);
        void send(int ch, int n, int velo, int duration, float x, float y);
        void enablePanning(bool pan) { panning = pan; }
        void setCenter(float x, float y) { cx = x; cy = y; }
        vector<string> getNames();
    protected:
    private:
        vector<SampleSet*> sets;
        bool panning;
        float cx, cy;
};

#endif // AUDIOSAMPLER_H
