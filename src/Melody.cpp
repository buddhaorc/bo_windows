#include "Melody.h"
#include "testApp.h"
#include "MidiFile.h"
#include "Chord.h"
#include "AllThreads.h"
#include "NotePlayer.h"
#include "Scale.h"

#include <strstream>

using namespace std;

Melody::GenType Melody::gtype = Melody::VERT_LINES;
int Melody::echoDelay = 2000;
int Melody::echoVelocity = -6;
int Melody::echoPitch = 0;

bool Melody::historyOn = false;

bool Melody::multiPause = true;
int Melody::pauseDuration = 50;

float Melody::arpeggioDelay = 0;


string Melody::storedKeys = "ruiasjzvmU0123456789ABCDEFGZLVN";

const int nnice[] = {0, 4, 7};
const int nsz = sizeof(nnice) / sizeof(nnice[0]);

static char buf[MAXTOKEN];

Melody * Melody::copy(int id, int trans)
{
    Melody *m = new Melody(trans, id);

    list<ofPoint*>::iterator pit = points.begin();
    for ( ; pit != points.end(); pit++)
    {
        ofPoint *p = *pit;
        m->addPoint(new ofPoint(*p));
    }

    m->boundingRect = boundingRect;
    m->setCenter(centroid, m_Colour);
    if (m->maxDist)
    {
        m_HasChild = true;
        m->m_ParentId = m_ParentId < 0 ? m_Id : m_ParentId;
        m->accord = accord;
        m->ccFlags = ccFlags;
        m->m_Bank = m_Bank;
        m->m_ImgHeight = m_ImgHeight;
        m->m_ImgWidth = m_ImgWidth;
        m->quality = quality;
        m->m_Ch0 = m_Ch0;
        m->drums = drums;
        m->effect = effect;
        m->inserted = inserted; // in the short list
        m->duration = duration; // total duration of all notes
        m->edge = edge; // is it needed? the route touches edge of the image
        m->paused = paused;
        m->vibrato = vibrato;
        m->portamento = portamento;
        m->reverse = reverse;
        m->volume = volume; // adjustment -127..+127

        m->m_legato = m_legato;
        m->keystory = keystory;
        m->ptrnIdx = ptrnIdx;
        m->m_Ptrn = m_Ptrn;

        m->m_Midi = m_Midi;
        m->m_Osc = m_Osc;
        m->m_Serial =  m_Serial;
        m->m_Sampler =  m_Sampler;
        return m;
    }
    else
    {
        delete m;
        return 0;
    }

}

SenderSet *Melody::getSenderSet()
{
    ss.mout = (m_Dest == MIDI || m_Dest == BOTH) ? m_Midi : 0;
    ss.osc = (m_Dest == OSC || m_Dest == BOTH) ? m_Osc : 0;
    ss.serial = (m_Dest == SERIAL) ? m_Serial : 0;
    ss.sampler = (m_Dest == AUDIO) ? m_Sampler : 0;
    ss.synth = (m_Dest == SYNTH) ? m_Synth : 0;
    return &ss;
}

int Melody::harmonize(int n)
{
    if (ofRandom(0, 100) < quality)
    {
        int mo = n % Scale::getNumNotes();
        int base = n - mo;
        for (int i=0; i < nsz; i++)
        {
            if (mo <= nnice[i]) return nnice[i] + base;
        }
        return base;
    }
    return n;
}

int Melody::dominant(int n)
{
    int mo = n % Scale::getNumNotes();
    return n - mo;
}

Melody::~Melody()
{
    //dtor
    list<ofPoint*>::iterator pit = points.begin();
    for ( ; pit != points.end(); pit++)
    {
        ofPoint *p = *pit;
        delete p;
    }

    clear();
}

void Melody::clear()
{
    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        Note *n = *nit;
        delete n;
    }
    melody.clear();
    duration = 0;
}

void Melody::goTo(float x, float y)
{
    Note *np = 0;
    float dist = 1e30;
    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        Note *n = *nit;
        float d = pow(n->pt.x - x, 2) + pow(n->pt.y - y, 2);
        if (!np || (d < dist))
        {
            np = n;
            dist = d;
        }
    }

    for (nit = melody.begin(); nit != melody.end(); nit++)
    {
        if (*nit == np)
        {
            Note *n = *playing;
            if (n->playing > 0)
            {
                stop(n);
            }
            playing = nit;
            return;
        }
    }
}

void Melody::extreme()
{
    if (melody.size() < 4) return;

    list<Note*>::iterator endit = melody.end();
    Note *prevnote = *(--endit);

    // distances: previous, current, and the next
    float pd = prevnote->distance(centroid);
    float cd = 0;
    float nd;

    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        list<Note*>::iterator nit1 = nit;
        if (++nit1 ==  melody.end())  break;

        Note *n = *nit;
        Note *n1 = *nit1;
        if (!cd) cd = n->distance(centroid);
        nd = n1->distance(centroid);

        float v = (cd - pd) * (cd - nd);
        //cout << "pd=" << pd << " cd=" << cd << " nd=" << nd << " v=" << v << endl;
        // extremum: the value is > 0
        if (v <=0 ) n->dur = -1; // mark for deletion
        pd = cd;
        cd = nd;
    }

    nit = melody.begin();
    while (nit != melody.end())
    {
        Note *n = *nit;
        if (n->dur < 0 )
        {
            nit = melody.erase(nit);
            delete n;
        }
        else
        {
            nit++;
        }
    }

    playing = melody.begin();
    countDurations();
}

void Melody::merge()
{
    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        list<Note*>::iterator nit1 = nit;
        list<Note*>::iterator nit2 = ++nit1;
        if ((nit1 ==  melody.end()) || (++nit2 == melody.end()))  break;

        Note *n = *nit;
        Note *n1 = *nit1;
        Note *n2 = *nit2;
        n->dur += n2->dur + n1->dur;
        if (playing == nit2 || playing == nit1) playing = nit;
        melody.erase(nit1);
        delete n1;

        melody.erase(nit2);
        delete n2;
    }
    limitDurations();
    countDurations();
}

// Unlike split(), this function removes every other note and
//  thus reduces overall duration of the melody
void Melody::reduce()
{
    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        list<Note*>::iterator nit1 = nit;
        if (++nit1 ==  melody.end())  break;

        Note *n = *nit;
        Note *n1 = *nit1;
        if (playing == nit1) playing = nit;

        melody.erase(nit1);
        delete n1;
    }
    countDurations();
}

void Melody::stop(Note *n)
{
    if (!n) n = current();
    n->playing = 0;
}

void Melody::split()
{
    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        Note *n = *nit;
        if (n->note0 < 0) continue;
        list<int> &spl = Analyzer::getNoteSplit();
        if (n->dur < spl.size()) continue;
        n->dur /= spl.size();
        n->dur *=2;
        list<int>::iterator is = spl.begin();
        for ( ; is != spl.end(); is++)
        {
            int incr = *is;
            int no = n->note0 + incr;
            if (no > Analyzer::highest) no = Analyzer::highest;
            if (no < Analyzer::lowest) no = Analyzer::lowest;
            int d = n->dur / spl.size();
            Note *n2 =new Note(n->pt, no, n->velo, 0, d);
            melody.insert(nit, n2);
        }
    }
    countDurations();
}

void Melody::generate(int w, int h, Ptrn *ptrn, NoteDuration *nd)
{
    keystory = "";
    centroid = centroid0;
    m_Ptrn = ptrn;
    m_ImgWidth = w;
    m_ImgHeight = h;
    clear();
    display = 10;
    if (!ptrn)
    {
        switch (gtype)
        {
            case ALL_POINTS:
                generateAllPoints(w, h);
                if (nd) {
                    nd->assignDurations(this);
                }
                break;
            case GRAVITY:
                generateAllPoints(w, h);
                gravityDurations();
                break;
            case VERT_LINES:
                generateVert(w, h);
                if (nd) {
                    nd->assignDurations(this);
                }
                break;
        }
        limitDurations();
        return;
    }
    list<ofPoint*>::iterator pit = points.begin();
    float scy = float(Analyzer::highest - Analyzer::lowest) / h;
    duration = 0;
    for ( ; pit != points.end(); pit++)
    {
        ofPoint *p = *pit;
        PtrnItem *pi = ptrn->getNext();
        if (!pi->dur) pi->dur = 1;
        int d = SHORTEST / pi->dur;
        int velo = pi->vol;
        if (velo < 0) velo = 0;
        if (velo >127) velo = 127;
        int no = Analyzer::highest - p->y * scy;
        if (no > Analyzer::highest) no = Analyzer::highest;
        if (no < Analyzer::lowest) no = Analyzer::lowest;
        Note *n = new Note(*p, no, velo, pi->cnt ? 4 : 0, d);
        melody.push_back(n);
        duration += d;
    }
    playing = melody.begin();
    cout << "Made " << melody.size() << " using " << ptrn->getTitle() << endl;
}

// TODO move this functionality to Duration.cpp
void Melody::gravityDurations()
{
    duration = 0;
    list<Note*>::iterator nit = melody.end();
    Note *prev = *(--nit);
    for (nit = melody.begin() ; nit != melody.end(); nit++)
    {
        Note *p = *nit;
        float dx = prev->pt.x - p->pt.x;
        float dy = prev->pt.y - p->pt.y;
        float d = fabs(atan2f(dy, dx)) * 5 + 1;
        p->dur = d;
        duration += d;
        prev = p;
    }
}

void Melody::generateAllPoints(int w, int h)
{
    float scy = float(Analyzer::highest - Analyzer::lowest) / h;
    // the longer melody - the shorter notes.
    int cnt = points.size();
    if (cnt > 512) cnt = 512;
    float scd = 128.0f / cnt;
    list<ofPoint*>::iterator pit = points.begin();
    duration = 0;
    for ( ; pit != points.end(); pit++)
    {
        ofPoint *p = *pit;
        int d = ofRandom(1, 8) * scd;
        if ((int(p->x) % 4) == 0 || (int(p->y) % 4) == 0) d *= 4;
        int no = Analyzer::highest - p->y * scy;
        if (no > Analyzer::highest) no = Analyzer::highest;
        if (no < Analyzer::lowest) no = Analyzer::lowest;
        Note *n = new Note(*p, no, 127, 0, d);
        melody.push_back(n);
        duration += d;
    }
    playing = melody.begin();
    // move to random location
//    int start = ofRandom(0, cnt - 1);
//    for (int i=0; i<start; ++i) playing++;
//    cout << "Added " << melody.size() << " notes " << endl;
}

void Melody::generateVert(int w, int h)
{
    float scx = float(Analyzer::highest - Analyzer::lowest) / w;
    float scy = 64.0f / h;
    // the longer melody - the shorter notes.
    int cnt = points.size();
    if (cnt > 512) cnt = 512;
    float scd = 128.0f / cnt;
    list<ofPoint*>::iterator pit = points.begin();
    duration = 0;
    while ( pit != points.end() )
    {
        ofPoint *p = *pit;
        int x = p->x;
        int d = 0;
        for ( ; pit != points.end() && ((*pit)->x == x); pit++) { d++; }
        if (d > 1)
        {
            int velo = p->x * scx + 63;// + 32 * ( 1+ sin(2 * M_PI * (float)pos / cnt));
            if (velo < 0) velo = 0;
            if (velo >127) velo = 127;
        int no = Analyzer::highest - p->y * scy;
        if (no > Analyzer::highest) no = Analyzer::highest;
        if (no < Analyzer::lowest) no = Analyzer::lowest;
            Note *n = new Note(*p, no, velo, 0, d);
            melody.push_back(n);
            duration += d;
        }

//        if (pit == points.end()) break;
    }
    if (melody.size() == 0)
    {
        cerr << "nothing!" << endl;
        edge = true;
    }
    else
    {
        playing = melody.begin();
    }
    // move to random location
//    int start = ofRandom(0, cnt - 1);
//    for (int i=0; i<start; ++i) playing++;
//    cout << "Added " << melody.size() << " notes " << endl;
}

void Melody::limitDurations()
{
    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        Note *n = *nit;
        if ((n->note0 >=0) && (n->dur > Analyzer::maxnote)) n->dur = Analyzer::maxnote;
        if ((n->note0 < 0) && (n->dur > Analyzer::maxpause)) n->dur = Analyzer::maxpause;
    }

}

bool Melody::advance(int &no, int &ve, int &du, int synch)
{
    if (paused && !Conditions::isActive(m_Id)) return false;

    Note *n = *playing;

    // synchronization
    if ((n->playing == 0) && n->synlev && (n->synlev != synch)) return false;

    // initial state: -1 - the note has not been played since creation.
    // it's not needed to stop it.
    if (n->playing < 0)
    {
        n->playing = n->dur;
        return true;
    }

    if (n->playing-- <= 0)
    {
        stop(n);
    }
    else
    {
        // no sound produced this time
        return false;
    }

    if (reverse)
    {
        bool ended = (playing == melody.begin());
        if (ended)
        {
            playing = melody.end();
        }
        --playing;
    }
    else
    {
        bool ended = (++playing == melody.end());
        if (ended)
        {
            playing = melody.begin();
        }
    }

    n = *playing;
    n->playing = n->dur;
    return true;
}

// Modern method from http://en.wikipedia.org/wiki/Fisher-Yates_shuffle
void permutate(int *v, int sz)
{
    int range = sz;
    for (int i = sz-1; i > 0; --i, --range)
    {
        int r = ofRandom(range);
        // swap [i] and [r]
        int t = v[i];
        v[i] = v[r];
        v[r] = t;
    }
}

int chordToArray(list<int> &chord, int *dest, int n0, int cType)
{
    int nn = 0;
    switch (cType)
    {
    default:

    case 1: // UP
        for (list<int>::iterator ci = chord.begin(); ci != chord.end(); ci++)
        {
            int no = *ci;
            if (no < n0) no += NOTE_NUM;
            dest[nn++] = no;
        }
        break;
    case 2: // DOWN
        for (list<int>::iterator ci = chord.end(); ci != chord.begin(); )
        {
            --ci;
            int no = *ci;
            if (no < n0) no += NOTE_NUM;
            dest[nn++] = no;
        }
        break;
    case 3: // UPDOWN
        nn = chordToArray(chord, dest, n0, 1);
        nn += chordToArray(chord, dest + nn, n0, 2);
        break;
    case 4: // RND
        nn = chordToArray(chord, dest, n0, 1);
        permutate(dest, nn);
        break;
    }

    return nn;
}

void Melody::sendDrum(int nmidi, int dtype, float v, float dt, float dur)
{
    if (m_Osc && (m_Dest == OSC))
    {
        ofxOscMessage m;
        m.setAddress( "/bo/drum" );
        m.addFloatArg( centroid.x );
        m.addFloatArg( centroid.y );
        // assuming there are no objects greater than 1/4
//        float maxarea = m_ImgWidth * m_ImgHeight / 4;
//        float volume = boundingRect.width * boundingRect.height / maxarea;
        m.addFloatArg( v );
        m.addIntArg( m_Id );
        m.addIntArg( dtype );
        m.addIntArg( 0 ); // TODO - access the colours
        m_Osc->sendMessage( m );
    }
    else
    {
        int ch = m_Ch0 < 0 ? (nmidi << 8) + 10 : m_Ch0;
        v *= 0.8f; // let drums be not as loud as a melody
        // we don't bend the pitch bend of drums
        int note = ((ch & 0xF) == 10) ? dtype :
            harmonize(Analyzer::highest - centroid.y * float(Analyzer::highest - Analyzer::lowest) / m_ImgHeight);
        new NotePlayer(*getSenderSet(), m_Id, ch, dt, dur, note, v, 0,
                       ofPoint(centroid.x / m_ImgWidth, centroid.y / m_ImgHeight) );
    }

}

// MIDI sends values in range 0..127
// Arduino in range 0..63
void Melody::sendControl(int ch, int tp, int val)
{
    if (m_Midi && (m_Dest == MIDI || m_Dest == BOTH))
    {
        m_Midi->sendControls(ch, tp, val);
    }
    if (m_Serial && (m_Dest == SERIAL))
    {
        int ch = -1;
        switch (tp)
        {
        case CC_ENVE:
            ch = 0;
            break;
        case CC_NOTE:
            ch = 1;
            break;
        case CC_VELO:
            ch = 2;
            break;
        case CC_TRCK:
            ch = 3;
            break;
        }
        if (ch >= 0)
        {
            m_Serial->sendControl(ch, val);
        }
    }
}

// return false if paused.
bool Melody::playNext(int ch, float vol, int tick, int synch)
{
    int d1, d2, d3; // dummy values - further we only use the incremented 'playing' iterater
    if (!advance(d1, d2, d3, synch)) return false;
    Note *n = *playing;

    if (m_Ch0 >= 0) ch = m_Ch0;
    if (drums) ch = (ch & 0xFF00) | 10; // midi channel 10 assigned for drums

    if (n->note0 < 0) return true;

//    midi.sendControlChange(ch, 7, full ? 127 : n->amp); // volume control
    int nt = n->note0;
    Note *nxt = getNextNote();
    if (drums) {
        n->note = nt = DrumKit::drumMelos[nt % DrumKit::numDrumMelos];
    } else if (nxt->note0 < 0) {
        // the next note is pause. This one should be dominant.
        nt = n->note = dominant(nt);
    } else {
        nt = n->note = harmonize(nt);
    }
    if (vol > 1) vol = 1;

    int env = vol * 127;
    if (maxDist != minDist)
    {
        float d = n->distance(centroid);
        float co = 1 - m_dynamic * (maxDist - d) / (maxDist - minDist);
        vol *= co;
    }
    //cout << "Playing " << n->note << " vel=" << n->velo * vol << endl;

    vol *= n->velo;
    vol += volume;
    if (vol > 127) vol = 127;
    if (vol < 0) vol = 0;
    float v = full ? 127 : vol;

    if (ccFlags & CC_NOTE) {
        sendControl(ch, CC_NOTE, (float)n->pt.x / m_ImgWidth * 127);
    }
    if (ccFlags & CC_ENVE) {
        sendControl(ch, CC_ENVE, env);
    }
    if (ccFlags & CC_VELO) {
        sendControl(ch, CC_VELO, v);
    }

    // a muted melody only sends CC commands
    if (ccFlags & CC_MUTE) {
        return true;
    }

    bool st = (nxt->note0 != nt) || (m_legato <= 1.0);
    int dur = (st ? n->dur : n->dur * 2) * tick * m_legato;
    addPlayed(nt);
    ofPoint pt = ofPoint((float)n->pt.x / m_ImgWidth, (float)n->pt.y / m_ImgHeight);
    if ((accord != 0) && Scale::hasTwelveNotes())
    {
        int base = dominant(nt);
        int n0 = nt - base;
        list<int> chord;
        int j;
        std::list<Note*>::iterator nxt = playing;
        switch (accord)
        {
        default:
            chord = CHord::getChord(n0);
            break;
        case 5:
            chord = played;
            break;
        }
        int cn[NOTE_NUM * 2];
        int nn = chordToArray(chord, cn, n0, accord);
        cout << nt << "(" << nn << ")-> ";
        for (int i = 0; i < nn; i++)
        {
            int nnn;
            float bend = 0;
            if(drums)
            {
                nnn = DrumKit::drumMelos[(base + (cn[i] % NOTE_NUM)) % DrumKit::numDrumMelos];
            }
            else
            {
                Scale::midiNoteToScaled(base + (cn[i] % NOTE_NUM), nnn, bend);
            }

            int d = dur;
            float shft = tick * arpeggioDelay;
            // limit duration of all notes except the last so that
            // pitch bend would not affect them
            if (Scale::enabled() && (i < nn - 1))
            {
                d = shft;
            }
            cout << nnn << " ";
            new NotePlayer(*getSenderSet(), m_Id, ch, i * shft, d, nnn, v, bend, pt);
        }
        cout << endl;
    }
    else
    {
        for (int i = 0; i <= m_echo; i++)
        {
            int nnn;
            float bend = 0;
            if(drums)
            {
                nnn = DrumKit::drumMelos[nt % DrumKit::numDrumMelos];
            }
            else
            {
                Scale::midiNoteToScaled(nt, nnn, bend);
            }

            new NotePlayer(*getSenderSet(), m_Id, ch, i * Melody::echoDelay, dur, nnn, v, bend, pt);

            nt += Melody::echoPitch;
            if (nt < 0 || nt > 127) break;
            v += Melody::echoVelocity;
            if (v < 0) break;
            if (v > 127) v = 127;
        }
    }

    return true;
}

void Melody::addPlayed(int n)
{
    // make sure we have no duplicate notes in the chord/arpeggio
    played.remove(n);

    if (played.size() >= 3)
    {
        played.pop_front();
    }

    played.push_back(n);
}

void Melody::midiThrough(int ch, MidiStatus cmd, int b1, int b2)
{
    if (cmd == MIDI_NOTE_ON)
    {
        b2 += volume;
        if (b2 < 0) b2 = 0;
        if (b2 > 127) b2 = 127;
        if (ch == 0 || m_Dest == OSC || m_Dest == BOTH)
        {
            ofPoint p(centroid.x, centroid.y);
            NotePlayer::sendNoteOsc(m_Osc, m_Id, ch, p, b1, b2, 0.5); // duration is unknown
        }
        if (ch > 0 && (m_Dest == MIDI || m_Dest == BOTH))
        {
            m_Midi->sendNoteOn(ch, b1, b2);
        }
    }
    else if (cmd == MIDI_NOTE_OFF)
    {
        if (ch == 0 || m_Dest == OSC || m_Dest == BOTH)
        {
            NotePlayer::stopNoteOsc(m_Osc, m_Id, ch);
        }
        if (ch > 0 && (m_Dest == MIDI || m_Dest == BOTH))
        {
            m_Midi->sendNoteOff(ch, b1, b2);
        }
    }
    else if (cmd == MIDI_CONTROL_CHANGE)
    {
        if (ch > 0 && (m_Dest == MIDI || m_Dest == BOTH))
        {
            m_Midi->sendControlChange(ch, b1, b2);
        }
    }

}

float Note::distance(ofPoint p)
{
    ofPoint opt(pt.x, pt.y);
    return opt.distance(p);
}

void Melody::startTrackOsc()
{
    if (m_Osc)
    {
        ofxOscMessage m;
        m.setAddress( "/bo/track/on" );
        m.addIntArg( m_Id );
        m.addFloatArg( (float)centroid.x / m_ImgWidth );
        m.addFloatArg( (float)centroid.y / m_ImgHeight );
        m_Osc->sendMessage( m );
    }
}

void Melody::stopTrackOsc()
{
    if (m_Osc)
    {
        ofxOscMessage m;
        m.setAddress( "/bo/track/off" );
        m.addIntArg( m_Id );
        m_Osc->sendMessage( m );
    }
}

// ADSR parameters
void Melody::sendADSR(int par, int val)
{
    if (m_Osc)
    {
        ofxOscMessage m;
        m.setAddress( "/bo/adsr" );
        m.addIntArg( m_Id );
        m.addIntArg( par );
        m.addIntArg( val );
        m_Osc->sendMessage( m );
    }
    if (par)
    {
        switch(par)
        {
        case 1:
            par = CC_ENVE;
            val = val * 127 / 1000;
            break;
        case 2:
            par = CC_NOTE;
            val = val * 127 / 1000;
            break;
        case 3:
            par = CC_VELO;
            val = val * 127 / 100;
            break;
        case 4:
            par = CC_TRCK;
            val = val * 127 / 1000;
            break;
        default:
            return;
        }
        sendControl(m_Ch0, par, val);
    }
}

void Melody::sendADSR(int amp, int a, int d, int s, int r, int e)
{
    if (m_Osc)
    {
        ofxOscMessage m;
        m.setAddress( "/bo/adsr/all" );
        m.addIntArg( m_Id );
        m.addIntArg( amp );
        m.addIntArg( a );
        m.addIntArg( d );
        m.addIntArg( s );
        m.addIntArg( r );
        m.addIntArg( e );
        m_Osc->sendMessage( m );
    }
}

const char* Melody::drumInfo()
{
    static char drumName[256];
    if (m_pDrum) {
        if(isDestOsc()) {
            sprintf(drumName, "OSC %i[%i]", m_pDrum->getDrumType(), m_pDrum->getDrumCount());
        } else {
            if (((m_Ch0 & 0xf) == 10) || (m_Ch0 < 0))
            {
                sprintf(drumName, "%s[%i]",
                    // for drums, the MIDI out parameter is ignored
                    MidiInstruments::getName(0, DrumKit::getDrumType(m_pDrum->getDrumType()), true),
                    m_pDrum->getDrumCount());
            }
            else
            {
                sprintf(drumName, "%s[%i]", instrInfo(), m_pDrum->getDrumCount());
            }
        }
    } else {
        drumName[0] = 0;
    }
    return drumName;
}

string Melody::getDestName(int i)
{
    switch (i)
    {
        case MIDI: return "MIDI";
        case OSC: return "OSC";
        case BOTH: return "MIDI+OSC";
        case SERIAL: return "SERIAL";
        case AUDIO: return "AUDIO";
        case SYNTH: return "SYNTH";
    }
    return "";
}

string Melody::getDestName()
{
    return getDestName(m_Dest);
}

void Melody::setDest(string nm)
{
    if (nm == "MIDI") m_Dest = MIDI;
    else if (nm == "OSC") m_Dest = OSC;
    else if (nm == "MIDI+OSC") m_Dest = BOTH;
    else if (nm == "SERIAL") m_Dest = SERIAL;
    else if (nm == "AUDIO") m_Dest = AUDIO;
    else if (nm == "SYNTH") m_Dest = SYNTH;
}

string Melody::getChordName(int i)
{
    switch (i)
    {
        case 0: return "None";
        case 1: return "Up";
        case 2: return "Down";
        case 3: return "UpDown";
        case 4: return "Random";
        case 5: return "History";
    }
    return "";
}

string Melody::getChordName()
{
    return getChordName(accord);
}

void Melody::setChord(string nm)
{
    if (nm == "None") accord = 0;
    else if (nm == "Up") accord = 1;
    else if (nm == "Down") accord = 2;
    else if (nm == "UpDown") accord = 3;
    else if (nm == "Random") accord = 4;
    else if (nm == "History") accord = 5;
}

const char* Melody::instrInfo()
{
    if (m_Ch0 < 0) return "undefined";
    if (m_Dest == OSC) {
        static char osc[200];
        int cnt = snprintf(osc, sizeof(osc) - 1, "OSC %i", m_Ch0);
        return osc;
    }
    if (m_Dest == SERIAL) {
        return "SER";
    }
    if (m_Dest == SYNTH) {
        return "SYNTH";
    }
    if (m_Dest == AUDIO) {
        return "AUDIO";
    }
    int nm = m_Ch0 >> 8;
    int ch = (m_Ch0 & 0xff) ? ((m_Ch0 - 1) & 0xf) + 1 : 0;
    const char * name = MidiInstruments::getName(nm, ch, drums);
    return (name && name[0]) ? name : "undefined";
}

void Melody::ccFlagsInfo(char *buf)
{
    char *p = buf;
    if (ccFlags & CC_ENVE) *p++ = 'L';
    if (ccFlags & CC_NOTE) *p++ = 'N';
    if (ccFlags & CC_TRCK) *p++ = 'K';
    if (ccFlags & CC_VELO) *p++ = 'V';
    if (ccFlags & CC_MUTE) *p++ = 'U';
    *p = 0;
}

const char* Melody::info(float dur)
{
    static char buf[5120];
    char flags[40];
    char chord[40];
    char *dest;
    switch (m_Dest)
    {
        case MIDI: dest = "M"; break;
        case OSC: dest = "O"; break;
        case BOTH: dest = "M+O"; break;
        case SERIAL: dest = "S"; break;
        case SYNTH: dest = "SY"; break;
        case AUDIO: dest = "A"; break;
        default: dest = "???";
    }
    ccFlagsInfo(flags);
    strcpy(chord, getChordName().c_str());
    int n = snprintf(buf, sizeof(buf) - 100, "%i n=%i %.1fs v=%i %s %s %s %s q=%i ch=%i %.1f [%s] (%s) (%s) (%s) E%i D%.2f B%i =%s",
            m_Id, melody.size(), dur, volume, (getPlayingState() & MPLAY) == 0 ? "paused" : "playing",
            full ? "max": "", chord, reverse ? "Back" : "Forth", quality,
            m_Ch0, m_legato, // m_Bank,
            flags,
            instrInfo(),
            dest,
            drumInfo(),
            m_echo,
            m_dynamic,
            m_Beat,
            keystory.c_str());
    buf[n] = 0;
    if (m_Ptrn)
    {
        strcat(buf, " ");
        strcat(buf, m_Ptrn->getTitle());
    }
    return buf;
}

bool Melody::control(int key)
{
    int val;
    int midiOffset = MidiOut::getMidiNo() << 8;
    storeKey(key);
    // cout << "mel " << m_Id << " key " << key << endl;
    switch(key)
    {
        case 'p': // play or pause
            if ((paused = !paused))
            {
                stop(0);
                stopTrackOsc();
            }
            else
            {
                startTrackOsc();
                sendControl(m_Ch0, CC_TRCK, (float) centroid.x / m_ImgWidth * 127 );
            }
            break;
        case 'f':
            full = !full;
            break;

        case 'V':
            toggleCCflag(CC_VELO);
            break;
        case 'K':
            toggleCCflag(CC_TRCK);
            break;
        case 'N':
            toggleCCflag(CC_NOTE);
            break;
        case 'L':
            toggleCCflag(CC_ENVE);
            break;
        case 'u':
            toggleCCflag(CC_MUTE);
            break;

        case 'a':
            if (!paused)
            {
                stop(0); // current
            }
            if (++accord == 6) accord = 0;
            break;
        case 'b':
            reverse = !reverse;
            break;
        // fixed channel assignment
        case '0': case '1': case '2': case '3':
        case '4': case '5': case '6': case '7':
        case '8': case '9':
            m_Ch0 = key - '0' + midiOffset;
            drums = false;
            break;
        case 'A': case 'B': case 'C':
        case 'D': case 'E': case 'F': case 'G':
            m_Ch0 = key - 'A' + 10 + midiOffset;
            drums = key == 'A';
            break;
        case '(':
            if (m_legato > 0.1) {
               m_legato -= 0.1;
            }
//            if (m_Bank) {
//               m_Midi->sendBankSelect(m_Ch0, --m_Bank);
//            }
            break;
        case ')':
            if (m_legato < 1.5) {
               m_legato += 0.1;
            }
//            m_Midi->sendBankSelect(m_Ch0, ++m_Bank);
            break;
        case 'v':
            effect = !effect;
            val = effect ? 127 : 0;
            m_Midi->sendControlChange(m_Ch0, MidiInstruments::m_Effect, val);
            cout << "ch=" << hex << m_Ch0 << dec <<" effect " << MidiInstruments::m_Effect << " val=" << val << endl;
            break;
        case 'i':
            moveTo(ofPoint(centroid) += ofPoint(0, -20));
            break;
        case 'j':
            moveTo(ofPoint(centroid) += ofPoint(0, 20));
            break;
        case 267: // F11
            if (m_dynamic >= 0.05) m_dynamic -= 0.05; else m_dynamic = 0;
            setDynamic(m_dynamic);
            break;
        case 268: // F12
            if (m_dynamic <= 0.95) m_dynamic += 0.05; else m_dynamic = 1;
            setDynamic(m_dynamic);
            break;
        case 'H':
            if (m_echo > 0) --m_echo;
            break;
        case 'J':
            ++m_echo;
            break;
        case 'T':
            stop();
            switch (m_Dest)
            {
                // MIDI, AUDIO, SYNTH, OSC, BOTH, SERIAL
                case MIDI: m_Dest = AUDIO; break;
                case AUDIO: m_Dest = SYNTH; break;
                case SYNTH: m_Dest = OSC; break;
                case OSC: m_Dest = BOTH; break;
                case BOTH: m_Dest = SERIAL; break;
                case SERIAL: m_Dest = MIDI; break;
            }
            break;
        case ':':
            vibrato = !vibrato;
            val = vibrato ? 127 : 0;
            m_Midi->sendControlChange(m_Ch0, 1, val);
            cout << "Mod wheel " << val << endl;
            break;
        case ';':
            portamento = !portamento;
            val = portamento ? 127 : 0;
            m_Midi->sendControlChange(m_Ch0, 5, val); // time coarse
            m_Midi->sendControlChange(m_Ch0, 65, val); // on/off
            cout << "Portamento " << val << endl;
            break;
        case '#':
            if (++m_Beat >= NSLIDERS)
            {
                m_Beat = 0;
            }
            break;
/*
1 Modulation wheel
7 Volume
10 Pan
11 Expression
64 Sustain pedal
91 Reverb level
92 Tremolo level
93 Chorus level
94 Celeste level
95 Phaser level
Filter Resonance (Timbre/Harmonic Intensity) (cc#71)
Release Time (cc#72)
Attack time (cc#73)
Brightness/Cutoff Frequency (cc#74)
Decay Time (cc#75)
Vibrato Rate (cc#76)
Vibrato Depth (cc#77)
Vibrato Delay (cc#78)
*/
    default:
        cout << (char)key << endl;
        return false;
    }

    return true;
}

void Melody::toggleCCflag(int fl) {
    if (ccFlags & fl) {
        ccFlags &= (~fl);
    } else {
        ccFlags |= fl;
    }
}

void Melody::setDynamic(float d) {
    m_dynamic = d;
}

Note::Note(ofPoint p, int n, int v, int syn, int d) : pt(p)
{
    note0 = n;
    note = 0;
    velo = v;
    synlev = syn;
    playing = -1;
    dur = d;
    pt = p;
}

char *Note::noteString(int n)
{
    const char *names[] = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    const char *snames = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static char buf[20];
    if (n < 0)
    {
        strcpy(buf, "P");
    }
    else
    {
        if (Scale::hasTwelveNotes())
        {
            int m = n % NOTE_NUM;
            int oct = n / NOTE_NUM;
            sprintf(buf, "%i%s", oct, names[m]);
        }
        else
        {
            int m = n % Scale::getNumNotes();
            int oct = n / Scale::getNumNotes();
            if (m < 36)
            {
                sprintf(buf, "%i/%c", oct, snames[m]);
            }
            else
            {
                sprintf(buf, "%i/-", oct);
            }
        }
    }
    return buf;
}

int MidiInstruments::m_Instrs[MAX_MIDI][NCH];
int MidiInstruments::m_InstrNum = 0;
int MidiInstruments::m_Effect = 1;

void MidiInstruments::assignChannels(int nm, MidiOutEx &mo, string &instrs)
{
    bool replay = instrs == "REPEAT";
    vector<string> inums;
    if (!replay) inums = ofSplitString(instrs, ",", true, true);

    // assign instruments
    for (int ch=0; ch<NCH; ch++)
    {
        if (replay)
        {
            if (m_Instrs[nm][ch] >= 0)
            {
                mo.sendProgramChange(ch+1, m_Instrs[nm][ch]);
            }
        }
        else if (ch < inums.size())
        {
            int inst = ofToInt(inums[ch]);
            mo.sendProgramChange(ch+1, m_Instrs[nm][ch] = inst);
        } else {
            m_Instrs[nm][ch] = -1;
        }
    }
    cout << endl;
    if (m_InstrNum <= nm) m_InstrNum = nm + 1;
}

void MidiInstruments::assignBank(int nm, MidiOutEx &mo, int ch, int b)
{
//  Bank Select MSB (Most Significant Byte) = MIDI Controller 0
//  Bank Select LSB (Least Significant Byte) = MIDI Controller 32
    mo.sendControlChange(ch, 0, b >> 8);
    mo.sendControlChange(ch, 32, b &0xff);
    mo.sendProgramChange(ch, MidiInstruments::m_Instrs[nm][ch-1]);

}

const char *MidiInstruments::getName(int nm, int n, bool drum)
{
    static char buff[256];
    if (n == 0)
    {
        return "{OSC}";
    }

    if ((n < 0) || !m_InstrNum)
    {
        return "";
    }

    const char *names[] = {
        // 1..20
        "Acoustic Grand Piano", "Bright Acoustic Piano", "Electric Grand Piano", "Honky-tonk Piano", "Electric Piano 1",
        "Electric Piano 2", "Harpsichord", "Clavi", "Celesta", "Glockenspiel",
        "Music Box", "Vibraphone", "Marimba", "Xylophone", "Tubular Bells",
        "Dulcimer", "Drawbar Organ", "Percussive Organ", "Rock Organ", "Church Organ",
        // 21..40
        "Reed Organ", "Accordion", "Harmonica", "Tango Accordion","Acoustic Guitar (nylon)",
        "Acoustic Guitar (steel)", "Electric Guitar (jazz)", "Electric Guitar (clean)", "Electric Guitar (muted)", "Overdriven Guitar",
        "Distortion Guitar", "Guitar harmonics", "Acoustic Bass", "Electric Bass (finger)", "Electric Bass (pick)",
        "Fretless Bass", "Slap Bass 1", "Slap Bass 2", "Synth Bass 1", "Synth Bass 2",
        // 41..60
        "Violin", "Viola", "Cello", "Contrabass", "Tremolo Strings",
        "Pizzicato Strings", "Orchestral Harp", "Timpani", "String Ensemble 1", "String Ensemble 2",
        "SynthStrings 1", "SynthStrings 2", "Choir Aahs", "Voice Oohs", "Synth Voice",
        "Orchestra Hit", "Trumpet", "Trombone", "Tuba", "Muted Trumpet",
        // 61..80
        "French Horn", "Brass Section", "SynthBrass 1", "SynthBrass 2", "Soprano Sax",
        "Alto Sax", "Tenor Sax", "Baritone Sax", "Oboe", "English Horn",
        "Bassoon", "Clarinet", "Piccolo", "Flute", "Recorder",
        "Pan Flute", "Blown Bottle", "Shakuhachi", "Whistle", "Ocarina",
        // 81..100
        "Lead 1 (square)", "Lead 2 (sawtooth)", "Lead 3 (calliope)", "Lead 4 (chiff)", "Lead 5 (charang)",
        "Lead 6 (voice)", "Lead 7 (fifths)", "Lead 8 (bass + lead)", "Pad 1 (new age)", "Pad 2 (warm)",
        "Pad 3 (polysynth)", "Pad 4 (choir)", "Pad 5 (bowed)", "Pad 6 (metallic)", "Pad 7 (halo)",
        "Pad 8 (sweep)", "FX 1 (rain)", "FX 2 (soundtrack)", "FX 3 (crystal)", "FX 4 (atmosphere)",
        // 101..120
        "FX 5 (brightness)", "FX 6 (goblins)", "FX 7 (echoes)", "FX 8 (sci-fi)", "Sitar",
        "Banjo", "Shamisen", "Koto", "Kalimba", "Bag pipe",
        "Fiddle", "Shanai", "Tinkle Bell", "Agogo", "Steel Drums",
        "Woodblock", "Taiko Drum", "Melodic Tom", "Synth Drum", "Reverse Cymbal",
        // 121..128
        "Guitar Fret Noise", "Breath Noise", "Seashore", "Bird Tweet", "Telephone Ring",
        "Helicopter", "Applause", "Gunshot"
    };

    // 35..81
    const char *drums[] = {
        "Acoustic Bass Drum", "Bass Drum 1", "Side Stick", "Acoustic Snare", "Hand Clap",
        // 41..60
        "Electric Snare", "Low Floor Tom", "Closed Hi Hat", "High Floor Tom", "Pedal Hi-Hat",
        "Low Tom", "Open Hi-Hat", "Low-Mid Tom", "Hi-Mid Tom", "Crash Cymbal 1",
        "High Tom", "Ride Cymbal 1", "Chinese Cymbal", "Ride Bell", "Tambourine",
        "Splash Cymbal", "Cowbell", "Crash Cymbal 2", "Vibraslap",
        // 61..80
        "Ride Cymbal 2", "Hi Bongo", "Low Bongo", "Mute Hi Conga", "Open Hi Conga",
        "Low Conga", "High Timbale", "Low Timbale", "High Agogo", "Low Agogo",
        "Cabasa", "Maracas", "Short Whistle", "Long Whistle", "Short Guiro",
        "Long Guiro", "Claves", "Hi Wood Block", "Low Wood Block", "Mute Cuica",

        "Open Cuica", "Mute Triangle", "Open Triangle"
    };

    if (drum)
    {
        const char *dr = "{DRUMS}";
        if (n >= 35 && n <= 81) dr = drums[n - 35];
//        sprintf(buff, "%i %s", n, dr);
        sprintf(buff, "%s", dr);
    }
    else
    {
        int iNum = m_Instrs[nm % m_InstrNum][n - 1];
        if (iNum < 0) { return ""; }
//        sprintf(buff, "%i %s", iNum, names[iNum - 1]);
        sprintf(buff, "%s", names[iNum - 1]);
    }
    return buff;
}

void Melody::draw(float ox, float oy, float sx, float sy, bool line, bool fill, ofColor c)
{
    if (display > 0) display--;
    if (display) c = ofColor::white;
    int cnt = melody.size();

    if (fill)
    {
        ofFill();
        float offs = centroid.distance(centroid0) + 10;
        ofSetColor(c, offs > 80 ? 80 : offs);
    }
    else
    {
        ofNoFill();
        ofSetColor(c);
    }
    ofSetPolyMode(OF_POLY_WINDING_ODD);	// this is the normal mode
    if (line && cnt < 200)
    {
        ofSetLineWidth(1);
        ofBeginShape();
        list<ofPoint*>::iterator pit = points.begin();
        for ( ; pit != points.end(); pit++)
        {
            ofPoint *p = *pit;
            float x = p->x * sx + ox;
            float y = p->y * sy + oy;
            ofVertex(x, y);
        }
        ofEndShape(OF_CLOSE);
    }

    ofSetLineWidth(2);
    ofBeginShape();
    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        Note *n = (*nit);
        if (n->velo == 0) continue;
        ofPoint &p = n->pt;
        ofVertex(p.x * sx + ox, p.y *sy + oy);
    }
    ofEndShape(OF_CLOSE);
    ofFill();

    ofSetColor(c);

    if (cnt < 20)
    {
        list<Note*>::iterator nit = melody.begin();
        for ( ; nit != melody.end(); nit++)
        {
            Note *n = (*nit);
            if (n->velo == 0) continue;
            if (line || display) ofSetColor(n->synlev ? ofColor::red : c);
            ofPoint &p = n->pt;
            ofCircle(p.x * sx + ox, p.y * sy + oy, 2);
        }
    }

    if (line)
    {
        ofSetColor(c);
        const int D = 3;
        float cx = centroid.x * sx + ox;
        float cy = centroid.y * sy + oy;
        ofLine(cx - D, cy, cx + D, cy);
        ofLine(cx, cy - D, cx, cy + D);
    }
}

void Melody::moveTo(ofPoint &newpos)
{
    float dx = newpos.x - centroid.x;
    float dy = newpos.y - centroid.y;
//    list<ofPoint*>::iterator pit = points.begin();
//    for ( ; pit != points.end(); pit++)
//    {
//        ofPoint *p = *pit;
//        p->x += dx;
//        p->y += dy;
//    }

    float scy = float(Analyzer::highest - Analyzer::lowest) / m_ImgHeight;
    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        Note *n = (*nit);
        ofPoint &p = n->pt;
        p.x += dx;
        p.y += dy;
        if (n->note0 >= 0) n->note0 = Analyzer::highest - p.y * scy;
    }
    centroid = newpos;
    display = 5;
}

void Melody::scale(float sx, float sy)
{

}

bool pauseNote(Note* n)
{
    if (n->note0 < 0)
    {
        delete n;
        return true;
    }
    return false;
}

void Melody::removePauses()
{
    melody.remove_if(pauseNote);
    playing = melody.begin();
    countDurations();
}

void Melody::addPauses()
{
    if(!multiPause)
    {
        Note *n = current();
        Note *p = new Note(n->pt, -1, n->velo, 0, pauseDuration);
        p->note = -1;
        melody.insert(playing, p);
        countDurations();
        return;
    }

    const int skip = 5;
    list<Note*>::iterator nit = melody.begin();
    if (melody.size() <= skip)
    {
        Note *n = *nit;
        Note *p = new Note(n->pt, -1, n->velo, 0, n->dur);
        p->note = -1;
        melody.insert(melody.end(), p);
        countDurations();
        return;
    }
//    list<Note*>::iterator last = melody.end();
    for ( ; nit != melody.end(); nit++)
    {
        for (int i=0; i < skip; ++i) if (++nit == melody.end()) return; // done
        Note *n = *nit;
        Note *p = new Note(n->pt, -1, n->velo, 0, n->dur);
        p->note = -1;
        melody.insert(nit, p);
    }
    countDurations();
}

void Melody::addPauses(int longest)
{
    int add = longest - duration;
    if ( add <= 0 ) return;
    add %= (duration + 3);

    list<Note*>::iterator nit = melody.begin();
    // search for the longest note and insert a pause after it.
    list<Note*>::iterator ins = nit;
    int maxd = 0;
    for ( ; nit != melody.end(); nit++)
    {
        Note *n = *nit;
        if (n->dur > maxd)
        {
            maxd = n->dur;
            ins = nit;
        }
    }

    Note *n = *ins;
    Note *p = new Note(n->pt, -1, n->velo, 0, add);
    p->note = -1;
    melody.insert(++ins, p);

    limitDurations();
    countDurations();
}

Ptrn::Ptrn(string p)
{
    char *t = new char[p.length() + 1];
    strcpy(t, p.c_str());
    char *tok = strtok(t, ":");
    strncpy(title, tok, sizeof(title) - 1);
    cout << title << ":";
    int a,b;
    int cnt = 0;
    while ((tok = strtok(0, ";")) != 0 && sscanf(tok, "%i,%i", &a, &b) == 2)
    {
        PtrnItem * pt = new PtrnItem();
        pt->dur = a;
        pt->vol = b;
        pt->cnt = cnt++;
        ptrn.push_back(pt);
        cout << a << "," << b << ";";
    }
    cout << endl;
    delete [] t;
    now = ptrn.begin();
}

Ptrn::~Ptrn()
{
    for (now = ptrn.begin(); now != ptrn.end(); now++)
    {
        PtrnItem *pi = (*now);
        delete pi;
    }
}

PtrnItem *Ptrn::getNext()
{
    PtrnItem *pi = *now;
    if (++now == ptrn.end()) now = ptrn.begin();
    return pi;
}

PatternSet::PatternSet()
{
    m_Cnt = 0;
    memset(ptrns, 0, sizeof(ptrns));
}

void PatternSet::init(ofxIniSettings &ini)
{
    // No assignment to ptrs[0] - melody is generated patternless
    while (m_Cnt++ < MAXPTRN)
    {
        char buf[10];
        sprintf(buf, "r%i", m_Cnt);
        string p = ini.get(buf, string("NONE"));
        if (p == "NONE") break;
        Ptrn *ptr = new Ptrn(p);
        ptrns[m_Cnt] = ptr;
    }
}

PatternSet::~PatternSet()
{
    for(int i=0; i<MAXPTRN; ++i) delete ptrns[i];
}

void Melody::countDurations()
{
    int d = 0;
    list<Note*>::iterator nit = melody.begin();
    for ( ; nit != melody.end(); nit++)
    {
        Note *n = *nit;
        d += n->dur;
    }
    duration = d;
}

void Melody::setCenter(ofPoint &c, ofColor col)
{
    centroid0 = centroid = c;
    float md = 0;
    float xd = 1e30;
    list<ofPoint*>::iterator nit = points.begin();
    for ( ; nit != points.end(); nit++)
    {
        ofPoint *p = *nit;
        ofPoint pt(p->x, p->y);
        float d = pt.distance(centroid);
        if (d > md) md = d;
        if (d < xd) xd = d;
    }
    maxDist = md;
    minDist = xd;
    m_dynamic = (md != xd) ? (md - xd) / md : 1;
    m_Colour = col;
}

void Melody::write(ostream &os)
{
    os << "{ID=" << m_Id <<"}";

    os << "{Points=";
    for (list<ofPoint*>::iterator ps = points.begin(); ps != points.end(); ++ps)
    {
        os << "{x=" << (*ps)->x <<"}";
        os << "{y=" << (*ps)->y <<"}";
    }
    os <<"}";

    os << "{Notes=";
    for (list<Note*>::iterator ns = melody.begin(); ns != melody.end(); ++ns)
    {
        (*ns)->write(os);
    }
    os <<"}";
}

bool Melody::read(istream &os)
{

}

void Note::write(ostream &os)
{
    os << "{note=" << note0 << "}";
    os << "{velo=" << velo << "}";
    os << "{synlev=" << synlev << "}";
    os << "{dur=" << dur <<"}";
    os << "{pt=";
    os << "{x=" << pt.x <<"}";
    os << "{y=" << pt.y <<"}";
    os << "}";
}

bool Note::read(istream &is)
{
    char key[MAXTOKEN];
    return MeloParser::nextInt(is, "note", note0) == MeloParser::OK &&
        MeloParser::nextInt(is, "velo", velo) == MeloParser::OK &&
        MeloParser::nextInt(is, "synlev", synlev) == MeloParser::OK &&
        MeloParser::nextInt(is, "dur", dur) == MeloParser::OK &&
        MeloParser::nextInt(is, "velo", velo) == MeloParser::OK &&
        MeloParser::nextKey(is, key) == MeloParser::OK && !strcmp(key, "pt") &&
        MeloParser::nextFloat(is, "x", pt.x) == MeloParser::OK &&
        MeloParser::nextFloat(is, "y", pt.y) == MeloParser::OK;
}

const int MeloParser::OK = 0;
const int MeloParser::EOM = -1;
const int MeloParser::CLOSE = -2;
const int MeloParser::ERR = -3;

int MeloParser::nextKey(istream &is, char *buf)
{
    char c;
    if (!(is >> c)) return EOM;
    if (c == '}') return CLOSE;
    for (int ptr = 0; (is >> c) && (ptr < MAXTOKEN); ++ptr)
    {
//        cout << "c " << c << endl;
        if (c == '=')
        {
            buf[ptr] = 0;
//            cout << "key " << buf << endl;
            return OK;
        }
        buf[ptr] = c;
    }
    return EOM;
}

int MeloParser::nextValue(istream &is, char *buf)
{
    char c;
    for (int ptr = 0; (is >> c) && (ptr < MAXTOKEN); ++ptr)
    {
//        cout << "c " << c << endl;
        if (c == '}')
        {
            buf[ptr] = 0;
//            cout << "val " << buf << endl;
            return OK;
        }
        buf[ptr] = c;
    }
    return EOM;
}

int MeloParser::nextInt(istream &is, const char *key, int &val)
{
    int r = MeloParser::nextKey(is, buf);
    if (r != OK) return r;
    if (strcmp(buf, key)) { cerr << "Expected " << key << " found " << buf << endl; return ERR; }
    r = MeloParser::nextValue(is, buf);
    if (r != OK) { cerr << "error " << r << " buf " << buf << endl; return r; }
    val = ofToInt(buf);
    return OK;
}

int MeloParser::nextFloat(istream &is, const char *key, float &val)
{
    int r = MeloParser::nextKey(is, buf);
    if (r != OK) return r;
    if (strcmp(buf, key)) { cerr << "Expected " << key << " found " << buf << endl; return ERR; }
    r = MeloParser::nextValue(is, buf);
    if (r != OK) { cerr << "error " << r << " buf " << buf << endl; return r; }
    val = ofToFloat(buf);
    return OK;
}

int MeloParser::nextBool(istream &is, const char *key, bool &val)
{
    int t = 0;
    int r = MeloParser::nextInt(is, key, t);
    if (r != OK) return r;
    val = t != 0;
    return OK;
}

Note *Melody::getNextNote()
{
    list<Note*>::iterator it = playing;
    if (reverse)
    {
        if (it == melody.begin()) {
            it = melody.end();
        }
        return *(--it);
    }
    else
    {
        if (++it == melody.end()) {
            it = melody.begin();
        }
    }
    return *it;
}

bool Melody::isDrumming()
{
    return m_pDrum && m_pDrum->getDrumCount();
}

//--------------------------------------------------------------
int Melody::getPlayingState()
{
    int st = 0;
    if (!paused || Conditions::isActive(m_Id)) st |= MPLAY;
    if (isDrumming()) st |= MDRUM;
    if (inserted) st |= MINST;
    return st;
}

//--------------------------------------------------------------
void Melody::storeKey(int k)
{
    char c = (char)k;
    if (historyOn && (storedKeys.find(c) != string::npos))
    {
        keystory += c;
        if (c == 'z' && Melody::multiPause == false)
        {
            keystory += "[" + ofToString(getCurrentIdx()) + "]";
        }

    }
}

//--------------------------------------------------------------
int MidiInstruments::getProgram(int ch)
{
    ch--;
    int nm = ch >> 8;
    return m_Instrs[nm][ch & 0xff];
}

//--------------------------------------------------------------
string Melody::writeMidi()
{
    char fn[512];
    int cnt = snprintf(fn, sizeof fn, "melody_%i-%s.mid", m_Id, ofGetTimestampString("%Y%m%d-%H%M%S").c_str());
    fn[cnt] = 0;
    MidiFile mf;
    if (m_Ch0 > 0) {
        mf.progChange (MidiInstruments::getProgram(m_Ch0));
    }
    for (list<Note*>::iterator ns = melody.begin(); ns != melody.end(); ++ns)
    {
        Note *n = *ns;
        int nt = n->note0;
        Note *nxt = getNextNote();
        if (nxt->note0 < 0) {
            // the next note is pause. This one should be dominant.
            nt = dominant(nt);
        } else {
            nt = harmonize(nt);
        }

        float d = n->distance(centroid);
        float vol = 1;
        if (maxDist != minDist)
        {
            float d = n->distance(centroid);
            vol = 1 - m_dynamic * (maxDist - d) / (maxDist - minDist);
        }

        //cout << "Playing " << n->note << " vel=" << n->velo * vol << endl;

        vol *= n->velo;
        vol += volume;
        if (vol > 127) vol = 127;
        if (vol < 0) vol = 0;

        int ccTypes[] = {CC_NOTE, CC_ENVE, CC_VELO};
        for (int cc = 0; cc < sizeof(ccTypes) / sizeof(ccTypes[0]); ++cc)
        {
            if (ccFlags & ccTypes[cc])
            {
                int ch = getCh() < 0 ? 0 : getCh();
                vector<int> ccs = m_Midi->getControls(ch, ccTypes[cc]);
                for (int i = 0; i <ccs.size(); ++i)
                {
                    mf.controlChange( ccs[i], (float)n->pt.x / m_ImgWidth * 127);
                }
            }
        }

    // echoes are not stored.
        int nnn;
        float bend = 0;
        if(drums)
        {
            nnn = DrumKit::drumMelos[nt % DrumKit::numDrumMelos];
        }
        else
        {
            Scale::midiNoteToScaled(nt, nnn, bend);
            cout << bend << endl;
        }

        if (Scale::enabled())
        {
            mf.pitchBend(bend * 2048 + 2048);
        }
        mf.noteOn(0, nnn, (int) vol);
        mf.noteOff(n->dur, nnn);

    }

    mf.writeToFile (fn);
    return fn;
}

string Melody::getCCLabel(int kind)
{
    return m_Midi->getControlName(getCh(), kind);
}

int Melody::getCurrentIdx()
{
    int idx = 0;
    std::list<Note*>::iterator it = melody.begin();
    while (it != melody.end())
    {
        if (*it != *playing)
        {
            idx++;
            it++;
        }
        else
        {
            break;
        }
    }
    return idx;
}

void Melody::setCurrentIdx(int idx)
{
    int i = 0;
    for (playing = melody.begin(); playing != melody.end(); playing++)
    {
        if (idx == i++) break;
    }
}
