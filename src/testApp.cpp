#include "testApp.h"
#include "Analyzer.h"
#include "Harmonizer.h"
#include "Chord.h"
#include "AllThreads.h"
#include "Scale.h"
#include "glut.h"
#include "NoteMaster.h"

#include "Panning.h"
//#include "ofxFFMPEGVideoWriter.h"

Panning panning;

extern NoteMaster master;
//ofxFFMPEGVideoWriter videoWriter;
ofImage screenimg;

//--------------------------------------------------------------
class DrumPlayer : public ofThread {
    DrumKit &dk;
    Envelope &env;
    Poco::Mutex &mtx;
    testApp *ta;
    int &period;
    bool &paused;

public:
    DrumPlayer(Envelope &e, int &p, bool &pa, DrumKit &d, Poco::Mutex &mt, testApp *app) :
        env(e), period(p), paused(pa), dk(d), mtx(mt), ta(app)
    {
        startThread(false, false);
    }

    ~DrumPlayer() {
        waitForThread(true);
    }

    void threadedFunction() {
        cout << "DrumPlayer thread started" << endl;
        while (isThreadRunning()) {
            long t = period / NUM_BEATS;
            long t1 = ofGetElapsedTimeMillis();

            if (!paused)
            {
                mtx.lock();
                dk.tick(&env, ta->getDrumOffset(), period);
                mtx.unlock();
            }

            long dt = ofGetElapsedTimeMillis() - t1;
            if (dt < t)
            {
                ofSleepMillis(t - dt);
            }
        } // while
    } // threadedFunction
};

class MidiPlayer : public ofThread {
    Analyzer &az;
    Envelope &env;
    Controller &ctl;
    Poco::Mutex &mtx;
    testApp *ta;

    int &period;
    int &tick;
    bool &paused;
    long t0;
    int cnt;

public:
    MidiPlayer(Analyzer &a, Envelope &e, Controller &c, int &p,
               bool &pa, int &t, Poco::Mutex &mt, testApp *app) :
        az(a), env(e), ctl(c), period(p), paused(pa), tick(t), mtx(mt), ta(app)
    {
        t0 = ofGetElapsedTimeMillis();
        cnt = 1;
        startThread(false, false);
    }

    ~MidiPlayer() {
        waitForThread(true);
        long dt = ofGetElapsedTimeMillis() - t0;
        cout << (float) dt / cnt << endl;
    }

    void threadedFunction() {
        cout << "MidiPlayer thread started" << endl;
        char buf[100];
        while (isThreadRunning()) {
            long t1 = ofGetElapsedTimeMillis();
            cnt++;
            int no = -1;
            int synch = testApp::getSynchLevel(ofGetElapsedTimeMillis(), period);

            Note *cur = 0;
            if (!paused)
            {
                mtx.lock();
                if (ctl.getSoloMelody())
                {
                    Melody *m = ctl.getSoloMelody();
                    int ch =(m->getCh() > 0) ? m->getCh() : 1;
                    float v = env.getValue( period, m->getBeat() );
                    if (m->playNext(ch, v, tick, synch)) cur = m->current();
                }
                else
                {
                    list<Melody*>::iterator mit = az.m_Music.begin();
                    for (int ch = 1; mit != az.m_Music.end(); mit++, ch++)
                    {
                        Melody *m = *mit;
                        if ((ch % 16)== 10) ch++; // skip channel 10 which is assigned to drums
                        float v = env.getValue( period, m->getBeat() );
                        m->playNext((ch % 16) + 1, v, tick, synch);
                    }
                }
                mtx.unlock();
            }

            long dt = ofGetElapsedTimeMillis() - t1;
            if (dt < tick)
            {
                ofSleepMillis(tick - dt);
            }
        } // while
    } // threadedFunction
};

//--------------------------------------------------------------

static MidiPlayer *mp = 0;
static DrumPlayer *dp = 0;

//--------------------------------------------------------------
void errorExit(string e)
{
    cout << e << endl;
    cout << "Press any key.";
    getchar();
    exit(2);
}
//--------------------------------------------------------------
void testApp::setup()
{
    Poco::Net::initializeNetwork();

    exitAt = 0;
    mp = 0;
    ini.load("config.ini", true);

    string caption = "Buddha Orchestra ";
    m_appTitle = 0;

    string disc = ini.get("discovery", string("none"));
    string myIp;
    if (disc != "none") {
        m_appTitle = "kandinsky music groups";
        caption = "Kandinsky Music Groups ";
        myIp = " [" + Replicator::getThisIp()+ "]";
    }

    serial.init(ini);
    if (serial.isActive()) {
        m_appTitle = "kandinsky sound";
        caption = "Kandinsky Sound ";
    }

    cout << "BO " << __DATE__ << " " << __TIME__ << endl;
    ofSetWindowTitle(caption + __DATE__  + myIp);

    ofDisableArbTex();
    ofDisableTextureEdgeHack();
    ofEnableSmoothing();
    screen.allocate(ofGetScreenWidth() + 100, ofGetScreenHeight() + 100, GL_RGB);

    displayMode = DISP_MODE_NUMBERS;
    alpha = ini.get("alpha", 30);
    aspect = ini.get("aspect", false);
    breathx = ini.get("breathx", 0);
    breathy = ini.get("breathy", 0);

    Harmonizer::init(ini);

    bpm = ini.get("bpm", BPM);
    periodCtr.init(ini, "bpm", &bpm);
    period = 60000 / bpm;
    tick0 = ini.get("tick", TICK_MS);
    tickCtr.init(ini, "tick", &tick0);
    tick = tick0;
    Melody::arpeggioDelay = ofToFloat(ini.get("arpeggio", string("4")));
    arpeggioCtr.init(ini, "arpeggio", &Melody::arpeggioDelay);

    env.init(ini);
    sampler.init(ini);
    synth.init(ini);

    string drumkit = ini.get(string("drumkit"), string("35,38,49,52"));
    DrumKit::initDrums(drumkit);
    drumkit = ini.get(string("drums"), drumkit);
    DrumKit::initMelos(drumkit);

    avar = ini.get("avar", 100);
    pvar = ini.get("pvar", 1000);
    tick2drum = getIniFloat(ini, "tick2drum", "1.0");

    osc.init(ini);
    cout << "OSC setup done" << endl;
    replicator.init(this, ini);

    string image = ini.get("image", string("image1.jpg"));

    string gen = ini.get("generator", string("gravity"));
    if (gen == "all") Melody::gtype = Melody::ALL_POINTS;
    else if (gen == "vert") Melody::gtype = Melody::VERT_LINES;
    else if (gen == "gravity") Melody::gtype = Melody::GRAVITY;

    az.init(ini);
    ctl.init(ini);

    const char*img = imgFile[0] ? imgFile : image.c_str();
    char *cp = new char[strlen(img)];
    strcpy(cp, img);
    loadImage(cp);
    ctl.setPtrns(&ptrs);
    tab.setController(&ctl);
    panning.init(ini, &osc);

	// also, frame rate:
	ofSetFrameRate(25);

    mout.init(ini);

    // This outputs the various ports
    // and their respective IDs.
//    midiIn[0].listPorts();
    string midi = ini.get(string("midiin"), string("nano"));
    int idx = 0;
    for (int i=0; i<midiIn[0].getNumPorts(); ++i) {
        string nm = midiIn[0].getPortName(i);
        int p = nm.find(midi);
        if (p >= 0) {
            cout << "Using " << nm << endl;
            // Now open a port to whatever
            // port ID your MIDI controller is on.
            midiIn[idx].openPort(i);
            // Add this app as a listener.
            midiIn[idx++].addListener(this);
        } else {
            cout << "Not using " << nm << endl;
        }
    }

    if (idx)
    {
        midiBtns.init(ini, this);
    }

    cnt = 0;

    ptrs.init(ini);

    ampCtr.init(ini, "ampl", &adsr);
    attCtr.init(ini, "attack", &adsr);
    decCtr.init(ini, "decay", &adsr);
    susCtr.init(ini, "susl", &adsr);
    relCtr.init(ini, "release", &adsr);
    fmCtr.init(ini, "fm", &adsr);
    sensCtr.init(ini, "sense", &adsr);
    dynamCtr.init(ini, "dynam", &adsr);

    cmd.setApp(this);
    string play = ini.get("play", string(""));
    if (!play.empty()) cmd.openIn(play.c_str());

    string rec = ini.get("record", string(""));
    if (!rec.empty())
    {
        cmd.openOut(rec.c_str());
        cmd.image(img);
    }

    CHord::init(ini);
    loadScale(ini.get("scale", string("")));

    help = table = paused = false;
    escaping = 0;
	t0 = ofGetElapsedTimeMillis();

	mp = new MidiPlayer(az, env, ctl, period, paused, tick, mtx, this);
    dp = new DrumPlayer(env, period, paused, drumKit, mtx, this);

    gui.init(this, ini);
    gui.updateInstruments(ctl.getCurrentMelody());

    vector<string> ignored = ofSplitString(ini.get("ignore",
                                           string("103,42,38,81,9,87")), // g, *, &, Q, TAB, W
                                           ",", true, true);
    for (int i=0;i<ignored.size(); i++)
    {
        ignoredKeys.push_back(ofToInt(ignored[i]));
    }

    cmd.restored(ctl.restoreMelodies());
    webServer.init(this, ini);

    Conditions::init(ini);
    AllThreads::addThread(&master);

    ofSetEscapeQuitsApp(false);
}

//--------------------------------------------------------------
void testApp::update(){

    long now = ofGetElapsedTimeMillis();
    if (exitAt)
    {
        if ((now > exitAt) || !master.getCount())
        {
            ofExit();
        }
        return;
    }

    if (panning.getEnabled())
    {
        panning.update();
    }

    if (escaping)
    {
        if (now > escaping)
        {
            escaping = 0;
        }
    }

    if (!reload)
    {
        cmd.act();

        while (!commandQueue.empty())
        {
            string c = commandQueue.front();
//            cout << c << endl;
            cmd.act(c);
            mtx.lock();
            commandQueue.pop_front();
            mtx.unlock();
        }
    }

	if (ofGetElapsedTimeMillis() - t0 > tick) {
        t0 = ofGetElapsedTimeMillis();
        updatePeriod(cnt++);
	}

    if (!reload)
    {
        replicator.invoke();
    }

//    if(videoWriter.isInitialized()) {
//        screenimg.grabScreen(0, 0, ofGetViewportWidth(), ofGetViewportHeight());
//        videoWriter.addFrame((const uint8_t*)(screenimg.getPixels()));
//    }

	ofBackground(ofColor::black);
	AllThreads::cleanUp();
}

//--------------------------------------------------------------
void testApp::draw() {

    static const char * helpString =
        "UP,DOWN - previous/next melody\n"
        "LEFT,RIGHT - playing melody\n"
        "n - nearest to mouse\n"
        "f - toggle full velocity\n"
        "p - toggle pause/play\n"
        "F5 or P - pause all melodies\n"
        "F2/F3 - volume control\n"
        "F6/F7 - envelope period\n"
        "F8/F9 - tempo control (tick)\n"
        "F10 - change MIDI bank\n"
        "J, H - echo\n"
        "o - toggle solo mode\n"
        "b - play backwards\n"
        "m - merge\n"
        "s - split\n"
        "a - toggle chords\n"
        "y - use next pattern\n"
        "PgUp, PgDn, . - drums\n"
        "K,L,N,V - toggle CC for tracK,\n"
        "   enveLope, Note, Velocity\n"
        "(, ) - staccato\n"
        "i, j - transposition\n"
        "z, Z - add/remove pauses\n"
//        "g - toggle group mode\n"
//        "i - toggle image\n"
        "r - reduce number of notes\n"
        "w - toggle full screen mode";
    const int infoLines = 23;

	char reportString[255];

    int H = ofGetHeight();
    int W = ofGetWidth();
    if (H < 10 || W < 10) return; // nothing to do
    ofRectangle imgRect = getImageRect();

	ofPushStyle();

    float envalue = env.getValue(period, 0);
    bool blackMode = displayMode == DISP_MODE_FRAME || displayMode == DISP_MODE_GALLERY;

    if (panning.getEnabled() && !exitAt && !reload)
    {
        ofPushMatrix();
        float x, y;
        panning.getOffsets(x, y);
        ofPoint c = imgRect.getCenter();
        float scale = panning.getScale();
        ofTranslate( c.x - (x - 0.5) * imgRect.width * scale, c.y - (y - 0.5) * imgRect.height * scale);
        ofScale( scale, scale );
        ofTranslate( -c );
    }

    if (!blackMode)
    {
        ofSetColor(ofColor::black);
        ofRect(0,0,W,H);
    }
    ofSetColor(ofColor::white);
    if (displayMode == DISP_MODE_FRAME)
    {
        ofRect(imgRect.x-1, imgRect.y-1, imgRect.width+2, imgRect.height+2);
    }
    bool useAlpha = displayMode != DISP_MODE_NUMBERS && alpha != 0;
    int displayAlpha = useAlpha ? alpha : 255;
    if (useAlpha) {
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, displayAlpha);
        float dx = envalue * breathx;
        float dy = envalue * breathy;
        imgRect.x -= dx;
        imgRect.y -= dy;
        imgRect.width += dx * 2;
        imgRect.height += dy * 2;
    } else {
        ofDisableAlphaBlending();
    }
    screen.draw(imgRect.x, imgRect.y, imgRect.width, imgRect.height);

    if (blackMode)
    {
        ofSetColor(0, 0, 0, displayAlpha);
        ofRect(imgRect);
        env.draw(imgRect.x, imgRect.y + imgRect.height - 10, imgRect.width, 10);
    }
    else
    {
        srcImg.draw(imgRect);
        if (!exitAt)
        {
            ofSetColor(0, 0, 0, displayAlpha);
            ofRect(0, 0, W, 38);
        }
    }

    if (reload || exitAt) // we would be waiting on the lock() call anyway
    {
        ofSetHexColor(0xFFFFFF);
        string bye  = ini.get("bye", string(""));
        ofDrawBitmapString(bye, ( W - bye.size() * 8)/2, H-20);
        ofPopStyle();
        return;
    }

    if (paused)
    {
        ofSetHexColor(0xFFFFFF);
        ofDrawBitmapString("PAUSED - PRESS F5 TO CONTINUE", 60, 10);
    }

    bool drumming = false;
    float sx = (float) imgRect.width / srcImg.width;
    float sy = (float) imgRect.height / srcImg.height;
    mtx.lock();

    list<Melody*>::iterator it = az.m_Music.begin();
    for ( ; it != az.m_Music.end(); it++)
    {
        Melody *m = *it;
        Note *n = m->current();
        int col = 0x0000FF;
        ofPoint cp = m->getCenter();
        ofColor coc = m->getColour();
        float br = n->velo + m->getVolume();
        float durpos = (float) n->playing / (n->dur + 1);
        if (br > 127) br = 127;
        if (br < 0) br = 0;
        coc.setBrightness(br * 2);
        int r = 1;
        int radActive = (displayMode != DISP_MODE_NUMBERS) ? H / 20 * br / 127 * durpos + 3 : 6;
        if (ctl.getSoloMelody() == m)
        {
            col = 0xFF0000;
            if (help) notes.drawMelody(m, col, 0xFFFFFF);
            r = radActive;
        }
        else if (ctl.isSelected(m))
        {
            col = (displayMode != DISP_MODE_NUMBERS) ? coc.getHex() : 0xFFFFFF;
            r = radActive;
            if (help) notes.drawMelody(m, col, 0xFF0000);
            col = coc.getHex();
        }
        else if (m->getPlayingState() & (MPLAY | MDRUM))
        {
            col = coc.getHex();
            r = radActive;
        }
        coc.setBrightness(blackMode ? 255 : 127);
        if (m->display || (m->getPlayingState() & MPLAY)) m->draw(imgRect.x, imgRect.y, sx, sy,
                                                                  displayMode == DISP_MODE_NUMBERS,
                                                                  blackMode, coc);

        float relt = (float)(ofGetElapsedTimeMillis() % period) / period;
        float a = relt * M_PI * 2;
        float si = sinf(a);
        float co = cosf(a);

        ofPoint &p = n->pt;
        float x = p.x * sx + imgRect.x;
        float y = p.y * sy + imgRect.y;
        // don't show current notes in blue if the melody is NOT playing
        if ((col != 0x0000FF) || (displayMode == DISP_MODE_NUMBERS))
        {
            Note *nextNote = m->getNextNote();
            float nx = nextNote->pt.x * sx + imgRect.x;
            float ny = nextNote->pt.y * sy + imgRect.y;
            if (n->dur && (n->playing >= 0))
            {
                x = nx - (nx - x) * durpos;
                y = ny - (ny - y) * durpos;
            }

            if (n->note0 < 0)
            {
                // let's make pauses hardly visible
                r = 3;
            }

            if (r > 8)
            {
                ofSetLineWidth(6);
                ofNoFill();
            }

            coc.setBrightness(200 + 50 * si);
            ofSetColor(coc, 50);
            ofCircle(x, y, r);

            ofFill();
            ofSetLineWidth(1);
            if (r > 6 && m->getCCflag(CC_VELO) || m->getCCflag(CC_NOTE) || m->getCCflag(CC_ENVE))
            {
                int ch = m->getCh() & 0xF;
                float rr = (displayMode != DISP_MODE_NUMBERS) ? r / (ch % 4 + 1) : 2;
                float rrr = (displayMode != DISP_MODE_NUMBERS) ? r / (ch / 4 + 1) : 4;
                coc.setBrightness(50);
                ofSetColor(coc, 80);
                ofCircle(x - si * rrr, y + co * rrr, rr);
            }
        }

        coc.setBrightness(200);
        ofSetColor(coc);
        int nn = n->playing > 0 ? n->note : n->note0;
        if (displayMode == DISP_MODE_NUMBERS)
        {
            sprintf(reportString, "(%i) %s d %i/%i", m->m_Id, Note::noteString(nn), n->dur, n->playing);
            ofDrawBitmapString(reportString, x, y + 10);
        }
        if (m->m_pDrum && m->m_pDrum->getDrumBit())
        {
            float centrx = cp.x * sx + imgRect.x;
            float centry = cp.y * sy + imgRect.y;

            if (m->getCh() < 0 || (m->getCh() & 0xF) == 10)
            {
                ofRect(centrx - 10, centry - 10, 20, 20);
            }
            else
            {
                ofCircle(centrx, centry, 15);
            }
        }
        if (m->isDrumming()) {
            drumming = true;
        }
    }

    if (useAlpha) {
        ofDisableAlphaBlending();
    }

    if (drumming && !blackMode)
    {
        ofSetHexColor(0xFFFFFF);
        ofCircle(drumKit.getPosition() * imgRect.width + imgRect.x, H - 5, 3);
    }

    if (!paused && !blackMode)
    {
        ofSetHexColor(0xAAAAAA);
        float dur = ctl.getCurrentMelody()->getDuration() * tick0 / 1000.0;
        ofDrawBitmapString(ctl.info(dur), 60, 10);

        sprintf(reportString, "bpm=%i tick=%i/%ims arp=%.0f [%s]",
                bpm, tick, tick0, Melody::arpeggioDelay, CHord::getInfo().c_str());
        ofDrawBitmapString(reportString, 40, 30);
        if (env.edit)
        {
            int qua = ctl.getCurrentMelody()->quality;
            env.draw(20, 50, 60, 40, qua);
        }
    }

// follow the mouse cursor
    if (!blackMode)
    {
        float relx = ofMap(mouseX, imgRect.getMinX(), imgRect.getMaxX(), 0, 1, true);
        float rely = ofMap(mouseY, imgRect.getMinY(), imgRect.getMaxY(), 0, 1, true);
        Melody *nearest = *ctl.nearestMelody(relx, rely);
        ofPoint c = nearest->getCenter();
        float cx = c.x * sx + imgRect.x;
        float cy = c.y * sy + imgRect.y;
        ofSetHexColor(0xFFDD00);
        const int D = 3;
        ofLine(cx - D, cy, cx + D, cy);
        ofLine(cx, cy - D, cx, cy + D);
    }
    mtx.unlock();

    long t = ofGetElapsedTimeMillis();
    int sec = t / 1000;
    int msec = (t % 1000) / 100;
    sprintf(reportString, "%s [%i] %3i:%02i.%1i {%3i}", mout.getMidiName().c_str(),
            mout.getMidiNo(), sec / 60, sec % 60, msec, master.getCount());
    ofSetHexColor(0x777777);
    ofDrawBitmapString(reportString, W - strlen(reportString) * 8 - 4, 30);

    if (m_appTitle)
    {
        int px = imgRect.x + imgRect.width - strlen(m_appTitle) * 8 - 4;
        int py = imgRect.y + imgRect.height - 15;
        ofSetColor(0);
        ofDrawBitmapString(m_appTitle, px, py);
        ofSetColor(196);
        ofDrawBitmapString(m_appTitle, px - 1, py - 1);
    }

    synth.draw(41, 15);

    ofSetColor(int(envalue * 255));
    ofRect(41, 1, 12, 12);

    if (table)
    {
        string insts[17];
        char b[1024];
        const char *instKeys = "0123456789ABCDEFG";
        int nm = mout.getMidiNo();
        int mx = 0;
        int cnt = 16;
        for (int ch = 0; ch <= cnt; ++ch)
        {
            const char *nam = MidiInstruments::getName(nm, ch, ch == 10);
            if (!*nam) { cnt = ch - 1; break; }
            int len = sprintf(b, "%c %s", instKeys[ch], nam);
            insts[ch] = b;
            if (len > mx) mx = len;
        }

        ofSetHexColor(0x222222);
        ofRect(18, 42, mx * 8 + 4, (cnt + 1) * 15);
        ofSetHexColor(0xFFFFFF);
        for (int ch = 0; ch <= cnt; ++ch)
        {
            ofDrawBitmapString(insts[ch], 20, 55 + ch * 15);
        }
    }

    tab.draw(W, H);

    screen.loadScreenData(imgRect.x, imgRect.y, imgRect.width, imgRect.height);

    if (panning.getEnabled())
    {
        ofPopMatrix();
        ofSetColor(ofColor::white);
        ofRectangle r(0,0,80,60);
        panning.draw(r, srcImg);
    }

    string s = info.getInfo();
    if (s.size())
    {
        ofSetColor(info.getColor());
        ofDrawBitmapString(s, 300, 30);
    }

	ofPopStyle();
}

//--------------------------------------------------------------
bool isKeyTransmitted(int key)
{
    if (strchr("aijnJHTw./", key)) return false;
    static int nt[] = {
        257, // F1
        258, 259, // F2, F3
        262, 263, // F6, F7
        264, 265, // F8, F9
        266, // F10
        267, 268, // F11, F12
        362, 363, // home, end
        359, 357, // down, up
        356, 358, // Left, Right
        9 // Tab
    };
    for (int i=0; i<sizeof(nt)/sizeof(nt[0]); ++i)
    {
        if (nt[i] == key) return false;
    }
    return true;
}

void testApp::keyPressed(int key){
    Melody *m = ctl.getCurrentMelody();

    bool wasTable = table;
    bool remote = false;
    // any key clears the instrument table except MIDI bank switch
    if (key != 266)
    {
        table = false;
    }

    if (key != 27)
    {
        if (key & 0x8000)
        {
            key &= 0x7FFF;
            remote = true;
        }
        else
        {
            if (strchr("srmzZbdfRYuLVNKx:;0123456789ABCDEFG'\"#", key) ||
                key == 364 ) // insert
            {
                replicator.key(key, m->m_Id);
            }
            else if (key == 'p')
            {
                bool pl = (m->getPlayingState() & MPLAY) != 0;
                if (pl) replicator.pause(m->m_Id);
                else replicator.play(m->m_Id);
            }
            // the keystrokes which are not transmitted.
            // what is transmitted is the end state of the operation
            else if (isKeyTransmitted(key))
            {
                replicator.key(key);
            }
        }
    }

    cmd.key(key);
    if ((glutGetModifiers() & GLUT_ACTIVE_SHIFT) && panning.handle(key))
    {
        // TODO replicate
        return;
    }

    if (escaping)
    {
        if (CHord::keypressed(key))
        {
            escaping = 0;
            info.publish("");
            return;
        }
    }

    string s;

    switch (key)
    {
    case 'P': // stops melodies and mutes all notes
        if ((paused = !paused)) {
            ctl.stopAll();
            MidiOut::stopAllNotes();
        }
        break;
    case 261: // F5
        if ((paused = !paused)) ctl.stopAll();
        break;
    case 262: // F6
        adjustPeriod(-5, !remote);
        break;
    case 263: // F7
        adjustPeriod(5, !remote);
        break;
    case 264: // F8
        adjustTick(-5, !remote);
        break;
    case 265: // F9
        adjustTick(5, !remote);
        break;
    case 266: // F10
        MidiOut::toggleMidi();
        s = mout.getMidiName() + "=" + ofToString(mout.getMidiNo());
        if (!remote) replicator.svalue(VALUE_MIDI, s, -1);
        cmd.svalue(VALUE_MIDI, s, -1);
        break;
    case 'h':
    case 257: // F1
        help = !help;
        break;
    case 't': table = !wasTable; break;
    case 'e': env.edit = !env.edit; break;
    case 'w': ofToggleFullscreen(); break;
    case 27: // ESC
        if (panning.getEnabled())
        {
            panning.setEnabled(false);
            sampler.enablePanning(false);
            gui.showGear(true);
        }
        else
        {
            escaping = ofGetElapsedTimeMillis() + 10000;
            string msg = "press Enter to exit";
            string range = CHord::getRange();
            if (!range.empty())
            {
                msg += ", or [";
                msg += range;
                msg += "] to change chord matrix";
            }
            info.publish(msg, ofColor::aquamarine, 10000);
        }
        return;
    case 13: // enter
        if (escaping)
        {
            startClosing();
        }
        else
        {
            panning.setEnabled(!panning.getEnabled());
            sampler.enablePanning(panning.getEnabled());
            gui.showGear(!panning.getEnabled());
        }
        return;
    case 'U':
        mtx.lock();
        az.addPauses();
        mtx.unlock();
        break;
    case 360: // PgUp
        drumKit.incMelody(m);
        if (!remote) replicator.value(VALUE_DRUM_COUNT, m->m_pDrum->getDrumCount(), m->m_Id);
        cmd.value(VALUE_DRUM_COUNT, m->m_pDrum->getDrumCount(), m->m_Id);
        break;
    case 361: // PgDn
        drumKit.decMelody(m);
        if (m->m_pDrum)
        {
            if (!remote) replicator.value(VALUE_DRUM_COUNT, m->m_pDrum->getDrumCount(), m->m_Id);
            cmd.value(VALUE_DRUM_COUNT, m->m_pDrum->getDrumCount(), m->m_Id);
        }
        break;
    case '.': // dot
        drumKit.changeDrum(m);
        if (!remote) replicator.value(VALUE_DRUM_TYPE, m->m_pDrum->getDrumType(), m->m_Id);
        cmd.value(VALUE_DRUM_TYPE, m->m_pDrum->getDrumType(), m->m_Id);
        break;
    case 45: // -
        // TODO check if these - and + are useful. if yes, add replication and GUI setting
        drumKit.minusDrum(m);
//            replicator.value(VALUE_DRUM_COUNT, m->m_pDrum->getDrumCount(), m->m_Id);
//            cmd.value(VALUE_DRUM_COUNT, m->m_pDrum->getDrumCount(), m->m_Id);
        break;
    case 43: // +
        drumKit.plusDrum(m);
//            replicator.value(VALUE_DRUM_COUNT, m->m_pDrum->getDrumCount(), m->m_Id);
//            cmd.value(VALUE_DRUM_COUNT, m->m_pDrum->getDrumCount(), m->m_Id);
        break;
    case 258: // F2
        adjustVolume(m, 5, !remote);
        break;
    case 259: // F3
        adjustVolume(m, -5, !remote);
        break;
    case '[':
        Melody::arpeggioDelay = ofClamp(Melody::arpeggioDelay - 1, 0, 100);
        break;
    case ']':
        Melody::arpeggioDelay = ofClamp(Melody::arpeggioDelay + 1, 0, 100);
        break;
    case 'S':
        ctl.dumpMelodies("melodies");
        ctl.dumpRoutes((string("routes-") + ofGetTimestampString("%Y%m%d-%H%M%S") + ".txt").c_str(), imgFile);
        break;
    case '<':
        if (MidiInstruments::m_Effect > 0) MidiInstruments::m_Effect--;
        break;
    case '>':
        if (MidiInstruments::m_Effect < 127) MidiInstruments::m_Effect++;
        break;
    case 'R':
        m = m->copy(az.m_NextId++, 4);
        m->generate(srcImg.width, srcImg.height, 0, ctl.getND());
        az.m_Music.push_back(m);
        ctl.control(363); // end
        break;
    case 'g':
        gui.toggleVisible();
        break;
    case '*':
        gui.showImageDir();
        break;
    case '&':
        gui.showMeloDir();
        break;
    case 'W':
        gui.toggleNetwork(replicator);
        break;
    case 'Q':
        gui.assignRoute(m->m_Id, replicator);
        break;
    case '!':
        ++displayMode;
        if (displayMode == DISP_MODE_LAST ||
            (!aspect && (displayMode == DISP_MODE_FRAME)))
        {
            displayMode = DISP_MODE_NUMBERS;
        }
        // if (!remote) replicator.value(VALUE_DISPLAY, displayMode, -1);
        glutSetCursor(displayMode < DISP_MODE_GALLERY ? GLUT_CURSOR_CROSSHAIR : GLUT_CURSOR_NONE);
        gui.showGear(displayMode < DISP_MODE_GALLERY);
        break;
    case 'O':
        saveBocConfig();
        break;
    case '/':
        env.reset();
        break;
    case '?':
        AllThreads::dump();
        break;
    case '\'':
        mtx.lock();
        Harmonizer::processMelody(getMelody(getCurrentMelody()), false);
        mtx.unlock();
        break;
    case '\"':
        mtx.lock();
        Harmonizer::processMelody(getMelody(getCurrentMelody()), true);
        mtx.unlock();
        break;
//    case '#':
//        if (videoWriter.isInitialized())
//        {
//            videoWriter.close();
//        }
//        else
//        {
//            screenimg.allocate(ofGetViewportWidth(), ofGetViewportHeight(), OF_IMAGE_COLOR);
//            videoWriter.setup("output.mpg", ofGetViewportWidth(), ofGetViewportHeight());
//        }

//        break;
    default:
        if (key < 9  || (key > 9 && key <=26)) // ctl-a, ctl-b, ... ctl-z - excepty ctl-I which is TAB
        {
            info.publish(mout.sendCustomMidi(key, ini));
        }
        else
        {
            mtx.lock();
            tab.control(key);
            mtx.unlock();
            if (!remote) replicator.select(ctl.getCurrentMelody()->m_Id);
        }
    }

    gui.updateGui();

    // these are handled inside Melody::control - we need to replicate the result values
    switch(key)
    {
        case 267: // F11
        case 268: // F12
            if (!remote) replicator.value(VALUE_DYNAMIC, m->getDynamic(), m->m_Id);
            cmd.value(VALUE_DYNAMIC, m->getDynamic(), m->m_Id);
            break;
        case 'i':
        case 'j':
            if (!remote) replicator.value(VALUE_TRANSPOSE, m->getCenter().y, m->m_Id);
            cmd.value(VALUE_TRANSPOSE, m->getCenter().y, m->m_Id);
            break;
        case 'H':
        case 'J':
            if (!remote) replicator.value(VALUE_ECHO, m->getEcho(), m->m_Id);
            cmd.value(VALUE_ECHO, m->getEcho(), m->m_Id);
            break;
        case '(':
        case ')':
            if (!remote) replicator.value(VALUE_DURATION, m->getLegato(), m->m_Id);
            cmd.value(VALUE_DURATION, m->getLegato(), m->m_Id);
            break;
        case 'T':
            if (!remote) replicator.svalue(VALUE_DEST, m->getDestName(), m->m_Id);
            cmd.svalue(VALUE_DEST, m->getDestName(), m->m_Id);
            break;
        case '0': case '1': case '2': case '3':
        case '4': case '5': case '6': case '7':
        case '8': case '9': case 'A': case 'B':
        case 'C': case 'D': case 'E': case 'F': case 'G':
            if (!remote) replicator.value(VALUE_MIDI_CH, m->getCh(), m->m_Id);
            cmd.value(VALUE_MIDI_CH, m->getCh(), m->m_Id);
            break;
        case 'a':
            if (!remote) replicator.svalue(VALUE_CHORD, m->getChordName(), m->m_Id);
            cmd.svalue(VALUE_CHORD, m->getChordName(), m->m_Id);
            break;
    }
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
    int h = ofGetHeight();
    int w = ofGetWidth();
    if (!w || !h) return;

    if (gui.isVisible())
    {
        return;
    }

    Melody *m = ctl.getCurrentMelody();
    if (m)
    {
        if (button)
        {
            m->moveBack();
        }
        else
        {
            float dx = (x - lastPoint.x) * srcImg.width / w;
            float dy = (y - lastPoint.y) * srcImg.height / h;
            lastPoint = ofPoint(x, y);
            ofPoint p(m->getCenter().x + dx, m->getCenter().y + dy);
            m->moveTo(p);
            drumKit.updateMelody(m);
            replicator.value(VALUE_TRANSPOSE, m->getCenter().y, m->m_Id);
            replicator.value(VALUE_POSITION_X, m->getCenter().x, m->m_Id);
            cmd.value(VALUE_TRANSPOSE, m->getCenter().y, m->m_Id);
            cmd.value(VALUE_POSITION_X, m->getCenter().x, m->m_Id);
        }
    }
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
    int h = ofGetHeight();
    int w = ofGetWidth();
    if (!w || !h) return;

    lastPoint = ofPoint(x, y);
    bool dblClck = ofGetElapsedTimeMillis() - lastClick < doubleClick;
    lastClick = ofGetElapsedTimeMillis();

    if (gui.isHit(x, y))
    {
        return;
    }

//    float sx = (float) w / srcImg.width;
//    float sy = (float) h / srcImg.height;
    ofRectangle imgRect = getImageRect();
    float relx = ofMap(x, imgRect.getMinX(), imgRect.getMaxX(), 0, 1, true);
    float rely = ofMap(y, imgRect.getMinY(), imgRect.getMaxY(), 0, 1, true);
    int imgx = relx * srcImg.width;
    int imgy = rely * srcImg.height;

    if (button) {
        cout << "x=" << relx << " y=" << rely <<endl;
        ofColor col = srcImg.getColor(imgx, imgy);
        int c = col.getHex();
        int r = (c & 0xff0000) >> 16;
        int g = (c & 0x00ff00) >> 8;
        int b = c & 0x0000ff;
        int amp = col.getLightness() / 2.55; // 255 -> 100
        cout << "r=" << r << " g=" << g << " b=" << b << " amp=" << amp << endl;
        // send the colour as ADSR control
        // amp, a, d, s, r, e
        // amp, s, e - 0..100
        Melody *m = ctl.getCurrentMelody();
        m->sendADSR(amp, r * 10, g * 10, -1, b * 10, -1);
    } else {
        // left click controls current melody pointer
        if (dblClck) {
            mouseDoubleClick(relx, rely);
        } else {

            ctl.mouse(relx, rely);
            replicator.goTo(imgx, imgy);
            cmd.goTo(imgx, imgy);
        }
        gui.updateGui();
    }
}

//--------------------------------------------------------------
void testApp::mouseDoubleClick(float x, float y) {
    Melody *m = ctl.getCurrentMelody();
    ctl.mouseDbl(x, y);
    Melody *mm = ctl.getCurrentMelody();
cout << x << " " << y << " " << mm->m_Id << endl;
    if (m != mm)
    {
        replicator.select(mm->m_Id);
        cmd.select(mm->m_Id);
        gui.updateInstruments(mm);
    }
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
    lastClick = ofGetElapsedTimeMillis();
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::goTo(float x, float y){
    int h = ofGetHeight();
    int w = ofGetWidth();
    if (!w || !h) return;

    float sx = (float) w / srcImg.width;
    float sy = (float) h / srcImg.height;
    mousePressed(x * sx, y * sy, 0);
}

//--------------------------------------------------------------
void testApp::select(int id){
    ctl.setCurrentMelody(id);
    replicator.select(id);
    cmd.select(id);
}

//--------------------------------------------------------------
void testApp::loadScale(string fn, bool repl)
{
    if (fn.length() == 0) return;
    if (!Scale::loadScale(fn.c_str()))
    {
        errorExit(string("Cannot load ") + fn);
    }
    info.publish("scale: " + fn);
    cmd.scale(fn);
    if (repl)
    {
        replicator.scale(fn);
    }
}

//--------------------------------------------------------------
void testApp::loadImage(string fn, bool repl){
    if (fn.length() == 0) return;

    if (!srcImg.loadImage(fn))
    {
        errorExit(string("Cannot load ") + fn);
    }
    cout << "image " << fn << endl;
    info.publish("image: " + fn);
    startOsc(fn.c_str());

    mtx.lock();
    reload = true;
    int cnt = az.analyze(srcImg);
    if (cnt)
    {
        info.publish("number of melodies: " + ofToString(cnt));

        drumKit.clear();
        az.generate(ctl.getND());
        tab.setMusic(&az.m_Music);
        ctl.setMusic(&az.m_Music);

        Harmonizer::clearHistogram();
        list<Melody*>::iterator it = az.m_Music.begin();
        for (int i=0; it != az.m_Music.end(); it++, i++)
        {
            Melody *m = *it;
            Harmonizer::addHistogram(m);
            m->setMoutOscSerial(&mout, &osc, &serial, &sampler, &synth);
        }
        Harmonizer::processHistogram();
        Harmonizer::dumpHistogram();
    }
    else
    {
        mtx.unlock();
        errorExit( "FATAL: cannot find any melodies!" );
    }

    ctl.setImage(&srcImg, this);
    reload = false;
    mtx.unlock();

    cmd.image(fn);
    if (repl)
    {
        replicator.image(fn);
    }
    strcpy(imgFile, fn.c_str());
}

//--------------------------------------------------------------
void testApp::loadStoredMelodies(string fn){
    cmd.restored(ctl.restoreMelodies(fn.c_str()));
}

//--------------------------------------------------------------
float testApp::getIniFloat(const char *key, const char *def) {
    return getIniFloat(ini, key, def);
}

//--------------------------------------------------------------
float testApp::getIniFloat(ofxIniSettings &settings, const char *key, const char *def) {
    string v = settings.get(string(key), string (def));
    return atof(v.c_str());
}

//--------------------------------------------------------------
void testApp::updatePeriod(int n) {
    tick = tick0 + sin(n * 2 * M_PI / pvar * tick0) * (avar * tick0 / 100.0);
}

//--------------------------------------------------------------
float testApp::getDrumOffset()
{
    return avar ? ((tick - tick0) / (avar * tick0 / 100.0) + 1) * tick2drum : 0;
}

//--------------------------------------------------------------
void testApp::newMidiMessage(ofxMidiMessage& ev)
{
    bool remote = ev.portName == "BO";
    bool updgui = false;
    if (reload)
    {
        string msg;
        if (ev.status == MIDI_CONTROL_CHANGE)
        {
            msg = " M " + ofToString(ev.control) + " " + ofToString(ev.value);
        }
        else if (ev.status == MIDI_NOTE_OFF || ev.status == MIDI_NOTE_ON)
        {
            msg = " N " + ofToString(ev.channel) + " " + ofToString(ev.status) + " " + ofToString(ev.pitch) + " " + ofToString(ev.velocity);
        }
        if (msg != "") commandQueue.push_back(msg);
        return;
    }

    cmd.midi(ev);
    float v;

    Melody *m = ctl.getCurrentMelody();
    int ch = m->getCh();
    if (ch <= 0) ch = 1;

    switch (ev.status)
    {
        case MIDI_NOTE_OFF:
        case MIDI_NOTE_ON:
            m->midiThrough(ch, ev.status, ev.pitch, ev.velocity);
            if (ev.portNum >=0)
            {
                replicator.midiNote(ev.status == MIDI_NOTE_ON, ch, ev.pitch, ev.velocity);
            }
            break;

        case MIDI_CONTROL_CHANGE:
            if (ev.portNum >=0 )
            {
                replicator.midiControl(ev.control, ev.value);
            }
            if (tickCtr.handle(ev) || ctl.handle(ev, env.getSlider(ev.control), env.edit)
                || (!env.edit && periodCtr.handle(ev))
                || midiBtns.handle(ev)) {
                    updgui = true;
            }
            else if (env.edit && env.handle(ev))
            {
                for (int i = 0; i < NSLIDERS; i++)
                {
                    gui.envelSliders[i]->setValue(env.getSliderValue(i));
                }
            }
            else if (!Conditions::handle(ev)) {
                    int par = -1;
                    if (ampCtr.handle(ev)) {
                        updgui = true;
                        par = 0;
                    } else if (attCtr.handle(ev)) {
                        par = 1;
                    } else if (decCtr.handle(ev)) {
                        par = 2;
                    } else if (susCtr.handle(ev)) {
                        par = 3;
                    } else if (relCtr.handle(ev)) {
                        par = 4;
                    } else if (fmCtr.handle(ev)) {
                        par = 5;
                    } else if (sensCtr.handle(ev)) {
                        par = 6;
                    } else if (dynamCtr.handle(ev)) {
                        updgui = true;
                        par = 7;
                    } else if (arpeggioCtr.handle(ev)) {
                        updgui = true;
                    }
                    else {
                        m->midiThrough(ch, ev.status, ev.control, ev.value);
                        cout << "CC through ch=" << ev.channel
                            << " port=" << ev.portNum
                            << " control=" << ev.control
                            << " value=" << ev.value
                            << " status=" << ev.status
                            << endl;
                    }

                    if (par >= 0 ) {

                        switch (par) {
                        case 0:
                            // volume adjustment - need to change range from 0..100 to -127..127
                            m->setVolume(v = adsr * 1.27 * 2 - 127);
                            if (ev.portNum >=0 )
                            {
                                replicator.value(VALUE_VOLUME, v, m->m_Id);
                            }
                            cmd.value(VALUE_VOLUME, v, m->m_Id);
                            break;

                        case 7: // dynamic
                            m->setDynamic(v = adsr / 127.0f);
                            if (ev.portNum >=0 )
                            {
                                replicator.value(VALUE_DYNAMIC, v, m->m_Id);
                            }
                            cmd.value(VALUE_DYNAMIC, v, m->m_Id);
                            break;
                        default:
                            m->sendADSR(par, adsr);
                        }
                    }
                }
                period = 60000 / bpm;
                if (updgui) gui.updateGui(false);
                break;
            default:
                cout << "MIDI ch=" << ev.channel
                    << " port=" << ev.portNum
                    << " control=" << ev.control
                    << " value=" << ev.value
                    << " status=" << ev.status
                    << endl;

//#define    MIDI_POLY_PRESSURE        160
//#define    MIDI_PROGRAM_CHANGE       192
//#define    MIDI_CHANNEL_PRESSURE     208
//#define    MIDI_PITCH_BEND           224
    }

}

//--------------------------------------------------------------
bool MidiControl::init(ofxIniSettings &ini, string id) {
    mn = ini.get(id + "Mn", 0);
    mx = ini.get(id + "Mx", 127);
    ch = ini.get(id + "Ch", -1);
    nm = id;
    return ch != -1;
}

//--------------------------------------------------------------
void MidiControl::init(ofxIniSettings &ini, string id, int *var) {
    if (init(ini, id)) v = var;
}

//--------------------------------------------------------------
void MidiControl::init(ofxIniSettings &ini, string id, float *var) {
    if (init(ini, id)) fv = var;
}

//--------------------------------------------------------------
void MidiControl::validate(int &v)
{
    if (v < mn) v = mn;
    if (v > mx) v = mx;
}

//--------------------------------------------------------------
bool MidiControl::handle(ofxMidiMessage& e) {
    if (e.control != ch) return false;
    int val = e.value;
    float res = float(val) * (mx - mn) / 127 + mn;
    // cout << nm << " " << res << endl;
    if (v) *v = res;
    else if (fv) *fv = res;
    else return false;

    return true;
}

int testApp::getSynchLevel(long t, int period)
{
    int m = (t % period) * 64 / period;
    int slev = 0;
    for (int i = 8; i; i /= 2)
    {
        if ((m % i) == 0) slev++;
    }
    return slev;
}

void MidiKeyMapper::init(ofxIniSettings &ini, testApp *ta)
{
    app = ta;
    playBtn = ini.get("playBtn", 45);
    stopBtn = ini.get("stopBtn", 46);
    instrKnob = ini.get("instrKnob", 14);
    for (int i=0; i<255; ++i) {
        string key = "ccmap" + ofToString(i);
        string cmd = ini.get(key, string(""));
        if (cmd.size()) {
            ccmap[i] = cmd;
        }
    }
}

bool MidiKeyMapper::handle(ofxMidiMessage &ev)
{
    if (ev.control == playBtn) {
        if (ev.value) app->keyPressed('p');
        return true;
    }
    else if (ev.control == stopBtn) {
        app->paused = ev.value ? true : false;
        return true;
    }
    else if (ev.control == instrKnob) {
        int ch = ev.value / 8; // 0..15
        ch = "0123456789ABCDEF"[ch];
//        cout << "b2=" << ev.byteTwo << " ch=" << ch << endl;
        app->keyPressed(ch);
        return true;
    }

    map<int, string>::iterator it = ccmap.find(ev.control);
    if (it == ccmap.end())
    {
        // not found
        return false;
    }

    string cmd = it->second;

    if (cmd.find("$table") == 0){
        app->table = ev.value ? true : false;
        return true;
    }
    else if (cmd.find("$envelop") == 0){
        app->env.edit = ev.value ? true : false;
        return true;
    }
    else if (cmd.find("$panx") == 0){
        panning.setTargetX( ev.value / 127.0 );
        return true;
    }
    else if (cmd.find("$pany") == 0){
        panning.setTargetY( ev.value / 127.0 );
        return true;
    }
    if (ev.value) {
        vector<string> cmds = ofSplitString(cmd, ";", true, true);
        app->mtx.lock();
        for (int i = 0; i < cmds.size(); i++) {
            app->commandQueue.push_back(cmds[i]);
        }
        app->mtx.unlock();
    }
    return true;
}

void testApp::adjustVolume(Melody *m, int v, bool repl)
{
    int vol = m->getVolume() + v;
    if (vol > 127) vol = 127;
    if (vol < -127) vol = -127;
    m->setVolume(vol);
    if (repl) replicator.value(VALUE_VOLUME, vol, m->m_Id);
    cmd.value(VALUE_VOLUME, vol, m->m_Id);
}

void testApp::adjustTick(int v, bool repl)
{
    tick0 += v;
    tickCtr.validate(tick0);
    if (repl) replicator.value(VALUE_TICK, tick0, -1);
    cmd.value(VALUE_TICK, tick0, -1);
}

void testApp::adjustPeriod(int v, bool repl)
{
    bpm += v;
    periodCtr.validate(bpm);
    period = 60000 / bpm;
    if (repl) replicator.value(VALUE_BPM, bpm, -1);
    cmd.value(VALUE_BPM, bpm, -1);
}

void testApp::sendCurrentRouteTo(const char *dest)
{
    Melody *me = ctl.getCurrentMelody();
    ofColor col = me->getColour();
    std::list<ofPoint*> points = me->getPoints();

    replicator.sendRouteHeader(dest, imgFile, srcImg.width, srcImg.height, me->m_Id, me->getCenter().x, me->getCenter().y, col.getHex(), points.size());

    for (std::list<ofPoint*>::iterator p = points.begin(); p != points.end(); ++p)
    {
        ofPoint *po = *p;
        replicator.sendRoutePoint(dest, me->m_Id, po->x, po->y);
    }

}

void testApp::saveBocConfig()
{
    int port = ini.get("oscListen", 0);
    if (!port)
    {
        info.publish("the BO is not configured for remote control.", ofColor::aqua);
        return;
    }
    string routeFile = ofGetTimestampString("routes-""%Y%m%d-%H%M%S.txt");
    ctl.dumpRoutes(routeFile.c_str(), imgFile);
    ofstream of("bconfig.ini", ios::out);
    of << "routes=" << routeFile << endl;
    of << "image=" << imgFile << endl;
    string chords;
    for (int i = 0; ; i++)
    {
        string nm = Melody::getChordName(i);
        if (nm.length() == 0) break;
        if (i) chords += ',';
        chords += nm;
    }
    of << "chords=" << chords << endl;
    string dest;
    for (int i = 0; ; i++)
    {
        string nm = Melody::getDestName(i);
        if (nm.length() == 0) break;
        if (i) dest += ',';
        dest += nm;
    }
    of << "dests=" << dest << endl;

    string drums;
    for (int i = 0; i < DrumKit::numDrumTypes; i++)
    {
        char buf[100];
        int t = DrumKit::getDrumType(i);
        if (i) drums += ",";
#ifdef DRUM_NAMES
        drums += MidiInstruments::getName(0, t, true);
#else
        drums += ofToString(t);
#endif
    }
    of << "# All possible drum types, see http://en.wikipedia.org/wiki/General_MIDI#Percussion" << endl;
    of << "drumkit=" << drums << endl;
    of << "# midi devices" << endl;
    string midis;
    int num;
    for (num = 0; ; num++)
    {
        string nm = mout.getMidiName(num);
        if (nm.length() == 0) break;
        if (num) midis += ",";
        midis += nm;
    }
    of << "midis=" << midis << endl;

    of << "# All instruments are listed here http://en.wikipedia.org/wiki/General_MIDI#Melodic_sounds" << endl;
    string instruments;
    for(int n=0; n < num; ++n)
    {
        string key = "instruments";
        if (n) key += ofToString(n);
        for(int i=0; i < 16; ++i)
        {
            int ch = (n << 8) + i + 1;
            int p = MidiInstruments::getProgram(ch);
            if (p<0) break;
            string nm = ofToString(p);
            if (i) instruments += ",";
            instruments += nm;
        }
        of << key << "=" << instruments << endl;
    }

    of << "# OSC host and port" << endl;
    of << "remote=" << Replicator::getThisIp() << endl;
    of << "oscPort=" << port << endl;
    of << "discovery=true" << endl;
    of.close();
    info.publish("the BO Control configuration has been saved.");
/*
# Control

# bpm
bpm=60
bpmMn=20
bpmMx=240
bpmCh=13

quality=50
qualityMn=5
qualityMx=100
qualityCh=13

tick=50
tickMn=20
tickMx=250
tickCh=22



# 16 following instruments are available for selection
instruments=19,49,122,28,123,26,4,1,45,46,47,12,36,31,33,105
instruments1=20,50,123,29,53,27,5,2,48,51,52,2,37,32,34,106

# OSC host and port
remote=192.168.1.3
oscPort=12345
*/

}

void testApp::startOsc(const char*img)
{
    ofxOscMessage m;
    m.setAddress( "/bo/start" );
    m.addStringArg(img);
    osc.sendMessage( m );
}

void testApp::stopOsc(const char*img)
{
    ofxOscMessage m;
    m.setAddress( "/bo/stop" );
    m.addStringArg(img);
    osc.sendMessage( m );
}

bool testApp::newOscMessage(ofxOscMessage& m)
{
    int mel;
    if(m.getAddress() == "/bo/melody/play"){
        mel = m.getArgAsInt32(0);
        Melody * m = ctl.getMelody(mel);
        if (m && !(m->getPlayingState() & MPLAY))
        {
            m->control('p');
            info.publish("remote play " + ofToString(mel));
        }
        else
        {
            info.publish("IGNORED remote play " + ofToString(mel), ofColor::aqua);
        }
    }
    else if(m.getAddress() == "/bo/melody/pause"){
        mel = m.getArgAsInt32(0);
        Melody * m = ctl.getMelody(mel);
        if (m && (m->getPlayingState() & MPLAY))
        {
            m->control('p');
            info.publish("remote pause " + ofToString(mel));
        }
        else
        {
            info.publish("IGNORED remote pause " + ofToString(mel), ofColor::aqua);
        }
    }
    else if(m.getAddress() == "/bo/key"){
        int key = m.getArgAsInt32(0);
        if (isAllowed(key))
        {
            int prevMel = -1;
            mtx.lock();
            if (m.getNumArgs() > 1)
            {
                prevMel = ctl.getCurrentMelody()->m_Id;
                // first select the melody if the ID is available
                ctl.setCurrentMelody(m.getArgAsInt32(1));
                cmd.select(m.getArgAsInt32(1));
            }
            keyPressed(key | 0x8000);
            string sym;
            if (key < 128) sym += (char) key;
            info.publish("remote key " + ofToString(key) + " " + sym);
            // restore
            if (prevMel >= 0)
            {
                ctl.setCurrentMelody(prevMel);
                cmd.select(prevMel);
            }
            mtx.unlock();
        }
        // cout << "remote key " << m.getArgAsInt32(0) << endl;
    }
    else if(m.getAddress() == "/bo/value"){
        setValue(m.getArgAsInt32(0), m.getArgAsFloat(2), m.getArgAsInt32(1), false);
        info.publish( "remote value " + ofToString(m.getArgAsInt32(0)));
    }
    else if(m.getAddress() == "/bo/svalue"){
        setStrValue(m.getArgAsInt32(0), m.getArgAsString(2), m.getArgAsInt32(1), false);
        info.publish( "remote svalue " + ofToString(m.getArgAsInt32(0)) + " '" + m.getArgAsString(2) + "'");
    }
    else if(m.getAddress() == "/bo/select"){
        ctl.setCurrentMelody(m.getArgAsInt32(0));
        int mid = ctl.getCurrentMelody()->m_Id;
        if (mid == m.getArgAsInt32(0))
        {
            cmd.select(mid);
            info.publish( "remotely selected " + ofToString(mid));
        }
    }
    else if(m.getAddress() == "/bo/goto"){
        float x = m.getArgAsFloat(0);
        float y = m.getArgAsFloat(1);
        ctl.getCurrentMelody()->goTo(x, y);
        // cout << "remote goto " << x << " " << y << endl;
    }
    else if(m.getAddress() == "/bo/image"){
        string s = m.getArgAsString(0);
        loadImage(s, false);
        info.publish( "remote load image " + s);
    }
    else if(m.getAddress() == "/bo/scale"){
        string s = m.getArgAsString(0);
        loadScale(s, false);
        info.publish( "remote load scale " + s);
    }
    else if(m.getAddress() == "/bo/midi/control"){
        ofxMidiMessage ev;
        ev.status = MIDI_CONTROL_CHANGE;
        ev.control = m.getArgAsInt32(0);
        ev.value = m.getArgAsInt32(1);
        ev.portName = "BO";
        ev.portNum = -1;
        newMidiMessage(ev);
        // cout << "remote midi control " << ev.control << " " << ev.value << endl;
    }
    else if(m.getAddress() == "/bo/midi/note"){
        ofxMidiMessage ev;
        ev.status = m.getArgAsInt32(0) ? MIDI_NOTE_ON : MIDI_NOTE_OFF;
        ev.channel = m.getArgAsInt32(1);
        ev.pitch = m.getArgAsInt32(2);
        ev.velocity = m.getArgAsInt32(3);
        ev.portName = "BO";
        ev.portNum = -1;
        newMidiMessage(ev);
        // cout << "remote midi note " << ev.channel << " " << ev.pitch << " " << ev.velocity << endl;
    }
    else if(m.getAddress() == "/bo/serial"){
        int b = m.getArgAsInt32(0);
        Conditions::handle(b);
        info.publish( "remote serial " + ofToString(b));
    }
    else {
        // unrecognized message: display on the bottom of the screen
        string msg_string;
        msg_string = m.getAddress();
        msg_string += ": ";
        for(int i = 0; i < m.getNumArgs(); i++){
            // get the argument type
            msg_string += m.getArgTypeName(i);
            msg_string += ":";
            // display the argument - make sure we get the right type
            if(m.getArgType(i) == OFXOSC_TYPE_INT32){
                msg_string += ofToString(m.getArgAsInt32(i));
            }
            else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
                msg_string += ofToString(m.getArgAsFloat(i));
            }
            else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
                msg_string += m.getArgAsString(i);
            }
            else{
                msg_string += "unknown";
            }
        }
        cout << msg_string << endl;
        return false;
    }
    gui.updateGui(true);
    return true;
}

ofRectangle testApp::getImageRect()
{
    int w = ofGetWidth(), h = ofGetHeight();
    if (aspect && w && h)
    {
        float ratioScr = (float) w / h;
        float ratioImg = (float) srcImg.width / srcImg.height;
        ofRectangle rt;
        if (ratioScr > ratioImg)
        {
            rt.height = h - 2;
            rt.width = rt.height * ratioImg;
        }
        else
        {
            rt.width = w - 2;
            rt.height = rt.width / ratioImg;
        }
        rt.x = (w - rt.width) / 2;
        rt.y = (h - rt.height) / 2;
        return rt;
    }
    else
    {
        int dh = exitAt ? 0 : 40;
        return ofRectangle(0, dh, w, h - 40);
    }
}

void testApp::setStrValue(int key, string s, int mel, bool repl)
{
    Melody *me = (mel >= 0) ? ctl.getMelody(mel) : ctl.getCurrentMelody();

    switch (key)
    {
    case VALUE_DEST:
        me->setDest(s);
        gui.updateInstruments(me);
        break;
    case VALUE_CHORD:
        me->setChord(s);
        break;
    case VALUE_MIDI:
        mout.selectMidi(s);
        gui.updateInstruments(me);
        break;
    }
    if (repl)
    {
        replicator.svalue(key, s, mel);
    }

    cmd.svalue(key, s, mel);
}

float testApp::getValue(int key) {
    Melody *me = ctl.getCurrentMelody();
    ofPoint pt;
    if (me) pt = me->getCenter();

    switch (key)
    {
    case VALUE_TICK:
        return tick0;
    case VALUE_BPM:
        return bpm;
    case VALUE_ALPHA:
        return alpha;
    case VALUE_ARPEGGIO:
        return Melody::arpeggioDelay;
    case VALUE_DRUM_COUNT:
        DrumKit::mustHaveDrum(me);
        return me->m_pDrum->getDrumCount();
    case VALUE_DRUM_TYPE:
        DrumKit::mustHaveDrum(me);
        return me->m_pDrum->getDrumType();
    case VALUE_ECHO:
        return me->getEcho();
    case VALUE_DURATION:
        return me->getLegato();
    case VALUE_TRANSPOSE:
        return pt.y;
    case VALUE_VOLUME:
        return me->getVolume();
    case VALUE_QUALITY:
        return me->quality;
    case VALUE_DISPLAY:
        return displayMode;
    case VALUE_DYNAMIC:
        return me->getDynamic();
    case VALUE_MIDI_CH:
        return me->getCh();
    case VALUE_POSITION_X:
        return pt.x;
    }
    cout << "unknown key " << key;
    return 0;
}

string testApp::getStrValue(int key)  {
    Melody *me = ctl.getCurrentMelody();
    if (me) {
        switch (key)
        {
        case VALUE_DEST:
            return me->getDestName();
        case VALUE_CHORD:
            return me->getChordName();
        case VALUE_MIDI:
            return mout.getMidiName();
        }
    }
    return "";
}

void testApp::setValue(int key, float v, int mel, bool repl)
{
    Melody *me = (mel >= 0) ? ctl.getMelody(mel) : ctl.getCurrentMelody();
    ofPoint pt;
    if (me) pt = me->getCenter();

    switch (key)
    {
    case VALUE_TICK:
        tick0 = v;
        break;
    case VALUE_BPM:
        if (v) period = 60000 / (bpm = v);
        break;
    case VALUE_ALPHA:
        alpha = v;
        break;
    case VALUE_ARPEGGIO:
        Melody::arpeggioDelay = v;
        break;
    case VALUE_DRUM_COUNT:
        DrumKit::mustHaveDrum(me);
        me->m_pDrum->setDrumCount(v);
        drumKit.assignDrum(me);
        break;
    case VALUE_DRUM_TYPE:
        DrumKit::mustHaveDrum(me);
        me->m_pDrum->setDrumType(v);
        drumKit.assignDrum(me);
        break;
    case VALUE_ECHO:
        me->setEcho(v);
        break;
    case VALUE_DURATION:
        me->setLegato(v);
        break;
    case VALUE_TRANSPOSE:
        pt.y = v;
        me->moveTo(pt);
        break;
    case VALUE_VOLUME:
        me->setVolume(v);
        break;
    case VALUE_QUALITY:
        me->quality = v;
        break;
    case VALUE_DISPLAY:
        displayMode = v;
        break;
    case VALUE_DYNAMIC:
        me->setDynamic(v);
        break;
    case VALUE_MIDI_CH:
        me->setCh(v);
        break;
    case VALUE_POSITION_X:
        pt.x = v;
        me->moveTo(pt);
    }

    if (repl)
    {
        replicator.value(key, v, mel);
    }

    cmd.value(key, v, mel);
}

vector<string> testApp::getInstrumentList(bool prefixed, Melody *me) {
    vector<string> instruments;
    if (!me) {
        me = ctl.getCurrentMelody();
    }
    string dest = me->getDestName();
    if (dest.find("MIDI") == 0)
    {
        const char *prefixes = "0123456789ABCDEFG";
        for(int i=0; i <= 16; ++i)
        {
            if (prefixed) {
                const char pp[3] = {prefixes[i], ' ', 0};
                string p = string(pp) + MidiInstruments::getName(mout.getMidiNo(), i, i==10);
                instruments.push_back(p);
            } else {
                instruments.push_back(MidiInstruments::getName(mout.getMidiNo(), i, i==10));
            }
        }
    }
    else if (dest == "AUDIO")
    {
        instruments = sampler.getNames();
    }

    return instruments;
}

vector<string> testApp::getMidiList() {
    vector<string> devices;
    for (int i = 0; ; i++)
    {
        string nm = mout.getMidiName(i);
        if (nm.length() == 0) break;
        devices.push_back(nm);
    }
    return devices;
}

bool testApp::isAllowed(int key)
{
    return !ofContains(ignoredKeys, key);
}

void testApp::startClosing()
{
    webServer.stop();
    aspect = true;
    exitAt = ofGetElapsedTimeMillis() + 10000; // don't wait for more than 10 seconds
    stopOsc(imgFile);
    if (mp) delete mp;
    mp = 0;
    if (dp) delete dp;
    dp = 0;
    gui.showGear(false);
    glutSetCursor(GLUT_CURSOR_NONE);

    cout << "closing... " << endl;
}

void testApp::exit()
{
    if (mp)
    {
        startClosing();
    }
    master.stopAll();
    AllThreads::stopAll();
    Poco::Net::uninitializeNetwork();
    cout << "bye" << endl;
}
