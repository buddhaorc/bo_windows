#include "Duration.h"
#include "testApp.h"

using namespace std;

NoteDuration::NoteDuration()
{
    //ctor
}

NoteDuration::~NoteDuration()
{
    //dtor
}

void NoteDuration::init(ofxIniSettings &ini)
{
    cntShort = ini.get("cnt.short", 20);
    cntMedium = ini.get("cnt.medium", 50); //not used
    cntLong = ini.get("cnt.long", 200);
    dscShort = testApp::getIniFloat(ini, "dscale.short", "2.0");
    dscMedium = testApp::getIniFloat(ini, "dscale.medium", "1.0");
    dscLong = testApp::getIniFloat(ini, "dscale.long", "0.2");
    cout << dscShort << " " << dscMedium << " " << dscLong << endl;
}

void NoteDuration::setDuration(Note *no, Note *prev, float dscale)
{
    float dx = (no->pt.x - prev->pt.x);
    float dy = (no->pt.y - prev->pt.y);
    float d = sqrt(dx * dx + dy * dy) * dscale;
    no->dur = ceil(d); // we don't want zero durations. they are confusing.
}

float NoteDuration::getDurationScale(Melody *me)
{
    int cnt = me->melody.size();
    // duration of a single pixel
    float pxt = dscMedium;
    if (cnt < cntShort) {
        pxt = dscShort;
    } else if (cnt > cntLong) {
        pxt = dscLong;
    }
    return pxt;
}

void NoteDuration::assignDurations(Melody *me)
{
    float dscale = getDurationScale(me);
    Note *prev = *(--me->melody.end());
    list<Note*>::iterator n = me->melody.begin();
    for ( ; n != me->melody.end(); n++)
    {
        setDuration(*n, prev, dscale);
        prev = *n;
    }
}
