#include "testApp.h"
#include "NoteLine.h"

const float HT = 50.0f;
const float DY = HT / 8;

NoteLine::NoteLine()
{
    //ctor
    // http://www.hindson.com.au/wordpress/free/free-fonts-available-for-download/
    if (!font.loadFont("STAFCPE_.TTF", 20, true, true, true)) {
        cout << "****** failed to load font" << endl;
    }

}

NoteLine::~NoteLine()
{
    //dtor
}

void NoteLine::drawMelody(Melody *m, int col, int plcol)
{
    ofPushStyle();
    const char *pitches = "zZxXcvVbBnNmaAsSdfFgGhHjqQwWerRtTyYu1!2@34$5%6^7";
    // the last B - 7 - has no sharp.
    const char *pitchesB = "zxXcCvVbnNmMasSdDfFghHjJqwWeErRtyYuU12@3#4$56^77";
    const int NP = strlen(pitches);
    const int LEFT = 20;
    ofSetHexColor(col);
    ofFill();
    // TODO : select treble or bass clef
    bool treble = m->getCenter().y < m->getImgHeight() / 2;

    int h = ofGetHeight();
    int w = ofGetWidth();
    if (!w || !h) return;

    int prevX = LEFT;
    float y = h - (treble ? 3 : 5) * DY;
    char buff[256];
    char *p = buff;
    *p++ = treble ? '&' : '?';
    *p = 0;
    ofRectangle playingRect = font.getStringBoundingBox(buff, LEFT, y);
    int Y = playingRect.y - 20;

    list<Note*>::iterator noit = m->melody.begin();
    for (int i=0; noit != m->melody.end(); noit++, i++)
    {
        Note *no = *noit;
        int w0 = 0;
        bool current = (m->current() == no);
        bool playing = (no->playing > 0);
        playingRect = font.getStringBoundingBox(buff, LEFT, y);
        prevX = playingRect.x + (w0 = playingRect.width);

        if (no->note0 < 0)
        {
            *p++ = '='; // some space
        }
        else
        {
            int nn = (playing ? no->note : no->note0);
            nn -= (treble ? 48 : 28);
            if (nn >=0 && nn < NP)
            {
                *p++ = treble ? pitches[nn] : pitchesB[nn];
            }
            else
            {
                *p++ = ' ';
            }
        }

        *p = 0;
        playingRect = font.getStringBoundingBox(buff, LEFT, y);
        playingRect.x = prevX;
        playingRect.width -= w0;

        if (current || playing) {
            ofSetHexColor(0xFFAAFF);
            int yy = Y - 2;
            ofSetLineWidth(3);
            ofLine(playingRect.x, yy, playingRect.width + playingRect.x, yy);
            // restore
            ofSetHexColor(col);
            ofSetLineWidth(1);
        }

        int yyy = Y - 4 - no->dur;
        ofLine(playingRect.x, yyy, playingRect.width + playingRect.x, yyy);

        if (p > buff + 254) break;
        if (playingRect.x > w) break;
    }

// the base line for durations
    int yy = Y - 4;
    ofLine(LEFT, yy, playingRect.width + playingRect.x, yy);

    font.drawString(buff, LEFT, y);
    ofPopStyle();
}

// http://www.dolmetsch.com/musictheory1.htm
// The lineNo is integer for notes EGBDF and has a half for FACE
// Lines are counted from lowest
// treble/bass clef
void NoteLine::placeNote(int no, bool treble, float &lineNo, bool &sharp)
{
    // middle C = note 60

}
