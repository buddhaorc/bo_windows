#include "Scale.h"
#include <strstream>

using namespace std;

int Scale::m_NumNotes = 0;
double *Scale::m_Ratios = 0;
string Scale::m_ScaleFile;

extern void trimLeading(string &s); // Commander.cpp

void Scale::midiNoteToScaled(int note, int &midiNote, float &bend)
{
    if (!m_NumNotes)
    {
        if (note < 0) midiNote = 0;
        else if (note > 127) midiNote = 127;
        else midiNote = note;
        bend = 0;
    }
    else
    {
        int noct = note / m_NumNotes; // 36
        int n = note % m_NumNotes;
        double required = n ? m_Ratios[n - 1] : 1.000000001;
        double relog = NOTE_NUM * log2(required);
        double mn = floor(relog); //
        midiNote = noct * NOTE_NUM + mn;
        bend = NOTE_NUM * log2( required / pow(2.0, mn / NOTE_NUM) );
//        cout << note << " -> " << midiNote << " : " << bend << endl;
    }
}

void Scale::mapFreqTo(float freq, int &midiNote, float &bend)
{
    if (!m_NumNotes)
    {
        int n = 69 + NOTE_NUM * log2f(freq / 440.0);
        if (n < 0) midiNote = 0;
        else if (n > 127) midiNote = 127;
        else midiNote = n;
        bend = 0;
    }
    else
    {

    }
}

bool Scale::loadScale(const char *fname)
{
    if (m_Ratios)
    {
        delete m_Ratios;
        m_Ratios = 0;
        m_NumNotes = 0;
        m_ScaleFile = "";
    }

    if (!fname || !strlen(fname))
    {
        return true; // done cleanup
    }

    ifstream ifs(fname, ios::in);
    if (!ifs) return false;
    m_ScaleFile = fname;
    string line;
    bool headline = false;
    int num = -1, idx = 0;

    while (getline(ifs, line))
    {
        trimLeading(line);
        if (!line.length() || line.at(0) == '!') continue;
        if (!headline)
        {
            cout << line << endl;
            headline = true;
            continue;
        }
        istrstream istr(line.c_str());
        if (num < 0)
        {
            if (! (istr >> num) )
            {
                cout << "ignored [1]: '" << line << "'" << endl;
                continue;
            }
            if (num < 0)
            {
                cout << "Number of notes must be not negative: " << num << endl;
                return false;
            }
            m_Ratios = new double[num];
            cout << num << endl;
            continue;
        }

        if (line.find('.') != string::npos)
        {
            double ce;
            // must be cents
            if (! (istr >> ce) )
            {
                cout << "ignored [2]: '" << line << "'" << endl;
                continue;
            }
            m_Ratios[idx] = pow( 2.0, ce / 1200 );
        }
        else
        {
            // ratio
            int a, b = 1;
            char c;
            if (! (istr >> a) )
            {
                cout << "ignored [3]: '" << line << "'" << endl;
                continue;
            }
            if ((istr >> c ) && c == '/')
            {
                istr >> b;
            }
            m_Ratios[idx] = (double) a / b;
        }

        if (m_Ratios[idx] <= 0)
        {
            cout << "negative ratios are not allowed " << line << endl;
            return false;
        }

        if (idx && m_Ratios[idx] < m_Ratios[idx - 1])
        {
            cout << "WARNING: The ratio " << m_Ratios[idx] << " is out of order" << endl;
        }
        else
        {
            cout << m_Ratios[idx] << endl;
        }
        idx++;
        if (idx >= num) break;
    }
    m_NumNotes = num;
    return true;
}

