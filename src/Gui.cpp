#include "Gui.h"
#include "testApp.h"
#include "Scale.h"
#include "Chord.h"
#include "NotePlayer.h"

#define CANVAS_WIDTH 200
#define CANVAS_WIDTH_2 300

Gui::Gui()
{
    //ctor
    wd = 200 - 2 * OFX_UI_GLOBAL_WIDGET_SPACING;
    guiAssign = guiN = 0;
    dirty = true;
}

Gui::~Gui()
{
    //dtor
}

void Gui::init(testApp *a, ofxIniSettings &ini)
{
    ta = a;
    instrList = 0;

    gear = new ofxUICanvas(0, 0, CANVAS_WIDTH, ofGetHeight());
    gearBtn = gear->addImageToggle("Gear", "GUI/Factory.png", true, 32.0, 32.0);
    gear->setVisible(true);
    gear->autoSizeToFitWidgets();
    ofAddListener(gear->newGUIEvent,this,&Gui::guiEvent);

    guiM = new ofxUICanvas(0, 40, CANVAS_WIDTH, ofGetHeight());
    guiM->addSpacer(wd, 10);
    melodyLabel = guiM->addLabel("MELODY", OFX_UI_FONT_SMALL);
    guiM->addSpacer();
    play = guiM->addLabelToggle("PLAY", true, true);
    play->setColorFill(ofColor(127,255,255,150));
    play->setColorBack(ofColor(255,127,255,200));

    reduceBtn = guiM->addLabelButton("REDUCE (r)", false, true);
    mergeBtn = guiM->addLabelButton("MERGE (m)", false, true);
    splitBtn = guiM->addLabelButton("SPLIT (s)", false, true);
    extremeBtn = guiM->addLabelButton("EXTREME (x)", false, true);
    pauseBtn = guiM->addLabelButton("ADD PAUSES (z)", false, true);
    rmPauseBtn = guiM->addLabelButton("CLEAR PAUSES (Z)", false, true);
    restoreBtn = guiM->addLabelButton("RESTORE (Y)", false, true);
    assignBtn = guiM->addLabelButton("ASSIGN (Q)", false, true);

    guiM->addSpacer();
    guiM->addLabel("MIDI CC", OFX_UI_FONT_SMALL);
    envelToggle = guiM->addLabelToggle("ENVELOPE (L)", true, true);
    veloToggle = guiM->addLabelToggle("VELOCITY (V)", true, true);
    noteToggle = guiM->addLabelToggle("NOTE (N)", true, true);
    trackToggle = guiM->addLabelToggle("TRACK (K)", true, true);

    guiM->addSpacer();

    soloToggle = guiM->addLabelToggle("SOLO (o)", true, true);
    muteToggle = guiM->addLabelToggle("MUTE (u)", true, true);
    reverseToggle = guiM->addLabelToggle("REVERSE (b)", true, true);

    guiM->addSpacer();
    guiM->addLabel("DESTINATION (T)", OFX_UI_FONT_SMALL);
    vector<string> dd;
    for (int i = 0; ; i++)
    {
        string nm = Melody::getDestName(i);
        if (nm.length() == 0) break;
        dd.push_back(nm);
    }
    destList = guiM->addRadio("DESTINATION", dd, OFX_UI_ORIENTATION_VERTICAL, OFX_UI_FONT_SMALL);

    guiS = new ofxUICanvas(200, 40, CANVAS_WIDTH, ofGetHeight());
    guiS->addSpacer();

    volumeSlider = new ofxUIMinimalSlider("VOLUME (F2/F3)", -127, 127, 0.0, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiS->addWidgetDown(volumeSlider);
    volumeSlider->setLabelPrecision(0);
    volumeSlider->setIncrement(1);

    dynamSlider = new ofxUIMinimalSlider("DYNAMIC (F11/F12)", 0, 1, 0.5, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiS->addWidgetDown(dynamSlider);
    dynamSlider->setLabelPrecision(2);
    dynamSlider->setIncrement(0.05);

    echoSlider = new ofxUIMinimalSlider("ECHO (J/H)", 0, 5, 0.0, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiS->addWidgetDown(echoSlider);
    echoSlider->setLabelPrecision(0);
    echoSlider->setIncrement(1);

    qualitySlider = new ofxUIMinimalSlider("QUALITY", 0, 100, 50.0, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiS->addWidgetDown(qualitySlider);
    qualitySlider->setLabelPrecision(0);
    qualitySlider->setIncrement(1);

    stacattoSlider = new ofxUIMinimalSlider("DURATION )/(", 0, 2.0, 1.0, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiS->addWidgetDown(stacattoSlider);
    stacattoSlider->setLabelPrecision(1);
    stacattoSlider->setIncrement(0.1);

    drumSlider = new ofxUIMinimalSlider("DRUM (PgUp/PgDn)", 0, 8, 0.0, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiS->addWidgetDown(drumSlider);
    drumSlider->setLabelPrecision(0);
    drumSlider->setIncrement(1);

    guiS->addSpacer();
    guiS->addLabel("CHORD (a)", OFX_UI_FONT_SMALL);
    vector<string> cc;
    if (CHord::getNumber() > 1)
    {
        for (int i = 0; i < CHord::getNumber(); i++)
        {
            string nm = CHord::getInfo(i);
            cc.push_back(nm);
        }
        chordTypeList = guiS->addRadio("CHORDS", cc, OFX_UI_ORIENTATION_HORIZONTAL, OFX_UI_FONT_SMALL);
        chordTypeList->activateToggle(CHord::getInfo());
        cc.clear();
    }
    else
    {
        chordTypeList = 0;
    }

    for (int i = 0; ; i++)
    {
        string nm = Melody::getChordName(i);
        if (nm.length() == 0) break;
        cc.push_back(nm);
    }
    chordList = guiS->addRadio("CHORD", cc, OFX_UI_ORIENTATION_VERTICAL, OFX_UI_FONT_SMALL);

    guiD = new ofxUICanvas(400, 40, CANVAS_WIDTH, ofGetHeight());
    guiD->addSpacer();
    guiD->addLabel("DRUMS (.)", OFX_UI_FONT_SMALL);
    vector<string> drums;
    for (int i = 0; i < DrumKit::numDrumTypes; i++)
    {
        char buf[100];
        int t = DrumKit::getDrumType(i);
        sprintf(buf,"%d %s", i + 1, MidiInstruments::getName(0, t, true));
        drums.push_back(buf);
    }
    drumList = guiD->addRadio("DRUMS", drums, OFX_UI_ORIENTATION_VERTICAL, OFX_UI_FONT_SMALL);

    guiP = guiI = 0;    // to be filled in updateGui()

    guiG = new ofxUICanvas(600, 40, CANVAS_WIDTH, ofGetHeight());
    //guiG->addSpacer(wd, 10);
    //guiG->addLabel("GLOBAL");
    //guiG->addSpacer();
    quitBtn = guiG->addLabelButton("QUIT (Esc)", true);
    pause = guiG->addLabelToggle("PAUSE (F5)", true);
    pause->setColorBack(ofColor(127,255,255,150));
    pause->setColorFill(ofColor(255,127,255,200));

    tickSlider = new ofxUIMinimalSlider("TICK (F8/F9)", 5, 250, 100, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiG->addWidgetDown(tickSlider);
    tickSlider->setLabelPrecision(0);

    bpmSlider = new ofxUIMinimalSlider("BPM (F6/F7)", 5, 200, 60, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiG->addWidgetDown(bpmSlider);
    bpmSlider->setLabelPrecision(0);

    alphaSlider = new ofxUIMinimalSlider("ALPHA", 0, 255, 0.0, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiG->addWidgetDown(alphaSlider);
    alphaSlider->setLabelPrecision(0);

    arpeggioSlider = new ofxUIMinimalSlider("ARPEGGIO [ / ]", 0, 100, Melody::arpeggioDelay, CANVAS_WIDTH, 0, OFX_UI_FONT_SMALL);
    guiG->addWidgetDown(arpeggioSlider);
    arpeggioSlider->setLabelPrecision(0);

    windowToggle = guiG->addLabelToggle("SCREEN (w)", true);

    vector<string> devices = ta->getMidiList();
    if (devices.size() > 1)
    {
        guiG->addSpacer(wd, 2);
        guiG->addLabel("DEVICE (F10)", OFX_UI_FONT_SMALL);
        midiList = guiG->addRadio("DEVICE", devices, OFX_UI_ORIENTATION_VERTICAL, OFX_UI_FONT_SMALL);
    }
    else
    {
        midiList = 0;
    }
    guiG->addSpacer();
    for (int i = 0; i < NSLIDERS; i++)
    {
        envelSliders[i] = new ofxUIMinimalSlider("ENV " + ofToString(i), 0, 127, ta->env.getSliderValue(i), CANVAS_WIDTH, 1);
        envelSliders[i]->setLabelPrecision(0);
        guiG->addWidgetDown(envelSliders[i]);
    }
    guiG->addSpacer();
    boConfigBtn = 0;// guiG->addLabelButton("BOC CONFIG (0)", false, true);

    guiLoadImg = new ofxUICanvas(0, 40, CANVAS_WIDTH_2, ofGetHeight());
    if ((imageFiles = addFilesRadio(guiLoadImg, ".", "IMAGES", "jpg") ) == 0)
    {
        delete guiLoadImg;
        guiLoadImg = 0;
    }
    else
    {
        imageDirBtn = guiG->addLabelButton("IMAGES (*)", false, true);
    }

    guiLoadMel = new ofxUICanvas(0, 40, CANVAS_WIDTH_2, ofGetHeight());
    if (( melodyFiles = addFilesRadio(guiLoadMel, "..", "MELODY SETTINGS (&)", "txt", "melodies") ) == 0)
    {
        delete guiLoadMel;
        guiLoadMel = 0;
    }
    else
    {
        meloDirBtn = guiG->addLabelButton("STORED SETTINGS (&)", false, true);
    }

    guiG->addSpacer();

    guiM->setVisible(false);
    guiS->setVisible(false);
    guiG->setVisible(false);
    guiD->setVisible(false);
    guiS->autoSizeToFitWidgets();
    guiM->autoSizeToFitWidgets();
    guiG->autoSizeToFitWidgets();
    guiD->autoSizeToFitWidgets();
//    gui->setColorBack(ofColor(255,100));
//    gui->setWidgetColor(OFX_UI_WIDGET_COLOR_BACK, ofColor(255,100));

    ofAddListener(guiM->newGUIEvent,this,&Gui::guiEvent);
    ofAddListener(guiS->newGUIEvent,this,&Gui::guiEvent);
    ofAddListener(guiG->newGUIEvent,this,&Gui::guiEvent);
    ofAddListener(guiD->newGUIEvent,this,&Gui::guiEvent);
}

//--------------------------------------------------------------
ofxUIRadio *Gui::addFilesRadio(ofxUICanvas *gui, string d, string caption, string ext, string filter)
{
    gui->addSpacer();
    gui->addLabel(caption);
    ofDirectory dir;
    dir.allowExt(ext);
    dir.listDir(d);
    dir.sort();
    vector<string> names;
    int fsz = filter.size();
    for (int i = 0; i < dir.size(); i++)
    {
        string nm = dir.getName(i);
        if (fsz && nm.substr(0, fsz) != filter) continue;
        names.push_back(nm);
    }
    if (!names.size()) return 0;

    ofxUIRadio *radio = gui->addRadio(caption, names, OFX_UI_ORIENTATION_VERTICAL, OFX_UI_FONT_SMALL);
    gui->addSpacer();
    gui->addLabelButton("CANCEL", true);
    gui->autoSizeToFitWidgets();
    gui->setVisible(false);
    ofAddListener(gui->newGUIEvent,this,&Gui::guiDirEvent);
    return radio;
}

//--------------------------------------------------------------
void Gui::showImageDir()
{
    if (guiLoadImg) guiLoadImg->setVisible(true);
}

//--------------------------------------------------------------
void Gui::showMeloDir()
{
    if (guiLoadMel) guiLoadMel->setVisible(true);
}

//--------------------------------------------------------------
void Gui::updatePlaying()
{
    if (guiP)
    {
        ofRemoveListener(guiP->newGUIEvent,this,&Gui::guiEvent);
        delete guiP;
        guiP = 0;
    }

    list<Melody*> *music = &ta->az.m_Music;
    vector<string> playing;
    vector<string> drumming;

    char buf[256];
    string activeP, activeD;
    for (list<Melody*>::iterator it= music->begin(); it != music->end(); it++)
    {
        Melody *m = *it;
        const char *pre = " ";
        if (ta->ctl.getSoloMelody() == m)
        {
            pre = "*";
        }

        if (m->getPlayingState() & (MPLAY | MINST))
        {
            sprintf(buf, "%2i%s%s", m->m_Id, pre, m->instrInfo());
            playing.push_back(buf);
            if (ta->ctl.isSelected(m))
            {
                activeP = buf;
            }
        }
        if (m->getPlayingState() & MDRUM)
        {
            sprintf(buf, "%2i%s%s", m->m_Id, pre, m->drumInfo());
            drumming.push_back(buf);
            if (ta->ctl.isSelected(m))
            {
                activeD = buf;
            }
        }
    }

    if (drumming.size() || playing.size())
    {
        guiP = new ofxUICanvas(guiG->getRect()->getMinX(), guiG->getRect()->getMaxY(), CANVAS_WIDTH, ofGetHeight());
        //guiP->addSpacer(wd, 10);
        if (playing.size())
        {
            guiP->addLabel("PLAYING", OFX_UI_FONT_SMALL);
            playingList = guiP->addRadio("PLAYING", playing, OFX_UI_ORIENTATION_VERTICAL, OFX_UI_FONT_SMALL);
            playingList->activateToggle(activeP);
            guiP->addSpacer();
        }
        else
        {
            playingList = 0;
        }

        if (drumming.size())
        {
            guiP->addLabel("DRUMMING", OFX_UI_FONT_SMALL);
            drummingList = guiP->addRadio("DRUMMING", drumming, OFX_UI_ORIENTATION_VERTICAL, OFX_UI_FONT_SMALL);
            if (activeD.length())
            {
                drummingList->activateToggle(activeD);
            }
            guiP->addSpacer();
        }
        else
        {
            drummingList = 0;
        }

        guiP->autoSizeToFitWidgets();
        guiP->setVisible(guiM->isVisible());
        ofAddListener(guiP->newGUIEvent,this,&Gui::guiEvent);
    }
    else
    {
        playingList = 0;
        drummingList = 0;
    }
}

//--------------------------------------------------------------
void Gui::toggleVisible() {
    guiM->toggleVisible();
    if (guiI) guiI->toggleVisible();
    if (guiP) guiP->toggleVisible();
    guiS->toggleVisible();
    guiD->toggleVisible();
    guiG->toggleVisible();
    if (dirty && guiM->isVisible()) {
        updateGui();
    }
}

//--------------------------------------------------------------
void Gui::updateInstruments(Melody *me)
{
    if (guiI)
    {
        guiI->disable();
        guiI->exit();
        ofRemoveListener(guiI->newGUIEvent,this,&Gui::guiEvent);
        delete guiI;
        guiI = 0;
        instrList = 0;
    }
    string header;
    string dest = me->getDestName();
    if (dest.find("MIDI") == 0)
    {
        header = "INSTRUMENTS";
    }
    else if (dest == "AUDIO")
    {
        header = "SAMPLES";
    }
    if (header != "")
    {
        vector<string> instruments = ta->getInstrumentList(true, me);
        guiI = new ofxUICanvas(200, guiS->getRect()->getMaxY(), CANVAS_WIDTH, ofGetHeight());
        guiI->addSpacer();
        guiI->addLabel(header, OFX_UI_FONT_SMALL);
        instrList = guiI->addRadio(header, instruments, OFX_UI_ORIENTATION_VERTICAL, OFX_UI_FONT_SMALL);
        guiI->autoSizeToFitWidgets();
        guiI->setVisible(guiM->isVisible());
        ofAddListener(guiI->newGUIEvent,this,&Gui::guiEvent);
    }

    if (midiList) midiList->activateToggle(ta->mout.getMidiName());
}

//--------------------------------------------------------------
void Gui::updateGui(bool pla)
{
    if (!guiM->isVisible()) {
        dirty = true;
        return;
    }
    dirty = false;
    Melody *me = ta->ctl.getCurrentMelody();
    bool pl = me->getPlayingState() & MPLAY;
    play->setValue(pl ? 1 : 0);
    play->getLabelWidget()->setLabel(pl ? "PAUSE" : "PLAY");

    bool paused = ta->paused;
    pause->setValue(paused ? 1 : 0);
    pause->getLabelWidget()->setLabel(paused ? "CONTINUE" : "PAUSE");

    string lbl = me->getCCLabel(CC_ENVE);
    if (lbl != "")
    {
        envelToggle->setVisible(true);
        envelToggle->getLabelWidget()->setVisible(true);
        envelToggle->setValue(me->getCCflag(CC_ENVE));
        envelToggle->getLabelWidget()->setLabel("L: " + lbl);
    }
    else
    {
        envelToggle->setVisible(false);
    }

    lbl = me->getCCLabel(CC_VELO);
    if (lbl != "")
    {
        veloToggle->setVisible(true);
        veloToggle->getLabelWidget()->setVisible(true);
        veloToggle->setValue(me->getCCflag(CC_VELO));
        veloToggle->getLabelWidget()->setLabel("V: " + lbl);
    }
    else
    {
        veloToggle->setVisible(false);
    }

    lbl = me->getCCLabel(CC_NOTE);
    if (lbl != "")
    {
        noteToggle->setVisible(true);
        noteToggle->getLabelWidget()->setVisible(true);
        noteToggle->setValue(me->getCCflag(CC_NOTE));
        noteToggle->getLabelWidget()->setLabel("N: " + lbl);
    }
    else
    {
        noteToggle->setVisible(false);
    }

    lbl = me->getCCLabel(CC_TRCK);
    if (lbl.size())
    {
        trackToggle->setVisible(true);
        trackToggle->getLabelWidget()->setVisible(true);
        trackToggle->setValue(me->getCCflag(CC_TRCK));
        trackToggle->getLabelWidget()->setLabel("K: " + lbl);
    }
    else
    {
        trackToggle->setVisible(false);
    }

    muteToggle->setValue(me->getCCflag(CC_MUTE));
    soloToggle->setValue(ta->ctl.getSoloMelody() != 0);
    reverseToggle->setValue(me->getReverse());
    reverseToggle->getLabelWidget()->setLabel(me->getReverse() ? "FORTH" : "BACK");

    volumeSlider->setValue(me->getVolume());
    dynamSlider->setValue(me->getDynamic());
    echoSlider->setValue(me->getEcho());
    stacattoSlider->setValue(me->getLegato());
    qualitySlider->setValue(me->quality);
    drumSlider->setValue(me->m_pDrum ? me->m_pDrum->getDrumCount() : 0);

    if (DrumKit::numDrumTypes)
    {
        int t = me->m_pDrum ? me->m_pDrum->getDrumType() : 0;
        ofxUIToggle *tt = drumList->getToggles()[t % DrumKit::numDrumTypes];
        if (tt)
        {
            drumList->activateToggle(tt->getName());
    //        drumList->setLabelText(tt->getName());
        }
    }

    if (pla)
    {
        // currently playing and drumming
        updatePlaying();
    }

    if (chordTypeList)
    {
        chordTypeList->activateToggle(CHord::getInfo());
    }

    if (instrList)
    {
        int ch0 = me->getCh();
        if (ch0 >= 0 && (ta->mout.getMidiNo() == (ch0 >> 8)))
        {
            int ch = (ch0 & 0xff) ? ((ch0 - 1) & 0xf) + 1 : 0;
            vector<ofxUIToggle*> toggles = instrList->getToggles();
            int numTgl = toggles.size();
            if (numTgl)
            {
                string active = toggles[ch % numTgl]->getName();
                instrList->activateToggle(active);
            }
        }
        else
        {
            ofxUIToggle *a = instrList->getActive();
            if (a) a->setValue(false);
        }
    }

    char buf[100];
    sprintf(buf, "MELODY %d", me->m_Id);
    melodyLabel->setLabel(buf);

    destList->activateToggle(me->getDestName());
    chordList->activateToggle(me->getChordName());
    echoSlider->setVisible(me->getChordName() == "None");

    bpmSlider->setValue(ta->bpm);
    tickSlider->setValue(ta->tick0);
    alphaSlider->setValue(ta->alpha);

    arpeggioSlider->setValue(Melody::arpeggioDelay);

    chordList->setVisible(Scale::hasTwelveNotes());
}

//--------------------------------------------------------------
void Gui::activateRadioById(ofxUIRadio *r, int id)
{
    if (!r) return;
    vector<ofxUIToggle*> tt = r->getToggles();
    for (int i = 0; i < tt.size(); ++i)
    {
        string s = tt[i]->getName();
        int t;
        if (sscanf(s.c_str(), "%d", &t) && t == id)
        {
            r->activateToggle(s);
            return;
        }
    }
    // nothing found
    ofxUIToggle * a = r->getActive();
    if (a) a->setValue(false);
}

//--------------------------------------------------------------
void Gui::guiDirEvent(ofxUIEventArgs &e)
{
    ofxUIWidget *parent = e.widget->getParent();
    if (!parent) return;
    if (parent == imageFiles)
    {
        guiLoadImg->setVisible(false);
        ta->loadImage(e.widget->getName());
    }
    else if (parent == melodyFiles)
    {
        guiLoadMel->setVisible(false);
        ta->loadStoredMelodies(e.widget->getName());
    }
    else if (parent == guiLoadImg)
    {
        guiLoadImg->setVisible(false);
    }
    else if (parent == guiLoadMel)
    {
        guiLoadMel->setVisible(false);
    }
    else cout << parent->getName() << endl;

}

//--------------------------------------------------------------
void Gui::guiEvent(ofxUIEventArgs &e)
{
    if (e.widget == play)
    {
        ta->keyPressed('p');
    }
    else if (e.widget == pause)
    {
        ta->keyPressed(261); // F5
    }
    else if (e.widget == quitBtn)
    {
        ta->keyPressed('g');
        ta->keyPressed(27);
        ta->keyPressed(13);
    }
    else if (e.widget == windowToggle)
    {
        ta->keyPressed('w');
    }
    else if (e.widget == restoreBtn)
    {
        if(restoreBtn->getValue()) ta->keyPressed('Y');
    }
    else if (e.widget == assignBtn)
    {
        if(assignBtn->getValue()) { ta->keyPressed('Q'); toggleVisible(); }
    }
    else if (e.widget == reduceBtn)
    {
        if(reduceBtn->getValue()) ta->keyPressed('r');
    }
    else if (e.widget == extremeBtn)
    {
        if(extremeBtn->getValue()) ta->keyPressed('x');
    }
    else if (e.widget == splitBtn)
    {
        if(splitBtn->getValue()) ta->keyPressed('s');
    }
    else if (e.widget == mergeBtn)
    {
        if(mergeBtn->getValue()) ta->keyPressed('m');
    }
    else if (e.widget == pauseBtn)
    {
        if(pauseBtn->getValue()) ta->keyPressed('z');
    }
    else if (e.widget == rmPauseBtn)
    {
        if(rmPauseBtn->getValue()) ta->keyPressed('Z');
    }
    else if (e.widget == boConfigBtn)
    {
        if(boConfigBtn->getValue()) ta->keyPressed('O');
    }
    else if (e.widget == imageDirBtn)
    {
        if(imageDirBtn->getValue())
        {
            ta->keyPressed('*');
            toggleVisible();
        }
    }
    else if (e.widget == meloDirBtn)
    {
        if(meloDirBtn->getValue())
        {
            ta->keyPressed('&');
            toggleVisible();
        }
    }

    else if (e.widget == envelToggle)
    {
        ta->keyPressed('L');
    }
    else if (e.widget == veloToggle)
    {
        ta->keyPressed('V');
    }
    else if (e.widget == noteToggle)
    {
        ta->keyPressed('N');
    }
    else if (e.widget == trackToggle)
    {
        ta->keyPressed('K');
    }
    else if (e.widget == soloToggle)
    {
        ta->keyPressed('o');
    }
    else if (e.widget == muteToggle)
    {
        ta->keyPressed('u');
    }
    else if (e.widget == reverseToggle)
    {
        ta->keyPressed('b');
    }
    else if (e.widget == tickSlider)
    {
        float v = tickSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_TICK, v);
    }
    else if (e.widget == bpmSlider)
    {
        float v = bpmSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_BPM, v);
    }
    else if (e.widget == arpeggioSlider) {
        float v = arpeggioSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_ARPEGGIO, v);
    }
    else if (e.widget == alphaSlider)
    {
        float v = alphaSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_ALPHA, v);
    }
    else if (e.widget == echoSlider)
    {
        float v = echoSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_ECHO, v, ta->ctl.getCurrentMelody()->m_Id);
    }
    else if (e.widget == volumeSlider)
    {
        float v = volumeSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_VOLUME, v, ta->ctl.getCurrentMelody()->m_Id);
    }
    else if (e.widget == dynamSlider)
    {
        float v = dynamSlider->getScaledValue();
        ta->setValue(VALUE_DYNAMIC, v, ta->ctl.getCurrentMelody()->m_Id);
    }
    else if (e.widget == qualitySlider)
    {
        float v = qualitySlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_QUALITY, v, ta->ctl.getCurrentMelody()->m_Id);
    }
    else if (e.widget == stacattoSlider)
    {
        float v = stacattoSlider->getScaledValue();
        ta->setValue(VALUE_DURATION, v, ta->ctl.getCurrentMelody()->m_Id);
    }
    else if (e.widget == drumSlider)
    {
        float v = drumSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_DRUM_COUNT, v, ta->ctl.getCurrentMelody()->m_Id);
        updatePlaying();
    }
    else
    {
        string name = e.widget->getName();
        int kind = e.widget->getKind();
        ofxUIButton *button;
        ofxUIImageButton *ibutton;
        ofxUILabelButton *lbutton;
        ofxUIToggle *toggle;
        ofxUIImageToggle *itoggle;
        ofxUILabelToggle *ltoggle;
        ofxUISlider *slider;
        ofxOscMessage m;

        switch(kind)
        {
        case OFX_UI_WIDGET_MINIMALSLIDER:
            slider = (ofxUISlider *) e.widget;
            if (name.find("ENV") == 0)
            {
                cout << name.substr(4) << " " << slider->getValue() << endl;
                int ch = ofToInt(name.substr(4));
                ta->env.setSliderValue(ch, slider->getValue());
            }
            else
                cout << name << "\t value MS: " << slider->getValue() << endl;
            break;
        case OFX_UI_WIDGET_SLIDER_H:
            slider = (ofxUISlider *) e.widget;
            cout << name << "\t value1: " << slider->getValue() << endl;
            break;
        case OFX_UI_WIDGET_BUTTON:
            button = (ofxUIButton *) e.widget;
            cout << name << "\t value2: " << button->getValue() << endl;
            break;
        case OFX_UI_WIDGET_TOGGLE:
            toggle = (ofxUIToggle *) e.widget;
            if (toggle->getValue())
            {
                if (e.widget->getParent() == destList)
                {
                    ta->setStrValue(VALUE_DEST, toggle->getName(), ta->ctl.getCurrentMelody()->m_Id);
                }
                else if (e.widget->getParent() == chordList)
                {
                    string s = toggle->getName();
                    ta->setStrValue(VALUE_CHORD, s, ta->ctl.getCurrentMelody()->m_Id);
                    echoSlider->setVisible(s == "None");
                }
                else if (e.widget->getParent() == chordTypeList)
                {
                    string s = toggle->getName();
                    CHord::keypressed(s.at(0));
                }
                else if (e.widget->getParent() == midiList)
                {
                    ta->setStrValue(VALUE_MIDI, toggle->getName());
                    updateGui();
                }
                else if (e.widget->getParent() == instrList)
                {
                    ta->keyPressed(toggle->getName().at(0));
                }
                else if (e.widget->getParent() == drumList)
                {
                    int t;
                    if (sscanf(toggle->getName().c_str(), "%d", &t))
                    {
                        ta->setValue(VALUE_DRUM_TYPE, t - 1, ta->ctl.getCurrentMelody()->m_Id);
                        updatePlaying();
                    }
                }
                else if (e.widget->getParent() == playingList || e.widget->getParent() == drummingList)
                {
                    int id;
                    if (sscanf(toggle->getName().c_str(), "%d", &id))
                    {
                        ta->select(id);
                        updateGui(false);
                        if (e.widget->getParent() == drummingList) activateRadioById(playingList, id);
                        else activateRadioById(drummingList, id);
                    }
                }
                else if (e.widget->getParent() && e.widget->getParent()->getName() == "SENDERS")
                {
                    char buf[100];
                    if (sscanf(toggle->getName().c_str(), "%s", buf))
                    {
                        ta->sendCurrentRouteTo(buf);
                        guiAssign->setVisible(false);
                    }
                }
                else
                    cout << name << "\t value3: " << toggle->getValue() << endl;
            }
            break;
        case OFX_UI_WIDGET_IMAGEBUTTON:
            ibutton = (ofxUIImageButton *) e.widget;
            cout << name << "\t value4: " << ibutton->getValue() << endl;
            break;
        case OFX_UI_WIDGET_IMAGETOGGLE:
            itoggle = (ofxUIImageToggle *) e.widget;
            if (itoggle == gearBtn)
            {
                toggleVisible();
            }
            else
                cout << name << "\t value5: " << itoggle->getValue() << endl;
            break;
        case OFX_UI_WIDGET_LABELBUTTON:
            lbutton = (ofxUILabelButton *) e.widget;
            if (lbutton->getParent() == guiAssign)
            {
                guiAssign->setVisible(false);
            }
            else
                cout << name << "\t value6: " << lbutton->getValue() << endl;
            break;
        case OFX_UI_WIDGET_LABELTOGGLE:
            ltoggle = (ofxUILabelToggle *) e.widget;
            cout << name << "\t value7: " << ltoggle->getValue() << endl;
            break;
        case OFX_UI_WIDGET_DROPDOWNLIST:
            // cout << ((ofxUIDropDownList*) e.widget)->getName() << endl;
            break;
        default:
            cout << kind << " " << name << endl;

        }
    }
}

void Gui::toggleNetwork(Replicator &replicator)
{
    if (guiN)
    {
        delete guiN;
        guiN = 0;
        return;
    }
    vector<string> senders = replicator.getSenders(false);
    if (senders.size() == 0)
    {
        ta->info.publish("not configured for network performance", ofColor::white);
        return;
    }

    guiN = new ofxUISuperCanvas("BO NETWORK", 0, 40, 400, ofGetHeight());
    guiN->addSpacer();
    guiN->addLabel("This BO: " + replicator.myAddress(), OFX_UI_FONT_MEDIUM);
    guiN->addRadio("Interfaces", replicator.getIfList());
    guiN->addSpacer();
    guiN->addRadio("Senders", replicator.getSenders(true));

    guiN->autoSizeToFitWidgets();
    ofAddListener(guiN->newGUIEvent,this,&Gui::guiEvent);
}

bool Gui::assignRoute(int mel, Replicator &replicator)
{
    vector<string> senders = replicator.getSenders(false);
    if (senders.size() == 0)
    {
        ta->info.publish("no active BO Control nodes found", ofColor::white);
        return true;
    }
    if (guiAssign)
    {
        delete guiAssign;
    }
    guiAssign = new ofxUISuperCanvas("SELECT CONTROL NODE", ofGetWidth() / 2, ofGetHeight() / 2, 400, ofGetHeight() / 2);
    guiAssign->addSpacer();
    guiAssign->addRadio("SENDERS", replicator.getSenders(false));
    guiAssign->addSpacer();
    guiAssign->addLabelButton("CANCEL", true);

    guiAssign->autoSizeToFitWidgets();
    ofAddListener(guiAssign->newGUIEvent,this,&Gui::guiEvent);

    return true;
}
