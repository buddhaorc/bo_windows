#ifndef SERIALSENDER_H
#define SERIALSENDER_H

#include "ofxIniSettings.h"
#include "Harmonizer.h"

class SerialSender : public ofThread
{
    public:
        SerialSender();
        virtual ~SerialSender();
        void init(ofxIniSettings &ini);
        // sets bits corresponding to given note to 1
        void sendOn(int n, int d, int v);
        // sets bits corresponding to given note to 0
        void sendOff(int n);

        // ch = 0..3, val = 0..63
        void sendControl(int ch, int val);

        void threadedFunction();

        string status();
        bool isActive() { return active; }
    protected:
    private:
        ofSerial serial;
        bool    active;
        bool    hasBits;

        unsigned char m_byte;
        // control bits that correspond to each note
        unsigned char m_bits[NOTE_NUM];
};

#endif // SERIALSENDER_H
