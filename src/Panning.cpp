#include "Panning.h"
#include "NotePlayer.h"

Panning::Panning()
{
    x = y = z = 0.5;
    tx = ty = tz = 0.5;
    ox = oy = oz = 0.5;
    scl = 2;
    speed = 5;
    enabled = false;
    camera = false;
    osc = 0;
    transition = 1000;
}

Panning::~Panning()
{
    //dtor
}

void Panning::init(ofxIniSettings &ini, OscSender *o)
{
    camera = ini.get("pan.camera", false);
    transition = ini.get("pan.transition", 1000);
    string s = ini.get("pan.scale", string("2.0f"));
    scl = ofToFloat(s);
    if (camera)
    {
        vidGrabber.setVerbose(true);
        if (!vidGrabber.initGrabber(320, 240, true))
        {
            cout << "video grabber failed" << endl;
            camera = false;
        }
    }
    osc = o;
}

void Panning::setOffsets(float cx, float cy, float cz)
{
    x = cx;
    y = cy;
    z = cz;
    sendOscState();
}

void Panning::setOffsets(float cx, float cy)
{
    x = cx;
    y = cy;
    sendOscState();
}

void Panning::setOffsetZ(float cz)
{
    z = cz;
    sendOscState();
}

void Panning::sendOscState()
{
    if (osc)
    {
        ofxOscMessage msg;
        msg.setAddress("/bo/pan");
        msg.addFloatArg(x);
        msg.addFloatArg(y);
        msg.addFloatArg(z);
        msg.addFloatArg(scl);
        osc->sendMessage(msg);
    }
}

float Panning::getPan(float px, float py)
{
    return (px - x) * 2 * scl;
}

float Panning::getAttenuation(float px, float py)
{
    float dx = px - x;
    float dy = py - y;
    return - (dx * dx + dy * dy) * scl * 100;
}

void Panning::update()
{
    if (enabled)
    {
        if (camera)
        {
            vidGrabber.update();
            buf.setFromPixels(vidGrabber.getPixels(), vidGrabber.width, vidGrabber.height, OF_IMAGE_COLOR, true);
            // find the brightest spot
            int idx = -1;
            int best = 0;
            unsigned char *p = buf.getPixels();
            for (int i = 0, n = vidGrabber.width * vidGrabber.height * 3; i < n; i += 3)
            {
                int c = (int)*p++ + *p++ + *p++;
                if (c > best)
                {
                    best = c;
                    idx = i;
                }
            }
            idx /= 3;
            float cx = (float) (idx % vidGrabber.width) / vidGrabber.width;
            float cy = (float) (idx / vidGrabber.width) / vidGrabber.height;
            if (fabs(cx - tx) > 0.05 || fabs(cy - ty) > 0.05)
            {
                setTargets(cx, cy);
            }
        }

        // move towards the target
        if (started)
        {
            long now = ofGetElapsedTimeMillis();
            long dt = now - started;
            if (dt < transition)
            {
                x = ofMap(dt, 0, transition, ox, tx);
                y = ofMap(dt, 0, transition, oy, ty);
                z = ofMap(dt, 0, transition, oz, tz);
            }
        }
    }
}

void Panning::draw(ofRectangle &rect, ofImage &img)
{
    if (enabled)
    {
        ofPoint centre = rect.getCenter();
        centre += ofPoint((x - 0.5) * rect.width, (y - 0.5) * rect.height);
        if (camera)
        {
            ofPushMatrix();
            ofTranslate(rect.width, 0);
            ofScale(-1, 1);
            buf.draw(rect);
            ofSetColor(ofColor::red);
            ofCircle(centre.x, centre.y, 2);
            ofPopMatrix();
        }
        else
        {
            img.draw(rect);
            ofSetColor(ofColor::white);
            rect.setFromCenter(centre, rect.width / scl, rect.height / scl);
            ofNoFill();
            ofRect(rect);
        }
    }
}

bool Panning::handle(int key)
{
    if (!enabled)
    {
        return false;
    }
    switch(key)
    {
    case 359: // down
        y += 0.05;
        break;
    case 357: // down, up
        y -= 0.05;
        break;
    case 356:
        x -= 0.05;
        break;
    case 358: // Left, Right
        x += 0.05;
        break;
    case 362: // home
        x = 0;
        y = 0;
        break;
    default:
        return false;
    }
    setOffsets(x, y);
    return true;
}
