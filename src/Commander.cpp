#include "testApp.h"

Commander::Commander() : is(0), os(0), t0(ofGetElapsedTimeMillis())
{
    //ctor
    nextActTime = 0x7fffffff;
}

Commander::~Commander()
{
    //dtor
    if (is) delete is;
    if (os) delete os;
}

void Commander::openOut(const char *fn)
{
    os = new std::ofstream(fn);
    if (!os)
    {
        cout << "Error opening " << fn << endl;
    }
    else
    {
        (*os) << "0 Z" << endl;
    }

}

void Commander::openIn(const char *fn)
{
    is = new std::ifstream(fn);
    readNext();
}

void trimLeading(string &s)
{
    size_t startpos = s.find_first_not_of(" \t");
    if( string::npos != startpos )
    {
        s.erase(0, startpos);
    }
}

bool Commander::readNext()
{
    int t;
    char c;
    if (! ((*is) >> t >> c) ) return false;
    if (! (getline(*is, cmd)) ) return false;
    nextAct = c;
    nextActTime = t;
    trimLeading(cmd);
 //   cout << nextAct << " in " << t << " '" << cmd << "'" << endl;
    return true;
}

void Commander::act(string actcmd)
{
    char nxt;
    string cmd;
    int key, mel;
    float x, y;
    ofxMidiMessage m;
    istringstream istr(actcmd);
	istr >> nxt;

    switch (nxt)
    {
    case 'K':
        if (istr >> key)
        {
            if (ta->isAllowed(key))
            {
                ta->keyPressed(key);
                if (key == 27) exit(0);
            }
        }
        break;
    case 'M':
        if (istr >> m.control >> m.value)
        {
            m.status = MIDI_CONTROL_CHANGE;
            ta->newMidiMessage(m);
        }
        else
        {
            cout << "ignored " << actcmd << endl;
        }
        break;
    case 'N':
        if (istr >> m.channel >> key >> m.pitch >> m.velocity)
        {
            m.status = (MidiStatus)key;
            ta->newMidiMessage(m);
        }
        else
        {
            cout << "ignored " << actcmd << endl;
        }
        break;
    case 'I':
        getline(istr, cmd);
        trimLeading(cmd);
        ta->loadImage(cmd, true);
        break;
    case 'L':
        getline(istr, cmd);
        trimLeading(cmd);
        ta->loadScale(cmd);
        break;
    case 'V':
        if (istr >> key >> mel >> x)
        {
            ta->setValue(key, x, mel);
            ta->updateGui(true);
        }
        break;
    case 'A':
        if (istr >> key >> mel)
        {
            size_t sep = actcmd.find('=');
            if (sep != string::npos)
            {
                ta->setStrValue(key, actcmd.substr(sep + 1), mel);
                ta->updateGui(true);
            }
        }
        break;
    case 'S':
        getline(istr, cmd);
        trimLeading(cmd);
        ta->loadStoredMelodies(cmd);
        break;
    case 'G':
        if (istr >> x >> y)
        {
            ta->goTo(x, y);
        }
        else
        {
            cout << "ignored " << actcmd << endl;
        }
        break;
    case 'T':
        if (istr >> key)
        {
            ta->select(key);
        }
        else
        {
            cout << "ignored " << actcmd << endl;
        }
        break;
    default:
        cout << "ignored " << nxt << " of " << actcmd << endl;
    }
}

void Commander::act()
{
    if (!is) return;
    int t = ofGetElapsedTimeMillis() - t0;
    int key, mel;
    float x, y;
    ofxMidiMessage m;
    do {
        if (t < nextActTime)
        {
            return;
        }

        switch (nextAct)
        {
        case 'Z':
            t0 = ofGetElapsedTimeMillis();
            break;
        case '0':
            t0 = ofGetElapsedTimeMillis();
            is->seekg( 0 );
            cout << "seekg " << is->tellg() << endl;
            readNext();
            return;
        case 'K':
            if (sscanf(cmd.c_str(), "%d", &key))
            {
                if (ta->isAllowed(key))
                {
                    ta->keyPressed(key);
                    if (key == 27) exit(0);
                }
            }
            break;
        case 'M':
            if (sscanf(cmd.c_str(), "%d %d", &m.control, &m.value))
            {
                m.status = MIDI_CONTROL_CHANGE;
                ta->newMidiMessage(m);
            }
            else
            {
                cout << "ignored " << cmd << endl;
            }
            break;
        case 'N':
            if (sscanf(cmd.c_str(), "%d %d %d %d", &m.channel, &key, &m.pitch, &m.velocity) == 4)
            {
                m.status = (MidiStatus)key;
                ta->newMidiMessage(m);
            }
            else
            {
                cout << "ignored " << cmd << endl;
            }
            break;
        case 'I':
            ta->loadImage(cmd, true);
            break;
        case 'L':
            ta->loadScale(cmd);
            break;
        case 'V':
            if (sscanf(cmd.c_str(), "%d %d %g", &key, &mel, &x))
            {
                ta->setValue(key, x, mel);
                ta->updateGui(true);
            }
            break;
        case 'A':
            if (sscanf(cmd.c_str(), "%d %d", &key, &mel) == 2)
            {
                size_t sep = cmd.find('=');
                if (sep != string::npos)
                {
                    ta->setStrValue(key, cmd.substr(sep + 1), mel);
                    ta->updateGui(true);
                }
            }
            break;
        case 'S':
            ta->loadStoredMelodies(cmd);
            break;
        case 'G':
            if (sscanf(cmd.c_str(), "%f %f", &x, &y))
            {
                ta->goTo(x, y);
            }
            else
            {
                cout << "ignored " << cmd << endl;
            }
            break;
        case 'T':
            if (sscanf(cmd.c_str(), "%d", &key))
            {
                ta->select(key);
            }
            else
            {
                cout << "ignored " << cmd << endl;
            }
            break;
        }
        nextActTime = 0x7fffffff;
    } while (readNext());
    cout << "Completed reading" << endl;
}

void Commander::key(int k)
{
    if (!os) return;
    int t = ofGetElapsedTimeMillis() - t0;
    (*os) << t << " K " << k << std::endl;
}

void Commander::svalue(int k, string s, int mel)
{
    if (!os) return;
    int t = ofGetElapsedTimeMillis() - t0;
    (*os) << t << " A " << k << " " << mel << "=" << s << std::endl;
}

void Commander::value(int k, float v, int mel)
{
    if (!os) return;
    int t = ofGetElapsedTimeMillis() - t0;
    (*os) << t << " V " << k << " " << mel << " " << v << std::endl;
}

void Commander::midi(ofxMidiMessage &ev)
{
    if (!os) return;
    int t = ofGetElapsedTimeMillis() - t0;
    if (ev.status == MIDI_CONTROL_CHANGE)
    {
        (*os) << t << " M " << ev.control << " " << ev.value << std::endl;
    }
    else if (ev.status == MIDI_NOTE_OFF || ev.status == MIDI_NOTE_ON)
    {
        (*os) << t << " N " << ev.channel << " " << ev.status << " " << ev.pitch << " " << ev.velocity << std::endl;
    }
}

void Commander::scale(string fname)
{
    if (!os) return;
    if (fname.length())
    {
        int t = ofGetElapsedTimeMillis() - t0;
        (*os) << t << " L " << fname << std::endl;
    }
}

void Commander::image(string fname)
{
    if (!os) return;
    if (fname.length())
    {
        int t = ofGetElapsedTimeMillis() - t0;
        (*os) << t << " I " << fname << std::endl;
    }
}

void Commander::restored(string fname)
{
    if (!os) return;
    if (fname.length())
    {
        int t = ofGetElapsedTimeMillis() - t0;
        (*os) << t << " S " << fname << std::endl;
    }
}

void Commander::select(int mel)
{
    if (!os) return;
    int t = ofGetElapsedTimeMillis() - t0;
    (*os) << t << " T " << mel << std::endl;
}

void Commander::goTo(float x, float y)
{
    if (!os) return;
    int t = ofGetElapsedTimeMillis() - t0;
    (*os) << t << " G " << x << " " << y << std::endl;
}
