#include <sstream>
#include "testApp.h"
#include "WebController.h"
#include "Poco/Net/Net.h"
#include "Poco/URI.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPRequestHandler.h"

using namespace Poco::Net;

map<string, PageDescriptor*> WebController::pages;
string WebController::index_page, WebController::page_template, WebController::wwwdir;
testApp *WebController::theApp;

class WebRequestHandler : public HTTPRequestHandler {

public:
    virtual void handleRequest(HTTPServerRequest &req, HTTPServerResponse &resp)
    {
        long expiration = 0;
        Poco::URI uri(req.getURI());
        string path = uri.getPath();

        //  # is it update for one of the pages?
        if (path.compare(0, 7, "/update") == 0) {

        //            http://localhost/update/LFOs?LFO%20v%20Frequency=0.17391304347826086
            vector<string> qq = ofSplitString(path.substr(8), "?");
            string pg = qq[0];
            PageDescriptor *pd = WebController::pages[pg];
            if (!pd) {
                notFound(resp, pg);
                return;
            }

            pd->update_page_values(uri.getQuery());
            pg = "[" + pd->make_html_slider_values() + "]";

            resp.set("Cache-Control", "no-cache, no-store");
            resp.setContentType("text/plain");
            resp.setContentLength(pg.length());
    //            # already expired
            resp.set("Expires", expires(-1000));
            resp.setStatus(HTTPResponse::HTTP_OK);
            ostream& out = resp.send();
            out << pg;
            out.flush();
        }
        else if (path.compare(0, 6, "/image") == 0) {
            string fn = WebController::theApp->imgFile;
            bool tmpfile = false;
            vector<string> qq = ofSplitString(uri.getQuery(), "&", true, true);
            if (qq.size() >= 2) {
                int w = -1, h = -1;
                for (int i=0; i<qq.size(); ++i) {
                    string p = qq[i];
                    vector<string> pp = ofSplitString(p, "=", true, true);
                    if (pp.size() == 2) {
                        if (pp[0] == "w") {
                            w = ofToInt(pp[1]);
                        } else if (pp[0] == "h") {
                            h = ofToInt(pp[1]);
                        }
                    }
                }
                if (w > 0 && h > 0) {
                    ofImage img;
                    img.loadImage(fn);
                    img.resize(w, h);
                    tmpfile = true;
                    fn = "tmp" + ofToString(ofGetElapsedTimeMillis()) + ".jpg";
                    img.saveImage(fn, OF_IMAGE_QUALITY_LOW);
                    cout << "saved " << fn << " " << w << " " << h <<endl;
                }
            }
            sendFile(resp, fn, -1000);
            if (tmpfile) {
                if (!ofFile::removeFile(fn)) {
                    cout << "failed to delete " << fn << endl;
                }
            }
        }
        else if (path == "/fullstop") {
            string pg = "bye";
            resp.set("Cache-Control", "no-cache, no-store");
            resp.setContentType("text/plain");
            resp.setContentLength(pg.length());
            resp.set("Expires", expires(-1000));
            resp.setStatus(HTTPResponse::HTTP_OK);
            ostream& out = resp.send();
            out << pg;
            out.flush();

            WebController::theApp->exit();
        } else if (path.compare(0, 7, "/jQuery") == 0) {
            sendFile(resp, WebController::wwwdir + path, 60*60*24*31*12);

        } else if (path == "/") {
            resp.setStatus(HTTPResponse::HTTP_OK);
            resp.setContentType("text/html");
            resp.setContentLength(WebController::index_page.length());
            resp.set("Expires", expires(3600));
            ostream& out = resp.send();
            out << WebController::index_page;
            out.flush();
        }
        else {
            vector<string> qq = ofSplitString(path.substr(1), "?");
            string pg = qq[0];
            PageDescriptor *pd = WebController::pages[pg];
            if (!pd) {
                notFound(resp, pg);
                return;
            }
            pd->update_page_values(uri.getQuery());

            string templ = WebController::page_template;
            int pos = templ.find("{{{CONTROLS}}}");
            if (pos != string::npos) {
                int len = strlen("{{{CONTROLS}}}");
                pg = templ.replace(pos, len, pd->make_html_slider_values());
            }

            pos = templ.find("{{{INIT}}}");
            if (pos != string::npos) {
                int len = strlen("{{{INIT}}}");
                pg = templ.replace(pos, len, pd->make_html_init());
            }

            resp.set("Cache-Control", "no-cache, no-store");
            resp.setContentType("text/html");
            resp.setContentLength(pg.length());
    //            # already expired
            resp.set("Expires", expires(-1000));
            resp.setStatus(HTTPResponse::HTTP_OK);
            ostream& out = resp.send();
            out << pg;
            out.flush();
        }

        cout << endl
             << "Response sent for URI=" << req.getURI() << endl;
    }

    bool sendFile(HTTPServerResponse &resp, string path, long expiration) {
        int sz;
        char *rsp = WebController::read_file(path, sz);
        if (sz && rsp) {
            string tp = "text/plain";
            int dot = path.rfind(".");
            if (dot != string::npos) {
                string ext = path.substr(dot);
                if (ext == ".gif") {
                    tp = "image/gif";
                }
                else if (ext == ".jpg") {
                    tp = "image/jpeg";
                }
                else if (ext == ".css") {
                    tp = "text/css";
                }
                else if (ext == ".js") {
                    tp = "application/x-javascript";
                }
            }
            resp.setContentType(tp);
            resp.setContentLength(sz);
    //            # already expired
            resp.set("Expires", expires(expiration));
            resp.setStatus(HTTPResponse::HTTP_OK);
            ostream& out = resp.send();
            out.write(rsp, sz);
            out.flush();
        } else {
            notFound(resp, path);
            return false;
        }
        return true;
    }

    string expires(long expiration) {
        time_t rawtime;
        struct tm * timeinfo;
        char buffer [80];

        time(&rawtime);
        rawtime += expiration;
        timeinfo = gmtime(&rawtime);

        strftime(buffer,80,"%a, %d %b %Y %H:%M:%S GMT", timeinfo);
        return buffer;
    }

    void notFound(HTTPServerResponse &resp, string pg) {
        resp.setStatusAndReason(HTTPResponse::HTTP_NOT_FOUND, "Not found");
        ostream& out = resp.send();
        out << "Not found " << pg;
        out.flush();
    }
};

class WebHTTPRequestHandler : public HTTPRequestHandlerFactory {
    virtual HTTPRequestHandler* createRequestHandler(const HTTPServerRequest& request) {
        return new WebRequestHandler();
    }
};

WebController::WebController()
{
    //ctor
}

WebController::~WebController()
{
    if (server) {
        server->stop();
    }
}

void WebController::stop()
{
    if (server) {
        server->stop();
        server = 0;
    }
}

PicPageDescriptor::PicPageDescriptor(string t, string bf, string def) : PageDescriptor(t, bf) {
    controls.push_back(new PicCtrlDescriptor(def));
}

void PicPageDescriptor::update(string t, string v) {
    vector<string> xy = ofSplitString(v, "_", true, true);
    if (xy.size() != 2) {
        cout << "not expected '" << v << "'" << endl;
        return;
    }
    float x = ofToFloat(xy[0]);
    float y = (1 - ofToFloat(xy[1]));
    WebController::theApp->mouseDoubleClick(x, y);
}

void WebController::init(testApp *app, ofxIniSettings &ini)
{
    theApp = app;

    string text;
    string button_format = ini.get("tmpl.index.button", string("MISSING tmpl.index.button in configuration"));
    wwwdir = ini.get("wwwdir", string("templates\\"));

    vector<string> pgs = ofSplitString(ini.get("pages", string("")), ",", true, true);
    for (int i=0; i<pgs.size(); ++i)
    {
        string p = pgs[i];
        string pic = ini.get((p + ".pic").c_str(), string("NONE"));
        if (pic != "NONE") {
            PicPageDescriptor *pd = new PicPageDescriptor(p, button_format, pic);
            text += pd->make_html_list_element(p);
            pages[p] = pd;
            continue;
        }

        PageDescriptor *pd = new PageDescriptor(p, button_format);

        vector<string> sliders = ofSplitString(ini.get((p + ".sliders").c_str(), string("")), ";", true, true);
        for (int j=0; j<sliders.size(); j++)
        {
            string c = sliders[j];
            pd->controls.push_back(new SliderCtrlDescriptor(c));
        }

        vector<string> toggles = ofSplitString(ini.get((p + ".toggles").c_str(), string("")), ";", true, true);
        for (int j=0; j<toggles.size(); j++)
        {
            string c = toggles[j];
            pd->controls.push_back(new ToggleCtrlDescriptor(c));
        }

        vector<string> lists = ofSplitString(ini.get((p + ".lists").c_str(), string("")), ";", true, true);
        for (int j=0; j<lists.size(); j++)
        {
            string c = lists[j];
            pd->controls.push_back(new ListCtrlDescriptor(c));
        }

        if (!pd->controls.size()) {
            cout << "WARNING: No controls on page " << p << endl;
        }
        else {
            text += pd->make_html_list_element(p);
            pages[p] = pd;
        }
    }

    int sz = 0;
    char *te = read_file(wwwdir + ini.get("tmpl.index", string("index.html")), sz);
    if (sz && te) {
        index_page = te;
        delete te;
        int pos = index_page.find("{{{BUTTONS}}}");
        if (pos != string::npos) {
            int len = strlen("{{{BUTTONS}}}");
            index_page.replace(pos, len, text);
        }
    }

    sz = 0;
    te = read_file(wwwdir + ini.get("tmpl.page", string("controls.html")), sz);
    if (sz && te) {
        page_template = te;
    }

    int port = ini.get("webport", 0);

    if (port) {
        server = new HTTPServer(new WebHTTPRequestHandler, ServerSocket(port), new HTTPServerParams);
        server->start();
    } else {
        server = 0;
    }

}

char *WebController::read_file(string fn, int &sz)
{
    char buff[1000];

    try {
        ofFile f(fn, ofFile::ReadOnly, true);
        if (f.bad()) {
            sz = snprintf(buff, sizeof(buff), "Error opening %s", fn.c_str());
        } else {
            sz = f.getSize();
            char *tmp = new char[sz + 1];
            f.read(tmp, sz);
            f.close();
            tmp[sz] = 0; // for strings
            return tmp;
        }
    } catch(Poco::FileNotFoundException e) {
        sz = snprintf(buff, sizeof(buff), "file not found %s", fn.c_str());
    } catch(Poco::PathNotFoundException e) {
        sz = snprintf(buff, sizeof(buff), "path not found %s", fn.c_str());
    }

    char *err = new char[sz + 1];
    strcpy(err, buff);
    cout << err << endl;
    return err;
}

void PageDescriptor::update_page_values(string q)
{
    vector<string> qq = ofSplitString(q, "&", true, true);
    for (int i=0; i<qq.size(); ++i) {
        vector<string> kv = ofSplitString(qq[i], "=", true, true);
        if (kv.size() == 2) {
            update(kv[0], kv[1]);
        }
    }
}

PageDescriptor::PageDescriptor(string t, string bf)
{
    title = t;
    button_format = bf;
}

// Entries like <li><a href="page1">Volume</a></li>,
string PageDescriptor::make_html_list_element(string key)
{
    char buff[1000];
    snprintf(buff, sizeof(buff), button_format.c_str(), key.c_str(), title.c_str());
    return buff;
}

string PageDescriptor::make_html_slider_values()
{
    ostringstream o;
    for (int i = 0; i < controls.size(); i++)
    {
        CtrlDescriptor *c = controls[i];
        if (i) {
            o << ",";
        }
        o << c->make_java_script_value();
    }
    o.flush();
    return o.str();
}

void PageDescriptor::update(string t, string v)
{
    cout << t << " " << v << endl;
    for (int i = 0; i < controls.size(); i++)
    {
        CtrlDescriptor *c = controls[i];
        if (c->title == t) {
            c->update(v);
            break;
        }
    }
}

void ToggleCtrlDescriptor::update(string v) {
    value = (v == "true");

    if (ctlType == KeyCtl) {
        WebController::theApp->keyPressed(key);

    } else if (ctlType == MidiCtl) {
        sendNewMidi(value ? 127 : 0);

    } else {
        cout << "ToggleCtrlDescriptor update ignored for " << title << ": " << v << endl;
    }
}

void ListCtrlDescriptor::update(string v) {
    value = ofToInt(v);
    if (ctlType == KeyCtl) {
        WebController::theApp->keyPressed(key);

    } else if (ctlType == ValueCtl) {
        switch (val) {
        case VALUE_DEST:
        case VALUE_CHORD:
        case VALUE_MIDI:
            WebController::theApp->setStrValue(val, items[value]);
            break;
        case VALUE_MIDI_CH:
//        cout << "b2=" << ev.byteTwo << " ch=" << ch << endl;
            WebController::theApp->keyPressed("0123456789ABCDEF"[value % 16]);
            break;
        default:
            WebController::theApp->setValue(val, value);
        }

    } else {
        cout << "ListCtrlDescriptor update ignored for " << title << ": " << v << endl;
    }
}

void SliderCtrlDescriptor::update(string v) {

    value = ofClamp(ofToFloat(v), 0, 1);

    if (ctlType == ValueCtl) {
        WebController::theApp->setValue(val, ofMap(value, 0, 1, minval, maxval));

    } else if (ctlType == MidiCtl) {
        sendNewMidi(int(value * 127));

    } else {
        cout << "SliderCtrlDescriptor update ignored for " << title << ": " << v << endl;
    }

}

void CtrlDescriptor::sendNewMidi(int v) {
    ofxMidiMessage ev;
    ev.status = MIDI_CONTROL_CHANGE;
    ev.channel = channel;
    ev.control = cc;
    ev.value = v;
    ev.portName = "WebBO";
    ev.portNum = -1;
    WebController::theApp->newMidiMessage(ev);
}

CtrlDescriptor::CtlType CtrlDescriptor::setType(string v, string &valStr)
{
    vector<string> vv = ofSplitString(v, ":", true, true);
    if (vv.size() < 3) {
        cout << "error in control descriptor: " << v << " - ignored" << endl;
        title = v;
        return ctlType = CtrlDescriptor::NoCtl;
    }

    title = vv[0];
    valStr = vv[1];
    if (vv[2][0] == '#') {
        ctlType = ValueCtl;
        val = ofToInt(vv[2].substr(1));
    } else if (vv[2][0] == '\'') {
        ctlType = KeyCtl;
        key = vv[2][1];
    } else if (vv.size() > 3) {
        ctlType = MidiCtl;
        channel = ofToInt(vv[2]);
        cc = ofToInt(vv[3]);
    } else {
        cout << "error in control descriptor: " << v << " - ignored" << endl;
    }
}

SliderCtrlDescriptor::SliderCtrlDescriptor(string v)
{
    string valStr;
    if (setType(v, valStr) != NoCtl) {
        if (sscanf(valStr.c_str(), "%f(%f,%f)", &value, &minval, &maxval) != 3)
        {
            minval = 0;
            maxval = 1;
        }
    }
}

string SliderCtrlDescriptor::make_java_script_value()
{
    if (ctlType == ValueCtl) {
        value = ofMap(WebController::theApp->getValue(val), minval, maxval, 0, 1);
    }

    char buff[1000];
    snprintf(buff, sizeof(buff), " {\"v\":%.3f,\"t\":\"%s\",\"s\":\"slider\"} ", value, title.c_str() );
    return buff;
}

ToggleCtrlDescriptor::ToggleCtrlDescriptor(string v)
{
    string valStr;
    if (setType(v, valStr) != NoCtl) {
        value = ofToBool(valStr);
    }
}

string ToggleCtrlDescriptor::make_java_script_value()
{
    char buff[1000];
    snprintf(buff, sizeof(buff), " {\"v\":%s,\"t\":\"%s\",\"s\":\"toggle\"} ", value ? "true" : "false", title.c_str() );
    return buff;
}
// LFO v Type:0:1:33:Sine,Triangle,Rect;
ListCtrlDescriptor::ListCtrlDescriptor(string v)
{
    string valStr;
    if (setType(v, valStr) != NoCtl) {
        value = ofToInt(valStr);
        if (title[0] != '@') {
            int p = v.find_last_of(':');
            if (p != string::npos) {
                string ss = v.substr(p + 1);
                items = ofSplitString(ss, ",", true, true);
            }
        } else if (title == "@midis") {
            items = WebController::theApp->getMidiList();
        }
    }
}

string vectorToJson(vector<string> items)
{
    ostringstream o;
    for (int i = 0; i < items.size(); i++)
    {
        if (i) {
            o << ",";
        }
        o << "\"" << items[i] << "\"";
    }
    o.flush();
    return o.str();
}

string ListCtrlDescriptor::make_java_script_value()
{
// TODO get the value  from theApp
    if (title == "@drums") {
        if (!items.size()) {
            for (int i = 0; i < DrumKit::numDrumTypes; i++)
            {
                int t = DrumKit::getDrumType(i);
                items.push_back(MidiInstruments::getName(0, t, true));
            }
        }
    } else if (title == "@instruments") {
        items.clear();
        items = WebController::theApp->getInstrumentList(false);
    }

    char buff[1000];
    snprintf(buff, sizeof(buff),
             " {\"v\":%i,\"t\":\"%s\",\"s\":\"list\", \"items\":[%s]}",
             value, title.c_str(), vectorToJson(items).c_str() );
    return buff;
}

PicCtrlDescriptor::PicCtrlDescriptor(string v)
{
    title = v;
    // nothing is used of the base class
}

string PicCtrlDescriptor::make_java_script_value()
{
    ofPoint dims = WebController::theApp->getDimensions();
    int curmel = WebController::theApp->getCurrentMelody();
    ostringstream os;
    os << "{\"v\":" << curmel << ",\"t\":\"" << title << "\",\"s\":\"pic\", ";
    os << "\"centers\":[";
    for (int i = 0; ; i++) {
        Melody *m = WebController::theApp->getMelody(i);
        if (!m) {
            break;
        }
        if (i) {
            os << ",";
        }
        ofPoint ct = m->getCenter();
        os << "{\"x\":" << (ct.x / dims.x) << ",\"y\":" << (ct.y / dims.y) << "}";
    }
    os << "]}";
    os.flush();
    return os.str();
}

string PicPageDescriptor::make_html_init()
{
    return "var imageObj = new Image();" \
        "imageObj.onload = function() {" \
        //"ctx.drawImage(imageObj, 0, 0);
        "redraw();" \
    "};" \
    "imageObj.src = 'image?w=' + canvas.width + '&h=' + canvas.height;";
}
