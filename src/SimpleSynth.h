#ifndef SIMPLESYNTH_H
#define SIMPLESYNTH_H

#include "ofxIniSettings.h"

#include <math.h>

#define TABLE_SIZE 16384

class Voice
{
    public:
        Voice();
        virtual ~Voice();
        float envelope(long t);
        float envelopeGain(long t) {
            return envelope(t) * gain;
        }
        void noteOn(int p, float velocity, long t);
        void noteOff(int p, long t);
        float getPhi() {
            return (phi = fmod(phi + dp, 1));
        }
        float getPhi(float df) {
            if ( (df = dp + df) <= 0 ) return phi;
            return (phi = fmod(phi + df, 1));
        }

        float getLastValue() { return lastValue; }
        long getTriggered() { return triggered; }
        long getReleased() { return released; }
        bool getActive() { return triggered || released; }

        void setA(float a) { A = a; }
        void setD(float d) { D = d; }
        void setS(float s) { S = s; }
        void setR(float r) { R = r; }

    protected:
    private:
        // amplitude envelope
        float   A,D,S,R;

        float 	phi, dp, gain, lastValue;
        int     pitch;
        long    triggered;
        long    released;

        float sineLookup(float x);
};

class SimpleSynth : public ofBaseSoundOutput, ofBaseSoundInput
{
    public:
        SimpleSynth();
        virtual ~SimpleSynth();
        void init(ofxIniSettings &ini);
        void sendOn(int note, int velo);
        void sendOff(int note);
        void setA(float s);
        void setD(float s);
        void setS(float s);
        void setR(float s);
        static float sampleRate;

        void draw(float x, float y);
        // from ofBaseSoundOutput
		virtual void audioOut( float * output, int bufferSize, int nChannels);

    protected:
    private:
        float sineLookup(float x);
        float waveshaper(float x);
        float sinewave[TABLE_SIZE];
        ofSoundStream sound;

        int numVoices;
        Voice *voices;
        // buffers for temporary data
        float *gains;
};

#endif // SIMPLESYNTH_H
