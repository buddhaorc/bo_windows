#ifndef TABMENU_H
#define TABMENU_H

#include <list>
class Controller;
class Melody;
class TabMenu
{
    public:
        TabMenu();
        virtual ~TabMenu();

        void setController(Controller *ctl) { m_ctl = ctl; }
        void setMusic(std::list<Melody*> *m) { m_music = m; }

        // might change the key and then call the controller
        void control(int key);

        void draw(int w, int h);

    protected:
    private:
        Controller *m_ctl;
        std::list<Melody*> *m_music;
        int m_mode;
};

#endif // TABMENU_H
