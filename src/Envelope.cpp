#include <math.h>
#include "Envelope.h"

Envelope::Envelope() : edit(false)
{
    //ctor
    for (int i = 0; i < NSLIDERS; ++i)
    {
        channels[i] = -1; // control is disabled as 1,2,3.. won't work
        sliders[i] = (10 - abs( i - 4 ) ) * 10 ;
    }
    sliders[NSLIDERS] = sliders[0];
    minVol = 100;
    maxVol = 127;
    reset();
}

Envelope::~Envelope()
{
    //dtor
}

bool Envelope::handle(ofxMidiMessage& e)
{
    int ch = getSlider(e.control);
    if (ch < 0) return false;
    setSliderValue(ch, e.value);
    return true;
}

void Envelope::init(ofxIniSettings &ini)
{
    int vmin = ini.get("minVolume", 100);
    int vmax = ini.get("maxVolume", 127);
    setRange(vmin, vmax);
    setSliders(ini.get(string("sliders"), string("2,3,4,5,6,8,9,12,13")).c_str());
    setEnvelope(ini.get(string("envelope"), string("2,3,4,5,6,8,9,12,13")).c_str());
}

void Envelope::setSliders(const char *chstr)
{
    char *tmp = new char[strlen(chstr) + 1];
    strcpy(tmp, chstr);
    char * t = strtok(tmp,", ");

    for (int ch=0; ch<NSLIDERS; ch++)
    {
        int c = t ? atoi(t) : -1;
        channels[ch] = c;
        t = strtok(0, ", ");
    }
    delete [] tmp;
}

void Envelope::setEnvelope(const char *chstr)
{
    char *tmp = new char[strlen(chstr) + 1];
    strcpy(tmp, chstr);
    char * t = strtok(tmp,", ");

    for (int i=0; t && (i<NSLIDERS); i++)
    {
        int v;
        if (!sscanf(t, "%d", &v)) break;
        sliders[i] = v;
        t = strtok(0, ", ");
    }
    // to close the loop
    sliders[NSLIDERS] = sliders[0];
    delete [] tmp;
}

int Envelope::getSlider(int ch)
{
    for (int i=0; i<NSLIDERS; ++i)
    {
        if (channels[i] == ch) return i;
    }
    return -1;
}

void Envelope::setSliderValue(int ch, int v)
{
    sliders[ch] = v;
    if (!ch) sliders[NSLIDERS] = v;
}

int Envelope::getSliderValue(int ch)
{
    return sliders[ch];
}

float Envelope::getValue(long period, int beat)
{
    unsigned long now = ofGetElapsedTimeMillis();
    long t = now - t0 + ((float)beat / NSLIDERS) * period;
    mtx.lock();
    if (t >= period) {
        t %= period;
        t0 = now - t;
    }
    mtx.unlock();
    float v = getVolumeInternal((float)t / period);
    return v * (maxVol - minVol) + minVol;
}

float Envelope::getRelativeTime(int period)
{
    return (float)((ofGetElapsedTimeMillis() - t0) % period) / period;
}

float Envelope::getVolumeInternal(float reltime)
{
    if (reltime < 0) reltime = 0;
    if (reltime > 1) reltime = 1;
    int idx = floor(NSLIDERS * reltime);
    int idx2 = ceil(NSLIDERS * reltime);
    if (idx == idx2)
    {
        return sliders[idx] / 127.0f;
    }
    float step = 1.0 / NSLIDERS;
    float dt = reltime - idx * step;
    float v = sliders[idx] + (sliders[idx2] - sliders[idx]) * dt / step;
    return v / 127.0f;
}

void Envelope::draw(float x, float y, float w, float h, int qua)
{
    ofSetHexColor(0xFFFFFF);
    ofRect(x, y, w, h);
    float scx = w / (NSLIDERS + 1);
    float scy = h / 127;
    ofSetHexColor(0x777777);
    int i;
    for (i=0; i<NSLIDERS; ++i)
    {
        float v = sliders[i] * scy;
        ofRect(i*scx + x, y + h - v, scx, v);
    }

    ofSetHexColor(0x111111);
    scy = h / 100.0f;
    float v = scy * qua;
    ofRect(i*scx + x, y + h - v, scx, v);
 }

void Envelope::draw(float x, float y, float w, float h)
{
    float scx = w / NSLIDERS;
    for (int i=0; i<NSLIDERS; ++i)
    {
        ofSetColor(sliders[i]);
        ofRect(i*scx + x, y - h, scx, h);
    }
}
