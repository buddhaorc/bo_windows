#ifndef CONDITIONS_H
#define CONDITIONS_H

#include "ofxIniSettings.h"
#include "ofxMidiMessage.h"
#include <map>

#define MAX_CONDITIONS 100

#define RULE_MIDI 1
#define RULE_SERIAL 2

class CondRuleBase
{
    public:
        CondRuleBase();
        virtual ~CondRuleBase();
        virtual bool check() { return m_Active; }
        virtual int getType() = 0; // either  RULE_MIDI or RULE_SERIAL
        virtual void dump() = 0;

    protected:
        bool m_Active;
};

class CondMidiRule : public CondRuleBase
{
        const int m_On;
        const int m_Off;
    public:
        CondMidiRule(int on, int off) : m_On(on), m_Off(off) { }
        virtual int getType() { return RULE_MIDI; }
        // MIDI-specific
        virtual bool handle(ofxMidiMessage& e);
        virtual void dump() { cout << "midi(" << m_On << "," << m_Off << ") act = " << m_Active << endl; }
};

class CondSerialRule : public CondRuleBase
{
        const int m_Bit;
    public:
        CondSerialRule(int bt) : m_Bit(1 << bt) { }
        virtual int getType() { return RULE_SERIAL; }
        virtual void dump() { cout << "serial(" << m_Bit << ") act = " << m_Active << endl; }
        bool handle(int b); // from serial port
};

class CondRuleList
{
        list<CondRuleBase*> m_rules;
    public:
        void add(CondRuleBase* r) { m_rules.push_back(r); }
        bool handle(ofxMidiMessage& e);
        bool handle(int b); // from serial port
        bool isActive();
        void dump() {
            for (list<CondRuleBase*>::iterator it = m_rules.begin(); it != m_rules.end(); it++)
            {
                (*it)->dump();
            }
        }
};

class Conditions
{
    public:
        Conditions();
        virtual ~Conditions();
        static void init(ofxIniSettings &ini);
        static bool handle(ofxMidiMessage& e);
        static bool handle(int b); // a byte from serial port
        static bool isActive(int melId);
    protected:
    private:
        static std::map<int, CondRuleList*> rules;
        static void dump();
};

#endif // CONDITIONS_H
