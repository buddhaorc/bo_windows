#ifndef NOTEPLAYER_H
#define NOTEPLAYER_H

#include "ofxMidi.h"
#include "ofxOsc.h"
#include "ofxIniSettings.h"
#include "SerialSender.h"
#include "AudioSampler.h"
#include "SimpleSynth.h"
#include "AllThreads.h"

// max number of MIDI sound modules
#define MAX_MIDI 8
// max number of MIDI channels
#define NCH 16

#define CC_VELO 1
#define CC_NOTE 2
#define CC_ENVE 4
#define CC_TRCK 8
#define CC_MUTE 16

class OscSender {
    list<ofxOscSender*> senders;
    ofMutex mtx;
public:
    OscSender();
    virtual ~OscSender();
    void init(ofxIniSettings &ini);
	void sendMessage( ofxOscMessage& message );
};

class MidiOutEx : private ofxMidiOut {
    public:
        MidiOutEx(int no) : ofxMidiOut("MidiOut" + ofToString(no)), panAllowed(true) {}
        virtual bool openPort(unsigned int port=0) { return ofxMidiOut::openPort(port); }
        virtual void closePort() { ofxMidiOut::closePort(); }
        virtual string getName() { return ofxMidiOut::getName(); }

        virtual void sendProgramChange(int ch, int v);
        virtual void sendPitchBend(int ch, int v);
        virtual void sendNoteOn(int channel, int id, int value);
        virtual void sendNoteOff(int channel, int id, int value);
        virtual void sendControlChange(int channel, int id, int value);
        virtual void allNotesOff(int channel);

        bool panAllowed;
	protected:
        Poco::Mutex   mtx;
};

class MidiOut {
    protected:
        FILE *f;
        static MidiOutEx* midis[MAX_MIDI];
        string noteControl[MAX_MIDI];
        string veloControl[MAX_MIDI];
        string trackControl[MAX_MIDI];
        string envelControl[MAX_MIDI];
        static int selected;
        static int num; // number of configured MIDI outs
    public:
        MidiOut() : f(0) {}
        virtual ~MidiOut();
        virtual void init(ofxIniSettings &ini);
        static int getMidiNo() { return selected; }
        string getMidiName(int n = -1);
        static void toggleMidi() { if (++selected >= num) selected = 0; }
        static void selectMidi(string nm);
        static void stopAllNotes();
        void recordOn();
        void recordOff();
        void sendBankSelect(int channel, int value);

        // the type is CC_NOTE etc
        virtual void sendControls(int channel, int type, int value);
        virtual void sendControls(int nm, int ch, string &s, int value);
        vector<int> getControls(int channel, int type);

        // the type is CC_NOTE etc
        string getControlName(int channel, int type);
        string getControlName(string cmd);

    	void sendNoteOn(int channel, int id, int value);
        void sendNoteOff(int channel, int id, int value);
        void sendProgramChange(int ch, int v);
        void sendPitchBend(int ch, int v);
        void sendControlChange(int channel, int v1, int v2);

        string sendCustomMidi(int key, ofxIniSettings &ini);

        bool isPanAllowed(int channel);
};

class FluxaMidi : public MidiOutEx
{
        ofSerial serial;
        string comport;

    public:
        FluxaMidi(int n) : MidiOutEx(n) { }
        virtual void init(ofxIniSettings &ini);

        virtual bool openPort(unsigned int port=0);
        virtual void closePort();
        virtual string getName();
        virtual void sendNoteOn(int channel, int id, int value);
        virtual void sendNoteOff(int channel, int id, int value);
        virtual void sendProgramChange(int ch, int v);
        virtual void sendPitchBend(int ch, int v);
        virtual void sendControlChange(int channel, int v1, int v2);
        virtual void allNotesOff(int channel);
};

class MidiInstruments
{
    static int m_Instrs[MAX_MIDI][NCH];
    public:
    static int m_InstrNum;
    static int m_Effect;

    static void assignChannels(int nm, MidiOutEx &mo, string &instr);
    static void assignBank(int nm, MidiOutEx &mo, int ch, int b);
    static const char *getName(int nm, int n, bool drum);
    static int getProgram(int ch);
};

struct SenderSet
{
    MidiOut *mout;
    OscSender *osc;
    SerialSender *serial;
    AudioSampler *sampler;
    SimpleSynth *synth;
};

class NotePlayer
{
    public:
        NotePlayer(SenderSet s, int id, int ch, int a, int dur, int no, int velo, float b, ofPoint p);
        virtual ~NotePlayer();
        long tick(long t);
        void stop();

        static void sendNoteOsc(OscSender *o, int melid, int channel, ofPoint &pt, int note, float velocity, float duration);
        static void stopNoteOsc(OscSender *o, int melid, int channel);

    protected:
    private:
        SenderSet ss;

        ofPoint pt;
        int melid, channel, after, duration, note, velocity;
        float bend;

        long t0;
        bool waiting;
};

#endif // NOTEPLAYER_H
