#include "AudioSampler.h"
#include "Scale.h"
#include "Analyzer.h"
#include "Panning.h"
#include <Poco/Path.h>

using Poco::Path;

extern Panning panning;

AudioSampler::AudioSampler()
{
    //ctor
}

AudioSampler::~AudioSampler()
{
    //dtor
}

void AudioSampler::init(ofxIniSettings &ini)
{
    vector<string> sp = ofSplitString(ini.get("samplespeedctl", string("none")), ";", true, true);
    vector<string> dirs = ofSplitString(ini.get("sampledirs", string("")), ";", true, true);
    for (int i = 0; i < dirs.size(); i++)
    {
        SampleSet * ss = new SampleSet();
        if (ss->init(dirs[i]))
        {
            sets.push_back(ss);
            if (sp.size()) {
                string speed = sp[i % sp.size()];
                if (speed == "pitch") {
                    ss->speedctl = SampleSet::PITCH;
                } else if (speed == "duration") {
                    ss->speedctl = SampleSet::DURATION;
                } else {
                    ss->speedctl = SampleSet::NONE;
                }
            } else {
                ss->speedctl = SampleSet::NONE;
            }
        }
        else
        {
            delete ss;
        }
    }
}

vector<string> AudioSampler::getNames()
{
    vector<string> nms;
    for (int i=0; i<sets.size(); i++)
    {
        nms.push_back(ofToString(i) + " " + sets[i]->dirname);
    }
    return nms;
}

bool SampleSet::init(string d)
{
    cout << "loading from " << d << endl;
    dirname = d;
    ofDirectory dir;
    dir.allowExt("mp3");
    dir.allowExt("wav");
    dir.listDir(d);
    dir.sort();
    vector<string> names;
    for (int i = 0; i < dir.size(); i++)
    {
        string nm = dir.getName(i);
        ofSoundPlayer *sp = new ofSoundPlayer();
        if (sp->loadSound(d + Path::separator() + nm))
        {
            sp->setMultiPlay(true);
            samples.push_back(sp);
            sp->play();
            sp->setPosition(0.99);
            int d = sp->getPositionMS();
            cout << "loaded " << nm << " " << d << " ms " << endl;
            durations.push_back(d);
        }
        else
        {
            delete sp;
            cout << "not loaded " << nm << endl;
        }
    }
    return samples.size() > 0;
}

SampleSet::~SampleSet()
{
    for (int i = 0; i<samples.size(); i++)
    {
        delete samples[i];
    }
    samples.clear();
}

void AudioSampler::send(int ch, int n, int velo, int duration, float x, float y)
{
    if (!sets.size()) return;
    if (n <= 0 || !duration) return;
    SampleSet *ss = sets[ch % sets.size()];
    int ns = n % ss->samples.size();
    ofSoundPlayer *p = ss->samples[ns];
    float speed;
    switch (ss->speedctl) {
    case SampleSet::PITCH:
        // at the middle of the screen the speed is 1
        speed = pow(2.0, ((float)n - (Analyzer::highest + Analyzer::lowest) / 2) / Scale::getNumNotes() );
        break;
    case SampleSet::DURATION:
        speed = (float)ss->durations[ns] / duration;
        // cout << duration << " speed=" << speed << endl;
        break;
    default:
        speed = 1.0;
    };

    p->setPosition(0);
    p->setSpeed( speed );
    if (panning)
    {
        float pan = ::panning.getPan(x, y);
        p->setPan(pan);
        velo += ::panning.getAttenuation(x, y);
        p->setVolume(velo / 127.0);
    }
    else
    {
        p->setPan(x * 2 - 1);
        p->setVolume(velo / 127.0);
    }
    p->play();
}
