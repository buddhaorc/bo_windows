#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <list>
#include <iostream>
#include "ofxMidi.h"
#include "ofxIniSettings.h"
#include "ofxOpenCv.h"

#include "Melody.h"
#include "Duration.h"

class MidiControl {
    int mn, mx;
    int *v;
    float *fv;
    int ch;
    string nm;
    bool init(ofxIniSettings &ini, string id);
public:
    void init(ofxIniSettings &ini, string id, int *var);
    void init(ofxIniSettings &ini, string id, float *var);
    bool handle(ofxMidiMessage& e);
    void validate(int &v);
};

class testApp;
class Controller
{
    public:
        Controller();
        virtual ~Controller();
        void init(ofxIniSettings &ini);
        void stopAll();
        void control(int key);

        // x, y in range [0..1]
        void mouse(float x, float y);
        void mouseDbl(float x, float y);

        void setImage(ofImage *img, testApp *app);
        void setPtrns(PatternSet *ps) { m_Ptrns = ps; }
        void setMusic(std::list<Melody*> *m) {
            m_Music = m;
            currentMelody = m->begin();
        }
        bool handle(ofxMidiMessage& e, int ch, bool envEdit);
        bool isSelected(Melody *m) { return m == (*currentMelody); }
        void nextPlaying(int mode);
        void prevPlaying(int mode);

        list<Melody*>::iterator nearestMelody(float x, float y);

        const char* info(float dur);
        Melody *getSoloMelody() { return soloMelody; }
        Melody *getMelody(int id);
        Melody *getCurrentMelody() { return soloMelody ? soloMelody : *currentMelody; }
        void setCurrentMelody(int id);

        ostream &write(ostream &os);
        void read(istream &os);

        void dumpRoutes(const char *name, const char *img);
        void dumpRoutes(ostream &os, const char *img);
        void dumpMelodies(const char *name);
        void dumpMelody(Melody *me, ostream &os);
        string restoreMelodies(const char *fn = 0);

        NoteDuration *getND() { return &m_ND; }

        MidiControl quaCtl;
        int meloKnob;

    protected:
    private:
        std::list<Melody*> *m_Music;
        std::list<Melody*>::iterator currentMelody;
        testApp *m_App;
        Melody* soloMelody;
        void pageUp();
        void pageDn();
        void searchUp(int id, int mode);
        void searchDn(int id, int mode);
        void nextPattern(Melody *m, bool init);
        // mlds must be at least MAX_GROUP_SZ
        void getGroup(Melody **mlds, int &cnt);
        bool control(Melody *m, int key);
        bool controlGroup(int key);

        bool belong(ofRectangle &r1, ofRectangle &r2);

        ofImage *m_Img;
        ofxCvColorImage m_CvImg;
        PatternSet *m_Ptrns;
        NoteDuration m_ND;
        string m_restore;
        bool m_autoplay;

        // variables for MIDI control of melody quality
        const int mn, mx;// midi range
        int qua; // to be despatched to the current melody

        bool groupRGB;
};

#endif // CONTROLLER_H
