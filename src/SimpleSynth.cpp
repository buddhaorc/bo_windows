#include "SimpleSynth.h"

float SimpleSynth::sampleRate = 44100;

SimpleSynth::SimpleSynth()
{
    //ctor
    numVoices = 0;
	for (int i=0; i<TABLE_SIZE; i++) {
        sinewave[i] = -cos(TWO_PI * i / TABLE_SIZE) / 2;
	}
	voices = 0;
}

SimpleSynth::~SimpleSynth()
{
    //dtor
}

void SimpleSynth::init(ofxIniSettings &ini)
{
    numVoices = ini.get("voices", numVoices);
    if (!numVoices)
    {
        return; // the synth is disabled
    }
	voices = new Voice[numVoices];
    gains = new float[numVoices];

    setA(ofToFloat(ini.get("synthA", ofToString("0.1"))));
    setD(ofToFloat(ini.get("synthD", ofToString("0.3"))));
    setS(ofToFloat(ini.get("synthS", ofToString("0.1"))));
    setR(ofToFloat(ini.get("synthR", ofToString("0.3"))));

	//sound.listDevices();
   	sound.setDeviceID(ini.get("audioout", 0)); 	//note some devices are input only and some are output only

	int bufferSize = ini.get("buffer", 512);
	sampleRate = ofToFloat(ini.get("sampling", ofToString(sampleRate)));
    sound.setOutput(this);
    sound.setInput(this);
    // 2 output channels,
	// 0 input channels
	// 44100 samples per second
	// 512 samples per buffer
	// 4 num buffers (latency)
	sound.setup(2, 0, sampleRate, bufferSize, 4);
}
//--------------------------------------------------------------
void SimpleSynth::audioOut( float * output, int bufferSize, int nChannels ){

    long t0 = ofGetElapsedTimeMicros();
    // sampling period in microseconds
    long dt = 1000000 / sampleRate;

    for (int i = 0; i < bufferSize; i++){
        float sum = 0;
        for (int j=0; j<numVoices; ++j) {
            Voice *vo = &voices[j];
            if (vo->getActive())
            {
                // from microseconds to milliseconds
                float e = vo->envelopeGain(t0 / 1000);
                if (e) {
                    sum += sineLookup(vo->getPhi()) * e;
                }
            }
        }

        output[i*nChannels    ] = sum;
        output[i*nChannels + 1] = sum;
        // advance current time to the next audio sample
        t0 += dt;
    }
}

//--------------------------------------------------------------
float SimpleSynth::sineLookup(float x) {
    if (x < 0) {
        x = -x;
    }
    int idx = x * TABLE_SIZE;
    return sinewave[idx % TABLE_SIZE];
}

void SimpleSynth::setA(float a){
    for (int i = 0; i < numVoices; ++i) {
        voices[i].setA(a);
    }
}

//--------------------------------------------------------------
void SimpleSynth::setD(float d){
    for (int i = 0; i < numVoices; ++i) {
        voices[i].setD(d);
    }
}

//--------------------------------------------------------------
void SimpleSynth::setS(float s){
    for (int i = 0; i < numVoices; ++i) {
        voices[i].setS(s);
    }
}

//--------------------------------------------------------------
void SimpleSynth::setR(float r){
    for (int i = 0; i < numVoices; ++i) {
        voices[i].setR(r);
    }
}

void SimpleSynth::sendOn(int note, int velo)
{
    if (!numVoices) return;

    // let's find a voice which is not playing.
    // if all are busy, use the oldest released (if any)
    // or the oldest triggered
    int trigIdx = -1;
    int reldIdx = -1;
    long triggered = 0;
    long released = 0;
    int idle = -1;

    for (int i=0; i<numVoices; ++i) {
        long t = voices[i].getTriggered();
        long r = voices[i].getReleased();
        if (!t && !r) {
            idle = i;
            break;
        }

        if (t) {
            if (!triggered || (triggered > t)) {
                triggered = t;
                trigIdx = i;
            }
        }
        if (r) {
            if (!released || (released > r)) {
                released = r;
                reldIdx = i;
            }
        }
    }

    if (idle < 0) {
        // all voices are busy
        if (released) {
            idle = reldIdx;
        } else if (triggered) {
            idle = trigIdx;
        }
    }

    voices[idle].noteOn(note, velo, ofGetElapsedTimeMillis());
}

void SimpleSynth::sendOff(int note)
{
    if (!numVoices) return;
    long t = ofGetElapsedTimeMillis();
    for (int i=0; i<numVoices; ++i) {
        // a voice switches itself if it is playing the same pitch
        voices[i].noteOff(note, t);
    }
}

void SimpleSynth::draw(float x, float y)
{
    for (int i=0; i<numVoices; ++i) {
        if (voices[i].getActive())
        {
            ofSetColor(int(voices[i].getLastValue() * 255));
            ofRect(x + i * 5, y, 4, 4);
        }
    }
}

Voice::Voice()
{
	triggered = 0;
	released = 0;
	lastValue = 0;
	pitch = -1;

	gain = 1;
	phi = dp = 0;
	A = 0.1;
	D = 0.5;
	S = 0.8;
	R = 0.3;
}

Voice::~Voice()
{
    //dtor
}

//--------------------------------------------------------------
float Voice::envelope(long t) {
    if (triggered) {
        long age = t - triggered;
        if (age < A) {
            return lastValue = age / A;
        } else {
            age -= A;
            if (age < D) {
                return lastValue = 1 - (1 - S) * age / D;
            }
            return lastValue = S;
        }
    }
    else if (released) {
        long age = t - released;
        if (age > R) {
            released = 0;
        } else {
            return lastValue = S * (1 - age / R);
        }
    }
    return lastValue = 0;
}

//--------------------------------------------------------------
void Voice::noteOn(int p, float velocity, long t) {
    triggered = t;
    released = 0;
    gain = velocity / 127.0;
    float freq = 440 * pow(2, ((pitch = p)- 60) / 12.0);
    dp = freq / SimpleSynth::sampleRate;
}

//--------------------------------------------------------------
void Voice::noteOff(int p, long t) {
    if (p == pitch) {
        released = t;
        triggered = 0;
        pitch = -1;
    }
}

//--------------------------------------------------------------
