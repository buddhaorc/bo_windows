#ifndef COMMANDER_H
#define COMMANDER_H

#include <fstream>
#include "ofxMidiMessage.h"

class testApp;

class Commander
{
    public:
        Commander();
        virtual ~Commander();
        void setApp(testApp *a) { ta = a; }
        void openOut(const char *fn);
        void openIn(const char *fn);
        void act();
        void act(string cmd);

        void image(string fname);
        void scale(string fname);
        void restored(string fname);
        void key(int k);
        void value(int k, float v, int mel);
        void svalue(int k, string s, int mel);
        void midi(ofxMidiMessage &ev);
        void select(int mel);
        void goTo(float x, float y);

    protected:
    private:
        int nextActTime;
        char nextAct;
        string cmd;
        long t0;
        testApp *ta;

        std::ofstream *os;
        std::ifstream *is;
        bool readNext();
};

#endif // COMMANDER_H
