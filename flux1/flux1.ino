#include "Fluxamasynth.h"


Fluxamasynth synth;
char vals[3];
int cmd;

int sensorPin = A5;
int sensorValue = 0;
int channel = 0;

void initial() {
  synth.midiReset();            // Do a complete MIDI reset

  for (int ch = 0; ch < 0; ++ch)
  {
    synth.setChannelVolume(ch, 127); // max. channel volume

    //setReverb( channel , program , level , feedback , delayFeedback )
    // Program 
    // 0: Room1   1: Room2    2: Room3 
    // 3: Hall1   4: Hall2    5: Plate
    // 6: Delay   7: Pan delay
    synth.setReverb(ch, 5, 255, 100); // A Plate Reverb with maximum effect level
  }
  synth.setMasterVolume(255);	// max. master volume
}

void setup() {
  Serial.begin(19200);		//  Set serial port baud rate:
  initial();
}

void checkReverb() {
  int val = analogRead(sensorPin);
  if (abs(val - sensorValue) > 5) {
    sensorValue = val;
    synth.setReverb(channel, 7, val / 3, val / 7);
  }
}

void loop() {
  //checkReverb();
  
  if (!Serial.available())
  {
    delay(20);
    return;
  }

  cmd = Serial.read();
  
  switch(cmd)
  {
  case 'a': // all notes off
    synth.allNotesOff(channel = Serial.read());
    break;

  case 'R': // all reset
    initial();
    break;

  case 'n': // note on
    if (Serial.readBytes(vals, 3) == 3)
    {
      synth.noteOn(channel = (vals[0] - 1) & 0xF, vals[1], vals[2]);
    }
    break;

  case 'f': // note off
    if (Serial.readBytes(vals, 3) == 3)
    {
      synth.noteOff(channel = (vals[0] - 1) & 0xF, vals[1]);
    }
    break;

  case 'p': // program change
    if (Serial.readBytes(vals, 2) == 2)
    {
      synth.programChange(0, channel = (vals[0] - 1) & 0xF, vals[1]);
    }
    break;

  case 'b': // pitch bend
    if (Serial.readBytes(vals, 2) == 2)
    {
      synth.pitchBend(channel = (vals[0] - 1) & 0xF, vals[1]);
    }
    break;

  case 'c': // control change
    if (Serial.readBytes(vals, 3) == 3)
    {
      channel = (vals[0] - 1) & 0xF;
      byte cc[] = { 0xB0 + channel, vals[1], vals[2]};
      synth.fluxWrite(cc, 3);
    }
    break;
  }
} 
