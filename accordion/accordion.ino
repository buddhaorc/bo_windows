// The program replicates bits coming from a serial port
// to a sequence of output digital bits
// BIT_BASE, BIT_BASE+1, etc.

// Do not set more than 8
#define MAX_PINS 8

// The first bit to set
// Pin 13 has an LED connected on most Arduino boards
#define BIT_BASE 7

// The first bit to read
#define INPUT_BIT_BASE 2

// 7,8,9,10,11,12,13,14

int inputVal = 0;

void setup() {
  
  // initialize digital pins as an output.
  for (int i=0; i<MAX_PINS; ++i) {
    int pin = BIT_BASE + i;
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
  }    

  for (int i=INPUT_BIT_BASE; i<BIT_BASE; ++i) {
    pinMode(i, INPUT);
  }    

  // start the communication between the oF app and the Arduino
  Serial.begin(9600); 
}

void writeBits(int bt) {
    for (int i=0; i<MAX_PINS; ++i) {
      if ((bt & 1) == 0) {
        digitalWrite(i + BIT_BASE, LOW);
      } else {
        digitalWrite(i + BIT_BASE, HIGH);
      }
      bt >>= 1;
    }
}

int readInputPins() {
  int v = 0;
  for (int i=INPUT_BIT_BASE, j=1; i<BIT_BASE; ++i, j <<= 1) {
    if (digitalRead(i)) {
      v += j;
    }
  }    
  return v;
}

void loop() {
  if (Serial.available() > 0) {
    int b = Serial.read();
    writeBits(b);
  }
  
  int sv = readInputPins();
  if (inputVal != sv) {
    Serial.write(inputVal = sv);
  }
  
  delay(10);
}

