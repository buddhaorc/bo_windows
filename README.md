# GIT

## Dealing with line endings in git
Line ending differences between Win, Lin and OS X can be messy in git. There are [settings to make this easier][EOL].
On Mar 17th, 2013 the .gitattributes file was created to force commited files to only use LF, and line ending to be replace by system settings on checkout (CRLF for Windows).

## Branches
There are two branches now, called master and bo_gnulinux. They have slight differences. The project files are different, the .ini file is located inside vs outside the data/ folder, and there are slight midi implementation differences. Those may be removed in the future. Ideally, with the use of #ifdef we could have a common code base (.cpp and .h files).

To switch branches:

1. git checkout master
1. git checkout bo_gnulinux

To push bo_gnulinux branch:

1. git push origin bo_gnulinux

To push all branches:

1. git push

To find out current branch:

1. git branch

[Good tutorial about using branches][branch]

# Ubuntu
From the [OpenFrameworks install guide][ofguide]

1. Download OpenFrameworks for Ubuntu and unpack the tar.gz.
1. cd scripts/linux/ubuntu
1. sudo ./install_codeblocks.sh
1. sudo ./install_dependencies.sh
1. cd ../../../addons
1. git clone https://github.com/danomatika/ofxMidi.git
1. git clone https://github.com/companje/ofxIniSettings.git
1. git clone https://github.com/companje/ofxExtras.git
1. Check the folders ofxIniSettings and ofxExtras. If .cpp and .h files are in the root of those folders, create ofxIniSettings/src/ and ofxExtras/src/ and move the .cpp and .h files inside the corresponding src/ folders. If they are left in the root they will not be found by CodeBlocks.
1. cd ../apps/myApps/
1. git clone https://{YOUR\_USER\_NAME}@bitbucket.org/buddhaorc/bo_windows.git
1. git checkout bo_gnulinux
1. Open bo.workspace in Code Blocks and hit F9 to compile.

# Windows
(coming soon... maybe)

# To do
If possible, rename repository from bo_windows to just bo or BuddhaOrc, since it contains not only the windows source.

[EOL]: https://help.github.com/articles/dealing-with-line-endings
[branch]: http://git-scm.com/book/en/Git-Branching-Basic-Branching-and-Merging
[ofguide]: http://www.openframeworks.cc/setup/linux-codeblocks/

# Hardware Exotics

## Doepfer Arpeggiator

Let's assume the Doepfer is connected with USB-MIDI adapter and configured as device #2

    midiout2=MIDIOUT

and

    instruments2=

To control arpeggiator tempo, select a melody with output assigned to device #2 (repeat F10
until MIDIOUT is selected and press 1 to assign the 1st channel),
then press Ctrl-a, Ctrl-b etc having these lines in config:

    midi2ctla=C,1,1,16
    midi2ctlb=C,1,1,32
    midi2ctlc=C,1,1,64
    midi2ctld=C,1,1,96
    midi2ctle=C,1,1,127
    # disable
    midi2ctlx=C,1,1,0

If it doesn't work, turn on the learning mode by pressing a little button on the Doepfer
and press Ctrl-x with this line in config

    midi2ctlx=P,1,18

To disable, press Ctrl-y

    midi2ctly=P,1,17


## Arduino Accordion

1. Connect Arduino with Accordion software loaded
1. Check the COM port assigned to Arduino in Device Manager
1. Specify it in config.ini like for example com=COM4
1. The serialBits value provides the mapping between notes (%12) and bits to be set on Arduino digital out.
1. For 7-keys accordion, ignore missing black keys serialBits=1,1,2,2,4,8,8,16,16,32,32,64 
1. Redirect a melody output to serial port by pressing Shift-T until it shows SER in status line

## Arduino Motion Sensor and Switches

1. Connect Arduino with Accordion software loaded
1. Check the COM port assigned to Arduino in Device Manager
1. Specify it in config.ini like for example com=COM4
1. Add rules to melodies that should be controlled by Arduino

These examples are for melodies 0 and 2:

    rule0=serial 0
    rule2=serial 1;serial 2

# Kandinsky Music Groups

1. Enable discovery and specify the port to listen on:

    discovery=true
    oscListen=12345

1. Assign melodies playing at the moment to BO Controller if present by pressing 
1. Synchronize execution by pressing '/'